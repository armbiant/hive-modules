/*	Copyright (C) 2018 Arrival Ltd
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit git.hiverepo.com */

/** @file */

#include <apiary/apiary.pmh>

#define	HIVE_HH_DEBUG	TRUE

using namespace fe;
using namespace fe::ext;

namespace hive
{

HiveHost::HiveHost(void)
{
	SingleMaster::usePrefix("hive");

#if HIVE_HH_DEBUG
	feLog("HiveHost::HiveHost\n");
#endif

	m_spNetHost=NetHost::create();
}

HiveHost::~HiveHost(void)
{
#if HIVE_HH_DEBUG
	feLog("HiveHost::~HiveHost\n");
#endif
}

//* static
sp<HiveHost> HiveHost::create(void)
{
	//* NOTE does this need a mutex?

#if HIVE_HH_DEBUG
	feLog("HiveHost::create\n");
#endif

	return sp<HiveHost>(new HiveHost());
}

void HiveHost::loadDataSet(sp<RecordGroup>& a_rDataSetRG,
	const Array<String>& a_rgFilenames)
{
	if(a_rDataSetRG.isNull())
	{
		a_rDataSetRG=new RecordGroup();
	}

	sp<Scope> spScope=master()->registry()->create("Scope");

	const I32 rgCount=a_rgFilenames.size();
	for(I32 rgIndex=0;rgIndex<rgCount;rgIndex++)
	{
		feLog("HiveHost::loadDataSet %d/%d \"%s\"\n",rgIndex,rgCount,
				a_rgFilenames[rgIndex].c_str());

		sp<data::StreamI> spStreamDataset(
				new data::AsciiStream(spScope));

		std::ifstream strm(a_rgFilenames[rgIndex].c_str());
		if(!strm.is_open())
		{
			feLog("HiveHost::loadDataSet"
					" could not open dataset rg %d/%d \"%s\"\n",
					rgIndex,rgCount,
					a_rgFilenames[rgIndex].c_str());
			continue;
		}
		sp<RecordGroup> rg_file = spStreamDataset->input(strm);
		strm.close();

		a_rDataSetRG->add(rg_file);
	}
}

} /* namespace hive */
