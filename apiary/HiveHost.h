/*	Copyright (C) 2018 Arrival Ltd
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit git.hiverepo.com */

/** @file */

#ifndef __apiary_HiveHost_h__
#define __apiary_HiveHost_h__

namespace hive
{

/**************************************************************************//**
    @brief Point of Entry for Hive Development

	HiveHost can be used alongside NetHost since
	a NetHost member is used as a delegate.

	@ingroup apiary
*//***************************************************************************/
class FE_DL_PUBLIC HIVE_APIARY_PORT HiveHost:
	public fe::Handled<fe::ext::NetHost>,
	public fe::CastableAs<HiveHost>
{
	public:
									HiveHost(void);
virtual								~HiveHost(void);

static	fe::sp<HiveHost> FE_CDECL	create(void);

		fe::sp<fe::Master>			master(void)
									{	return m_spNetHost->master(); }

		BWORD						hasSpace(fe::String a_space)
									{	return m_spNetHost->hasSpace(a_space); }

		fe::sp<fe::StateCatalog>	accessSpace(fe::String a_space,
											fe::String a_implementation=
											"ConnectedCatalog")
									{	return m_spNetHost->accessSpace(
												a_space,a_implementation); }

		void						loadDataSet(
										fe::sp<fe::RecordGroup>& a_rDataSetRG,
										const fe::Array<fe::String>&
										a_rgFilenames);

	private:

		fe::sp<fe::ext::NetHost>	m_spNetHost;
};

} /* namespace hive */

#endif /* __apiary_HiveHost_h__ */
