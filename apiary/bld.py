import sys
import os
import shutil
import utility
forge = sys.modules["forge"]

def prerequisites():
	return [ "networkhost" ]

def setup(module):
	srcList = [	"HiveHost",
				"apiary.pmh",
				"apiaryDL" ]

	dll = module.DLL( "hiveApiaryDL", srcList )

	deplibs = forge.corelibs + [
				"fexSignalLib",
				"fexNetworkDLLib",
				"fexNetworkHostDLLib" ]

	if forge.fe_os == 'FE_WIN32' or forge.fe_os == 'FE_WIN64':
		deplibs += [	"fexDataToolLib" ]

	forge.deps( ["hiveApiaryDLLib"], deplibs )

	forge.tests += [
		("inspect.exe",	"hiveApiaryDL",					None,	None) ]

	module.Module('test')
