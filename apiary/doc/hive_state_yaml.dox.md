Importing State with YAML {#hive_state_yaml}
=========================

Loading YAML into a Catalog {#hive_state_yaml_load}
===========================

You set a load state into an fe::Catalog from YAML files.
This fe::Catalog can simply be an fe::StateCatalog,
or the state can be loaded into any fe::Catalog and then
overlayed onto an existing Catalog or StateCatalog.

```cpp
sp<CatalogReaderI> spCatalogReader=spRegistry->create("CatalogReaderI.*.*.yaml");
if(spCatalogReader.isValid())
{
	sp<Catalog> spImportedCatalog=spMaster->createCatalog("yaml import");
	if(spImportedCatalog.isValid())
	{
		for(I32 arg=1;arg<argc;arg++)
		{
			spCatalogReader->load(argv[arg],spImportedCatalog);
		}

		spStateCatalog->overlayState(spImportedCatalog);
	}
}
```

In this example, a Catalog used for the loading is created on the spot.
The Catalog is created through the fe::Master,
which leaves a reference there so that the isolated loaded state
could be examined later by some other code.

The spCatalogReader will be an fe::ext::CatalogReaderYaml or
a suitable substitute.

This simple example naively treats every argument the executable was run with
as a yaml filename.
A real application should presumably use more discretion.

In this case, as each file is loaded, it is added to the same Catalog.
The `load` method does not clear the target Catalog when it starts.

@note
The `load` method does not lock the Catalog it is given,
but the `overlayState` method does lock the StateCatalog it was run on.
So even after the StateCatalog has started,
additional state could be overlayed from a loaded Catalog
in the middle of active communications.

Writing a State YAML File {#hive_state_yaml_syntax}
=========================

The files used to provide state data are standard YAML format files.
The layout of these files consists of a particular syntax
conforming to the genereralized YAML rules.

### YAML Block: include

As with C or C++, you can insert text from another file using an include.
The simplest form is just one line.

```yaml
- include: "config/more_state.yaml"
```

Instead, of a single string filename,
a list of filenames can be provided.

```yaml
- include:
    - "config/my_connection.yaml"
    - "config/my_track.yaml"
    - "config/my_car.yaml"
```

If an included file has substitution variables,
you can add a sub block with name/value pairs to substitute
in those variables when parsing the included file.

```yaml
- include: "config/my_car.yaml"
  variables:
    VID: 7
    COLOR: "red"
```

### YAML Block: variables

The state files can have substitution variables that replace text
of the form `$VARIABLE` or `${VARIABLE}`.
You should use the brackets if the token is not delimited by whitespace.

```yaml
- variables:
  VID: 0
  COLOR: "black"
```

If a file is included by another file, the `include` block can provide
overriding values for variable.
In that case, the values in the included file are essentially default values.

### YAML Block: state

A state block provides values as name/value pairs.
These pairs become catalog entries.

```yaml
- state:
  vehicles.${VID}.color: ${COLOR}
  vehicles.${VID}.mass: 1000.0
```

### YAML Raw State

Unconditional state doesn't have to be provided in `state` block.
Top level YAML nodes with the dash prefix
are interpreted as simple name-value pairs.

The prior example could have omitted the encapsulating `state` block.

```yaml
vehicles.${VID}.color: ${COLOR}
vehicles.${VID}.mass: 1000.0
```

### YAML Conditional: only

Any of the aformentioned blocks can have a conditional block
to determined whether that block affects the resulting state.

Conditional state does need to be in a `state` block.

```yaml
- include: "config/my_car.yaml"
  only:
    variables:
      - $VEHICLE_COUNT > 7
  variables:
    VID: 7
    COLOR: "red"
```

In this case, the include for a red car with an id of 7 is only included
if there are more than seven cars in the scene
(where the car ids started with zero).

If there are are multiple conditions on the list,
they must all be true for the block to take effect.

A variable check can use the `==`, `!=`, `<`, `<=`, `>=`, or `>` operator.

@note
The only conditional currently supported is `variables`.

### YAML State Properties

Normally, state is only given for the the default "value" property
of each key name.
If you wish to set any other property, or a list of properties,
then all property names must be explicitly given,
using a list of property/value pairs.

```yaml
vehicles.${VID}.mass:
  value: 1000.0
  units: "kg"
```

### YAML State Types

In most cases, simple types can be auto-detected.
If an explicit type specification is need,
use a YAML tag.

```yaml
track.coordinates: !vector3d [36.2748244, -115.016884, 607.59]
track.finish.location: [-74.0, 15.0, 0.0]
```

In this case, the location use the default single precision of vector3f,
but the map coordinates of the track origin needs double precision.

At a minimum, the available types are:
  - bool, boolean
  - unsigned char
  - int, integer, U8, U16, I16, U32, I32, U64, I64
  - real, float, double, F32, F64
  - string
  - vector3, vector3f, vector3d
  - stringarray

@warning
When specifiying scalar numbers,
remember that 3 is an integer and 3.0 is a float.
If you don't want to use `!float` tags,
make sure to have a decimal on every float.
Any apparent lenience may change in the future.
