import sys
forge = sys.modules["forge"]

import os.path

def setup(module):
	deplibs = forge.corelibs[:]

	deplibs += [	"fexNetworkHostDLLib",
					"hiveApiaryDLLib" ]

	if forge.fe_os == 'FE_WIN32' or forge.fe_os == 'FE_WIN64':
		deplibs += ["fexSignalLib",
					"fexDataToolLib",
					"fexNetworkDLLib" ]

	tests = [	'xHiveStartStop',
				'xRateTestSender',
				'xRateTestReceiver',
			]

	for t in tests:
		exe = module.Exe(t)
		if forge.fe_os == "FE_LINUX":
			exe.linkmap = { "stdthread": "-lpthread" }
		forge.deps([t + "Exe"], deplibs)

	forge.tests += [
		("xHiveStartStop.exe",		"",		"",		None) ]

#	forge.tests += [
#		("xRateTestSender.exe",			"",		"",
#		("xRateTestReceiver.exe",		"",		"",		None)) ]
