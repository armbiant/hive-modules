/*	Copyright (C) 2018 Arrival Ltd
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit git.hiverepo.com */

/** @file */

#ifndef __x_RateTest_Common_Data_Types_h__
#define __x_RateTest_Common_Data_Types_h__

#include <thread>
#include <chrono>

//-----------------------------------------------------------------
struct floatVect_3
{
	float x;
	float y;
	float z;
};

//-----------------------------------------------------------------
struct floatMat_33
{
	floatVect_3 col_0;
	floatVect_3 col_1;
	floatVect_3 col_2;
};

//-----------------------------------------------------------------
struct floatTransform
{
	floatVect_3 _pos;
	floatMat_33 _rot;
};


//-----------------------------------------------------------------
struct vehicleDataT
{
	floatTransform		chassisInfo;
	floatTransform		wheelsInfo[4];
	int 				frameNumber;
	bool operator==(const vehicleDataT & other)const;
};
inline bool vehicleDataT::operator==(const vehicleDataT & other)const
{
	bool cInfo = (0 == memcmp(reinterpret_cast<const void *>(&chassisInfo), reinterpret_cast<const void *>(&other.chassisInfo), sizeof(chassisInfo)));
	bool wInfo = (0 == memcmp(reinterpret_cast<const void *>(&wheelsInfo), reinterpret_cast<const void *>(&other.wheelsInfo), sizeof(wheelsInfo)));
	bool fInfo = (0 == memcmp((const void *)(&frameNumber), (const void *)(&other.frameNumber), sizeof(frameNumber)));
	return (cInfo && wInfo && fInfo);

}

//-----------------------------------------------------------------
struct HiveHost_T
{
	fe::sp<hive::HiveHost>		m_spHiveHost;
	fe::sp<fe::StateCatalog>	m_spStateCatalog;
	vehicleDataT				m_vehicleXform;
    std::thread *				m_xferThred;
    bool						m_isRunnig;
    double						m_frameRate;
    int							m_frameNumber;
};

class InfoVehicleDataT : public fe::BaseType::Info
{
	//----------------------------------------------------------------
	// print()
	//----------------------------------------------------------------
	virtual	fe::String	print(void *instance)
	{

		U32 N = sizeof(vehicleDataT) / sizeof(float);
		const vehicleDataT *pV = (vehicleDataT *)instance;
		fe::String string;

		// Chassis Position
		string.catf("%.6G%s", pV->chassisInfo._pos.x, " ");
		string.catf("%.6G%s", pV->chassisInfo._pos.y, " ");
		string.catf("%.6G%s", pV->chassisInfo._pos.z, " ");

		// Chassis Rotation
		string.catf("%.6G%s", pV->chassisInfo._rot.col_0.x, " ");
		string.catf("%.6G%s", pV->chassisInfo._rot.col_0.y, " ");
		string.catf("%.6G%s", pV->chassisInfo._rot.col_0.z, " ");
		string.catf("%.6G%s", pV->chassisInfo._rot.col_1.x, " ");
		string.catf("%.6G%s", pV->chassisInfo._rot.col_1.y, " ");
		string.catf("%.6G%s", pV->chassisInfo._rot.col_1.z, " ");
		string.catf("%.6G%s", pV->chassisInfo._rot.col_2.x, " ");
		string.catf("%.6G%s", pV->chassisInfo._rot.col_2.y, " ");
		string.catf("%.6G%s", pV->chassisInfo._rot.col_2.z, " ");

		// Wheels
		string.catf("%.6G%s", pV->wheelsInfo[0]._pos.x, " ");
		string.catf("%.6G%s", pV->wheelsInfo[0]._pos.y, " ");
		string.catf("%.6G%s", pV->wheelsInfo[0]._pos.z, " ");
		string.catf("%.6G%s", pV->wheelsInfo[0]._rot.col_0.x, " ");
		string.catf("%.6G%s", pV->wheelsInfo[0]._rot.col_0.y, " ");
		string.catf("%.6G%s", pV->wheelsInfo[0]._rot.col_0.z, " ");
		string.catf("%.6G%s", pV->wheelsInfo[0]._rot.col_1.x, " ");
		string.catf("%.6G%s", pV->wheelsInfo[0]._rot.col_1.y, " ");
		string.catf("%.6G%s", pV->wheelsInfo[0]._rot.col_1.z, " ");
		string.catf("%.6G%s", pV->wheelsInfo[0]._rot.col_2.x, " ");
		string.catf("%.6G%s", pV->wheelsInfo[0]._rot.col_2.y, " ");
		string.catf("%.6G%s", pV->wheelsInfo[0]._rot.col_2.z, " ");

		string.catf("%.6G%s", pV->wheelsInfo[1]._pos.x, " ");
		string.catf("%.6G%s", pV->wheelsInfo[1]._pos.y, " ");
		string.catf("%.6G%s", pV->wheelsInfo[1]._pos.z, " ");
		string.catf("%.6G%s", pV->wheelsInfo[1]._rot.col_0.x, " ");
		string.catf("%.6G%s", pV->wheelsInfo[1]._rot.col_0.y, " ");
		string.catf("%.6G%s", pV->wheelsInfo[1]._rot.col_0.z, " ");
		string.catf("%.6G%s", pV->wheelsInfo[1]._rot.col_1.x, " ");
		string.catf("%.6G%s", pV->wheelsInfo[1]._rot.col_1.y, " ");
		string.catf("%.6G%s", pV->wheelsInfo[1]._rot.col_1.z, " ");
		string.catf("%.6G%s", pV->wheelsInfo[1]._rot.col_2.x, " ");
		string.catf("%.6G%s", pV->wheelsInfo[1]._rot.col_2.y, " ");
		string.catf("%.6G%s", pV->wheelsInfo[1]._rot.col_2.z, " ");

		string.catf("%.6G%s", pV->wheelsInfo[2]._pos.x, " ");
		string.catf("%.6G%s", pV->wheelsInfo[2]._pos.y, " ");
		string.catf("%.6G%s", pV->wheelsInfo[2]._pos.z, " ");
		string.catf("%.6G%s", pV->wheelsInfo[2]._rot.col_0.x, " ");
		string.catf("%.6G%s", pV->wheelsInfo[2]._rot.col_0.y, " ");
		string.catf("%.6G%s", pV->wheelsInfo[2]._rot.col_0.z, " ");
		string.catf("%.6G%s", pV->wheelsInfo[2]._rot.col_1.x, " ");
		string.catf("%.6G%s", pV->wheelsInfo[2]._rot.col_1.y, " ");
		string.catf("%.6G%s", pV->wheelsInfo[2]._rot.col_1.z, " ");
		string.catf("%.6G%s", pV->wheelsInfo[2]._rot.col_2.x, " ");
		string.catf("%.6G%s", pV->wheelsInfo[2]._rot.col_2.y, " ");
		string.catf("%.6G%s", pV->wheelsInfo[2]._rot.col_2.z, " ");

		string.catf("%.6G%s", pV->wheelsInfo[3]._pos.x, " ");
		string.catf("%.6G%s", pV->wheelsInfo[3]._pos.y, " ");
		string.catf("%.6G%s", pV->wheelsInfo[3]._pos.z, " ");
		string.catf("%.6G%s", pV->wheelsInfo[3]._rot.col_0.x, " ");
		string.catf("%.6G%s", pV->wheelsInfo[3]._rot.col_0.y, " ");
		string.catf("%.6G%s", pV->wheelsInfo[3]._rot.col_0.z, " ");
		string.catf("%.6G%s", pV->wheelsInfo[3]._rot.col_1.x, " ");
		string.catf("%.6G%s", pV->wheelsInfo[3]._rot.col_1.y, " ");
		string.catf("%.6G%s", pV->wheelsInfo[3]._rot.col_1.z, " ");
		string.catf("%.6G%s", pV->wheelsInfo[3]._rot.col_2.x, " ");
		string.catf("%.6G%s", pV->wheelsInfo[3]._rot.col_2.y, " ");
		string.catf("%.6G%s", pV->wheelsInfo[3]._rot.col_2.z, " ");
		string.catf("%.6G%s", pV->frameNumber, "");

		return string;
	}

	//----------------------------------------------------------------
	// output()
	//----------------------------------------------------------------
	virtual	IWORD	output(std::ostream &ostrm, void *instance, t_serialMode mode)
	{
		IWORD n = 0;
		vehicleDataT *pV = (vehicleDataT *)instance;

		if (mode == e_ascii)
		{
			ostrm << '"'; ostrm << pV->chassisInfo._pos.x;
			ostrm << ' '; ostrm << pV->chassisInfo._pos.y;
			ostrm << ' '; ostrm << pV->chassisInfo._pos.z;
			ostrm << ' '; ostrm << pV->chassisInfo._rot.col_0.x;
			ostrm << ' '; ostrm << pV->chassisInfo._rot.col_0.y;
			ostrm << ' '; ostrm << pV->chassisInfo._rot.col_0.z;
			ostrm << ' '; ostrm << pV->chassisInfo._rot.col_1.x;
			ostrm << ' '; ostrm << pV->chassisInfo._rot.col_1.y;
			ostrm << ' '; ostrm << pV->chassisInfo._rot.col_1.z;
			ostrm << ' '; ostrm << pV->chassisInfo._rot.col_2.x;
			ostrm << ' '; ostrm << pV->chassisInfo._rot.col_2.y;
			ostrm << ' '; ostrm << pV->chassisInfo._rot.col_2.z;
			for (uint32_t i = 0; i < 4; ++i)
			{
				ostrm << ' '; ostrm << pV->wheelsInfo[i]._pos.x;
				ostrm << ' '; ostrm << pV->wheelsInfo[i]._pos.y;
				ostrm << ' '; ostrm << pV->wheelsInfo[i]._pos.z;
				ostrm << ' '; ostrm << pV->wheelsInfo[i]._rot.col_0.x;
				ostrm << ' '; ostrm << pV->wheelsInfo[i]._rot.col_0.y;
				ostrm << ' '; ostrm << pV->wheelsInfo[i]._rot.col_0.z;
				ostrm << ' '; ostrm << pV->wheelsInfo[i]._rot.col_1.x;
				ostrm << ' '; ostrm << pV->wheelsInfo[i]._rot.col_1.y;
				ostrm << ' '; ostrm << pV->wheelsInfo[i]._rot.col_1.z;
				ostrm << ' '; ostrm << pV->wheelsInfo[i]._rot.col_2.x;
				ostrm << ' '; ostrm << pV->wheelsInfo[i]._rot.col_2.y;
				ostrm << ' '; ostrm << pV->wheelsInfo[i]._rot.col_2.z;
			}
            ostrm << ' '; ostrm << pV->frameNumber;
			ostrm << '"';
		}
		else
		{
			ostrm << pV->chassisInfo._pos.x;
			ostrm << pV->chassisInfo._pos.y;
			ostrm << pV->chassisInfo._pos.z;
			ostrm << pV->chassisInfo._rot.col_0.x;
			ostrm << pV->chassisInfo._rot.col_0.y;
			ostrm << pV->chassisInfo._rot.col_0.z;
			ostrm << pV->chassisInfo._rot.col_1.x;
			ostrm << pV->chassisInfo._rot.col_1.y;
			ostrm << pV->chassisInfo._rot.col_1.z;
			ostrm << pV->chassisInfo._rot.col_2.x;
			ostrm << pV->chassisInfo._rot.col_2.y;
			ostrm << pV->chassisInfo._rot.col_2.z;
			for (uint32_t i = 0; i < 4; ++i)
			{
				ostrm << pV->wheelsInfo[i]._pos.x;
				ostrm << pV->wheelsInfo[i]._pos.y;
				ostrm << pV->wheelsInfo[i]._pos.z;
				ostrm << pV->wheelsInfo[i]._rot.col_0.x;
				ostrm << pV->wheelsInfo[i]._rot.col_0.y;
				ostrm << pV->wheelsInfo[i]._rot.col_0.z;
				ostrm << pV->wheelsInfo[i]._rot.col_1.x;
				ostrm << pV->wheelsInfo[i]._rot.col_1.y;
				ostrm << pV->wheelsInfo[i]._rot.col_1.z;
				ostrm << pV->wheelsInfo[i]._rot.col_2.x;
				ostrm << pV->wheelsInfo[i]._rot.col_2.y;
				ostrm << pV->wheelsInfo[i]._rot.col_2.z;
			}
			ostrm << pV->frameNumber;
		}

		n = sizeof(vehicleDataT) / sizeof(float);
		return (mode == e_ascii) ? c_ascii : n;
	}

	//----------------------------------------------------------------
	// input()
	//----------------------------------------------------------------
	virtual	void	input(std::istream &istrm, void *instance, t_serialMode mode)
	{
		vehicleDataT *pV = (vehicleDataT *)instance;
		float t = 0.0f;
		int f = -1;

		helpF32.input(istrm, (void *)&t, mode); pV->chassisInfo._pos.x = t;
		helpF32.input(istrm, (void *)&t, mode); pV->chassisInfo._pos.y = t;
		helpF32.input(istrm, (void *)&t, mode); pV->chassisInfo._pos.z = t;
		helpF32.input(istrm, (void *)&t, mode); pV->chassisInfo._rot.col_0.x = t;
		helpF32.input(istrm, (void *)&t, mode); pV->chassisInfo._rot.col_0.y = t;
		helpF32.input(istrm, (void *)&t, mode); pV->chassisInfo._rot.col_0.z = t;
		helpF32.input(istrm, (void *)&t, mode); pV->chassisInfo._rot.col_1.x = t;
		helpF32.input(istrm, (void *)&t, mode); pV->chassisInfo._rot.col_1.y = t;
		helpF32.input(istrm, (void *)&t, mode); pV->chassisInfo._rot.col_1.z = t;
		helpF32.input(istrm, (void *)&t, mode); pV->chassisInfo._rot.col_2.x = t;
		helpF32.input(istrm, (void *)&t, mode); pV->chassisInfo._rot.col_2.y = t;
		helpF32.input(istrm, (void *)&t, mode); pV->chassisInfo._rot.col_2.z = t;

		for (int i = 0; i < 4; i++)
		{
			helpF32.input(istrm, (void *)&t, mode); pV->wheelsInfo[i]._pos.x = t;
			helpF32.input(istrm, (void *)&t, mode); pV->wheelsInfo[i]._pos.y = t;
			helpF32.input(istrm, (void *)&t, mode); pV->wheelsInfo[i]._pos.z = t;
			helpF32.input(istrm, (void *)&t, mode); pV->wheelsInfo[i]._rot.col_0.x = t;
			helpF32.input(istrm, (void *)&t, mode); pV->wheelsInfo[i]._rot.col_0.y = t;
			helpF32.input(istrm, (void *)&t, mode); pV->wheelsInfo[i]._rot.col_0.z = t;
			helpF32.input(istrm, (void *)&t, mode); pV->wheelsInfo[i]._rot.col_1.x = t;
			helpF32.input(istrm, (void *)&t, mode); pV->wheelsInfo[i]._rot.col_1.y = t;
			helpF32.input(istrm, (void *)&t, mode); pV->wheelsInfo[i]._rot.col_1.z = t;
			helpF32.input(istrm, (void *)&t, mode); pV->wheelsInfo[i]._rot.col_2.x = t;
			helpF32.input(istrm, (void *)&t, mode); pV->wheelsInfo[i]._rot.col_2.y = t;
			helpF32.input(istrm, (void *)&t, mode); pV->wheelsInfo[i]._rot.col_2.z = t;
		}
		helpI32.input(istrm, (void *)&f, mode); pV->frameNumber = f;
	}

	//----------------------------------------------------------------
	// iosize()
	//----------------------------------------------------------------
	virtual	IWORD	iosize(void) { return sizeof(vehicleDataT); }

	//----------------------------------------------------------------
	// alignment()
	//----------------------------------------------------------------
	virtual	FE_UWORD	alignment(void)
	{
		int N = sizeof(vehicleDataT) / sizeof(float);
		if (1)
		{
			return (N * sizeof(vehicleDataT) == FE_MEM_ALIGNMENT ||
				(N + 1) * sizeof(vehicleDataT) == FE_MEM_ALIGNMENT) ?
				FE_MEM_ALIGNMENT : 0;
		}
		else
		{
			return 0;
		}
	}

	//----------------------------------------------------------------
	// getConstruct()
	//----------------------------------------------------------------
	virtual	bool	getConstruct(void) { return true; }

	//----------------------------------------------------------------
	// construct()
	//----------------------------------------------------------------
	virtual	void	construct(void *instance)
	{
		memset(instance, 0, sizeof(vehicleDataT));
	}

	//----------------------------------------------------------------
	// destruct()
	//----------------------------------------------------------------
	virtual	void	destruct(void *instance)
	{
	}

private:
	fe::InfoF32 helpF32;
	fe::InfoI32 helpI32;
};

void assertVehicleData(fe::sp<fe::TypeMaster> spTypeMaster)
{
	fe::sp<fe::BaseType> spT;
	spT = spTypeMaster->assertType<vehicleDataT>("vehicleDataT");
	spT->setInfo(new InfoVehicleDataT());
}
#endif
