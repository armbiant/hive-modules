/*	Copyright (C) 2018 Arrival Ltd
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit git.hiverepo.com */

#ifndef __computer_vision_ComputerVisionI_h__
#define __computer_vision_ComputerVisionI_h__


#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>

namespace hive
{

struct HoughParam
{
	double rho;
	int threshold;
	int minLineLen;
	int maxLineGap;
};

struct LineParameters
{
    double _slope;
    double _intercept;
};


class FE_DL_EXPORT ComputerVisionI : virtual public fe::Component,
							         public fe::CastableAs<ComputerVisionI>
{
public:
	virtual void canny(cv::Mat & srcImg, cv::Mat & dstImg, cv::Size gaussBlur) = 0;	
	virtual void regionOfInterestLeft(cv::Mat & srcImg, cv::Mat & dstImg) = 0;;
	virtual void regionOfInterestRight(cv::Mat & srcImg, cv::Mat & dstImg) = 0;
	virtual void regionOfInterest(cv::Mat & srcImg, cv::Mat & dstImg, cv::Point * maskPoly, int numMaskVertices) = 0;
	virtual void Hough(cv::Mat & srcImg, std::vector<cv::Vec4i> & lines, HoughParam hp) = 0;;
	virtual void AvgSlopeIntercept(cv::Mat & srcImg, 
	    				   std::vector<cv::Vec4i> & lines,
						   std::vector<cv::Vec4i> & right_lines, 
						   std::vector<cv::Vec4i> & left_lines) = 0;
    virtual void average_slope_intercept(cv::Mat & image, 
                             std::vector<cv::Vec4i> & lines,						   
                             cv::Vec4i & right_line, 
						     cv::Vec4i & left_line) = 0;
	virtual void drawLines(cv::Mat & srcImg, std::vector<cv::Vec4i> & lines) = 0;


};
}
#endif
