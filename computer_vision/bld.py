import os
import sys
import string
forge = sys.modules["forge"]

#opencv on Windows
# install vcpkg in your FE_WINDOWS_LOCAL
# see https://github.com/Microsoft/vcpkg/
# vcpkg.exe install opencv:x86-windows-static-md
# vcpkg.exe install opencv:x64-windows-static-md
# vcpkg.exe install opencv[vtk]:x86-windows-static-md
# vcpkg.exe install opencv[vtk]:x64-windows-static-md

def prerequisites():
	return [ "apiary" ]

def setup(module):
	srcList = [	"computer_vision.pmh",
				"ImageProcessor",
				"computer_visionDL" ]

	deplibs = forge.basiclibs + [
				"fePluginLib",
				"fexSignalLib",
				"fexNetworkDLLib",
				"hiveApiaryDLLib" ]

	dll = module.DLL( "hiveComputerVisionDL", srcList )

	dll.linkmap = {}

	if forge.fe_os == "FE_LINUX":
		dll.linkmap["opencv_libs"] = "-lopencv_core -lopencv_highgui -lopencv_imgcodecs -lopencv_videoio -lopencv_video -lopencv_photo -lopencv_imgproc"
	elif forge.fe_os == "FE_WIN32" or forge.fe_os == "FE_WIN64":
		dll.linkmap["opencv_libs"] = "zlib.lib lzma.lib libpng16.lib jpeg.lib GlU32.Lib  OpenGL32.Lib webp.lib tiff.lib \
				opencv_core.lib opencv_highgui.lib opencv_imgcodecs.lib opencv_videoio.lib opencv_video.lib opencv_imgproc.lib"

	forge.deps( ["hiveComputerVisionDLLib"], deplibs )

	forge.tests += [
		("inspect.exe",	"hiveComputerVisionDL",	None,	None) ]

	#module.Module('test')

def auto(module):
	if forge.fe_os == "FE_WIN32" or forge.fe_os == "FE_WIN64":
		forge.linkmap["opencv_libs"] = "zlib.lib lzma.lib libpng16.lib jpeg.lib GlU32.Lib  OpenGL32.Lib webp.lib tiff.lib \
							opencv_core.lib opencv_highgui.lib opencv_imgcodecs.lib opencv_videoio.lib opencv_video.lib opencv_imgproc.lib"
	else:
		forge.linkmap["opencv_libs"] =  "-lopencv_core -lopencv_highgui -lopencv_imgcodecs -lopencv_videoio -lopencv_video -lopencv_photo -lopencv_imgproc"

	test_file = """
#include <opencv2/opencv.hpp>
int main(void)
{
	return(0);
}
    \n"""

	result = forge.cctest(test_file)

	forge.linkmap.pop('opencv_libs', None)

	return result

