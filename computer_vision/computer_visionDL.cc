/*	Copyright (C) 2018 Arrival Ltd
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit git.hiverepo.com */

#include <computer_vision/computer_vision.pmh>
#include "platform/dlCore.cc"

using namespace fe;
using namespace hive;
extern "C"
{

//-----------------------------------------------------------------
// ListDependencies()
//-----------------------------------------------------------------
FE_DL_EXPORT void ListDependencies(List<String*>& list)
{
}


//-----------------------------------------------------------------
// CreateLibrary()
//-----------------------------------------------------------------

FE_DL_EXPORT Library* CreateLibrary(sp<Master> spMaster)
{
	Library *pLibrary = new Library();
	pLibrary->add<ImageProcessor>("ComputerVisionI.ImageProcessor.hive");
	return pLibrary;
}


//-----------------------------------------------------------------
// InitializeLibrary()
//-----------------------------------------------------------------

FE_DL_EXPORT void InitializeLibrary(sp<Library> spLibrary)
{
}

}
