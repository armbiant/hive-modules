/*	Copyright (C) 2018 Arrival Ltd
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit git.hiverepo.com */

#include "sensors/sensors.h"
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
//#include <opercv2/core/core_c.h>
#include <iostream>
using namespace std;
using namespace cv;

fe::String videoFile1 = "c:\\local\\Highway.mp4";
fe::String videoFile2 = "c:\\local\\solidYellowLeft.mp4";

int main()
{

    VideoCapture cap(videoFile2.c_str());
    if(!cap.isOpened())
    {
        cout<<"error";
        return -1;
    }
    Mat frame, whiteLane, yellowLane, LinesImg, HSV_Img, gray;

    while(1)
    {
        bool b = cap.read(frame);

        if(!b)
        {
            cout<<"err";
            cap.open(videoFile2.c_str());
            cap.read(frame);
            // break;
        }
        if (1)
        {
            // Convert to gray-scale            
            cvtColor(frame, gray, COLOR_BGR2GRAY);
            
            // Store the edges 
            Mat edges;
            // Find the edges in the image using canny detector
            Canny(gray, edges, 50, 200);
            // Create a vector to store lines of the image
            vector<Vec4i> lines;
            // Apply Hough Transform
            int threshold = 43;
            HoughLinesP(edges, lines, 1, CV_PI/180, threshold, 10, 250);
            // Draw lines on the image
            for (size_t i=0; i<lines.size(); i++) 
            {
                Vec4i l = lines[i];
                line(frame, Point(l[0], l[1]), Point(l[2], l[3]), Scalar(255, 0, 0), 3, LINE_AA);
            }
            // Show result image
            //imshow("Result Image", frame);
            
            namedWindow("frame");
            moveWindow("frame",275,30);
            imshow("frame",frame);

            namedWindow("gray");
            moveWindow("gray",1300,30);
            imshow("gray",gray);

            if(waitKey(30)==27)
            {
                cout<<"esc";
                break;
            }
        }
        else
        {
        }
    }

    return 0;
}

void doOriginal()
{
    /*
    //resize image
        resize(frame, frame, Size(640,480) );

        //here we define our region of interest
        //box(x, y, b, c);
        Rect const box(100, 295, 400, 185); //this mean the first corner is
                                             //(x,y)=(100,295)
                                            // and the second corner is
                                            //(x + b, y+c )= (100 +400,295+185)
        Mat ROI = frame(box);

        // convert our img to HSV Space
        cvtColor(ROI, HSV_Img, COLOR_RGB2HSV);

        //white color thresholding
        Scalar whiteMinScalar = Scalar(90,0,236);
        Scalar whiteMaxScalar = Scalar(111,93,255);

        inRange(HSV_Img, whiteMinScalar, whiteMaxScalar, whiteLane);

        //yellow color thresholding
        //Scalar yellowMinScalar = Scalar(81,119,200);
        //Scalar yellowMaxScalar = Scalar(101,255,255);
        Scalar yellowMinScalar = Scalar(90,0,236);
        Scalar yellowMaxScalar = Scalar(111,93,255);

        inRange(HSV_Img, yellowMinScalar, yellowMaxScalar, yellowLane);

        //combine our two images in one image
        addWeighted(whiteLane, 1.0, yellowLane, 1.0, 0.0, LinesImg);
        // Edge detection using canny detector
        int minCannyThreshold = 190;
        int maxCannyThreshold = 230;
        Canny(LinesImg, LinesImg, minCannyThreshold, maxCannyThreshold, 5, true);

        // Morphological Operation
        Mat k=getStructuringElement(cv::MORPH_RECT,Size(9,9)); //MATLAB :k=Ones(9)
        morphologyEx(LinesImg, LinesImg, MORPH_CLOSE, k);

        // now applying hough transform TO dETECT Lines in our image
        vector<Vec4i> lines;
        double rho = 1;
        double theta = CV_PI/180;
        int threshold = 43;
        double minLinLength = 35;
        double maxLineGap = 210;

        HoughLinesP(LinesImg, lines, rho, theta, threshold, minLinLength, maxLineGap );

        //draw our lines

        for( size_t i = 0; i < lines.size(); i++ )
        {
            Vec4i l = lines[i];  // we have 4 elements p1=x1,y1  p2= x2,y2
            Scalar greenColor= Scalar(0,250,30);  // B=0 G=250 R=30
            //line( ROI, Point(l[0], l[1]), Point(l[2], l[3]), greenColor, 3, CV_AA);
            line( ROI, Point(l[0], l[1]), Point(l[2], l[3]), greenColor, 3, cv::LineTypes::LINE_AA);

        }
        //
        namedWindow("frame");
        moveWindow("frame",275,30);
        imshow("frame",frame);

       // 

       // 
        namedWindow("WhiteLane");
        moveWindow("WhiteLane",950,280);
        imshow("WhiteLane",whiteLane);
        //
        //
        namedWindow("YellowLane");
        moveWindow("YellowLane",950,30);
        imshow("YellowLane",yellowLane);
        //

        if(waitKey(30)==27)
        {
            cout<<"esc";
            break;
        }
      */  
}
