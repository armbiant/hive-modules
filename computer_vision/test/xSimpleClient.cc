/*	Copyright (C) 2018 Arrival Ltd
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit git.hiverepo.com */

/** @file 
 * 
 * 
 * This is an integration test: Needs Unreal-Hive and Camera Sensors
 * 
 * 
*/

// cd unreal-devbot\Plugins\FreeElectron\Source\ThirdParty\FreeElectronLibrary\FE

#include "sensors/sensors.h"
#include "computer_vision/computer_vision.pmh"
#include "math/math.h"
#include <map>
#include <iostream>
#include <thread>

using namespace fe;
using namespace hive;


//#define SINGLE_THREAD
#ifndef SINGLE_THREAD
#define MULTI_THREAD
#endif

//-----------------------------------------------------------------
//                      CAMERA HANDLER
//-----------------------------------------------------------------
#define NUM_CAMERAS     2
#define SURFACE_HEIGHT	144
#define SURFACE_WIDTH	256

class CameraHandler
{
public:    
    CameraHandler(SensorID_t sType);
    ~CameraHandler();
    void start();
	bool isRunning(){return _running;}
	bool isDataReady(){return _dataReady;}
	void resetDataReady(){_dataReady = false;}
	cv::Mat & getAppData(){return _appImg;}

private:
    void execute();

    std::thread      *          _pWorkerThread;
    //SensorNetCatalogSubscriber  _subscriber;
	fe::sp<hive::SensorSubscriberI>  _subscriber;
    ImageProcessor      _isp;
	Array<U8>		    _data;
	cv::Mat             _readImg;
	cv::Mat             _workImg;
	cv::Mat             _appImg;
	SensorID_t			_type;
	bool				_running;
	bool				_executionDone;
	bool				_dataReady;
};

std::map<SensorID_t, std::string> camOutputName;

//-----------------------------------------------------------------
// CameraHandler::CameraHandler()
//-----------------------------------------------------------------
CameraHandler::CameraHandler(SensorID_t sType)
  : _pWorkerThread(NULL), 
   	_readImg(SURFACE_HEIGHT, SURFACE_WIDTH, CV_8UC4, cv::Scalar::all(0)),
	_workImg(SURFACE_HEIGHT, SURFACE_WIDTH, CV_8UC4, cv::Scalar::all(0)),
	_appImg(SURFACE_HEIGHT, SURFACE_WIDTH, CV_8UC4, cv::Scalar::all(0)),
  	_running(false), _type(sType), _dataReady(false)
{
	fe::sp<hive::HiveHost> spHiveHost(hive::HiveHost::create());
	fe::sp<fe::Master> spMaster=spHiveHost->master();
	fe::sp<fe::Registry> spRegistry=spMaster->registry();

	spRegistry->manage("feAutoLoadDL");
	spRegistry->manage("hiveApiaryDL");

	_subscriber = spRegistry->create("SensorSubscriberI.SensorZeromqSubscriber");

	if(_subscriber.isValid())
		feLog("Ready\n");
}

//-----------------------------------------------------------------
// CameraHandler::~CameraHandler()
//-----------------------------------------------------------------
CameraHandler::~CameraHandler()
{
	_running = false;
	
	if(_pWorkerThread != NULL)
	{
		while (!_executionDone)
		{
			std::this_thread::sleep_for(std::chrono::milliseconds(100));
		}
		delete _pWorkerThread;
		_pWorkerThread = NULL;
	}
}

//-----------------------------------------------------------------
// void CameraHandler::start()
//-----------------------------------------------------------------
void CameraHandler::start()
{
	std::string topic;
	std::string addr;
	switch (_type)
    {
        case eCameraFrontCenter: topic = "cam_front_center"; addr = "tcp://127.0.0.1:7791"; break;
        case eCameraFrontLeft:   topic = "cam_front_left";   addr = "tcp://127.0.0.1:7792"; break;
        case eCameraFrontRight:  topic = "cam_front_right";  addr = "tcp://127.0.0.1:7790"; break;
        default:                 topic = "cam_front_center"; addr = "tcp://127.0.0.1:7791"; break;
    }	
	_running = true;
	_subscriber->start(topic, addr);

#ifdef MULTI_THREAD
    if(_pWorkerThread == NULL) 
	{
        _pWorkerThread = new std::thread(&CameraHandler::execute, this);
    }
#endif	
}

//-----------------------------------------------------------------
// void CameraHandler::execute()  Thread Function
//-----------------------------------------------------------------
void CameraHandler::execute()
{    
	cv::namedWindow(camOutputName[_type]);
	feLog("Thread started %s\n", camOutputName[_type]);

	_data.resize(SURFACE_HEIGHT * SURFACE_WIDTH	* 4);

	static int cnt = 0;
	_running = true;
	while(_running)
	{
		_subscriber->listen(camOutputName[_type], _data);	// We need to get image dimensions				
		if (_data.size() <= 0)
		{
			std::this_thread::sleep_for(std::chrono::milliseconds(1));
			continue;
		}
			

		//feLog("r %s\n",camOutputName[_type]);

		memcpy(_readImg.data, _data.data(), _data.size());		

		cv::Size kernelSzGaussianBlur(3,3);
		_isp.canny(_readImg, _workImg, kernelSzGaussianBlur);

		if(_type == eCameraFrontLeft)
		{
   			cv:: Point points[4] = { cv::Point(0, SURFACE_HEIGHT),			// bottom_left
                            cv::Point(0, SURFACE_HEIGHT/2+10),				// top_left
                            cv::Point(SURFACE_WIDTH, SURFACE_HEIGHT/2+10),	// top_right
                            cv::Point(SURFACE_WIDTH/2, SURFACE_HEIGHT)		// bottom_right
                            };			
			_isp.regionOfInterest(_workImg, _workImg, points, 4);
		}
		else
		{
			cv:: Point points[4] = { cv::Point(SURFACE_WIDTH/2, SURFACE_HEIGHT),// bottom_left
							cv::Point(0, SURFACE_HEIGHT/2+10),	        		// top_left
							cv::Point(SURFACE_WIDTH, SURFACE_HEIGHT/2+10),	    // top_right
							cv::Point(SURFACE_WIDTH, SURFACE_HEIGHT)		    // bottom_right
						};
			_isp.regionOfInterest(_workImg, _workImg, points, 4);
		}

		HoughParam hp2;
		hp2.rho = 1;
		hp2.threshold = 20;
		hp2.minLineLen = 20;
		hp2.maxLineGap = 300;
		std::vector<cv::Vec4i> lines;
		_isp.Hough(_workImg, lines, hp2);

		// Draw Lines	
		std::vector<cv::Vec4i> right_lines, left_lines;
		cv::Vec4i right_line, left_line;
		_isp.average_slope_intercept(_workImg, lines, right_line, left_line);
		
		right_lines.push_back(right_line);
		left_lines.push_back(left_line);
		_isp.drawLines(_readImg, right_lines);
		_isp.drawLines(_readImg, left_lines);
		
		/*
		if(_dataReady == false)
		{
			memcpy(_appImg.data, _readImg.data, _data.size());		
			_dataReady = true;
		}
		*/
		cv::imshow(camOutputName[_type], _readImg);
		cv::waitKey(1);

	}
	std::cout << "received finished message" << std::endl;
    _pWorkerThread->join();
	_executionDone = true;
}

//=================================================================
//							MAIN()
//=================================================================
int main(int argc,char** argv)
{
	camOutputName[eCameraFrontRight]  = "cam_front_right";
	camOutputName[eCameraFrontLeft]   = "cam_front_left";
	camOutputName[eCameraFrontCenter] = "cam_front_center";
	
	CameraHandler ch1(eCameraFrontLeft);
	CameraHandler ch2(eCameraFrontRight);
	ch1.start();
	ch2.start();
/*
	cv::namedWindow(camOutputName[eCameraFrontLeft]);
	cv::namedWindow(camOutputName[eCameraFrontRight]);

	while (ch1.isRunning() || ch2.isRunning())
	{
		if(ch1.isDataReady())					// Wait on multiple objects
		{
			cv::imshow(camOutputName[eCameraFrontLeft], ch1.getAppData());
			cv::waitKey(1);		
			ch1.resetDataReady();
		}
		if(ch2.isDataReady())
		{
			cv::imshow(camOutputName[eCameraFrontRight], ch2.getAppData());
			cv::waitKey(1);		
			ch2.resetDataReady();
		}
		std::this_thread::sleep_for(std::chrono::milliseconds(1));
	}
*/
	while (1) std::this_thread::sleep_for(std::chrono::milliseconds(20));
	std::cout << "Main is done" << std::endl;
	return 0;
}
