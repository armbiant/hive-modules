/*	Copyright (C) 2018 Arrival Ltd
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit git.hiverepo.com */

/** @file */

#include "driving_test/driving_test.pmh"

using namespace fe;
using namespace fe::ext;

namespace hive
{

DrivingTestLua::DrivingTestLua():
	m_started(FALSE)
{
};

void DrivingTestLua::initialize(void)
{
	m_spHiveHost=HiveHost::create();
	FEASSERT(m_spHiveHost.isValid());

	m_spStateCatalog=m_spHiveHost->accessSpace("world");

	m_spLuaHandler=registry()->create("*.LuaHandler");

	m_spNetDispatch=registry()->create("*.NetDispatch");
}

void DrivingTestLua::handle(Record& a_signal)
{
//	feLog("DrivingTestLua::handle layout \"%s\"\n",
//			a_signal.layout()->name().c_str());

	if(m_spStateCatalog.isNull())
	{
		feLog("DrivingTestLua::handle no StateCatalog\n");
		return;
	}

	if(m_spLuaHandler.isNull())
	{
		feLog("DrivingTestLua::handle no LuaHandler\n");
		return;
	}

	sp<LuaI> spLuaI(m_spLuaHandler);

	if(!m_started)
	{
		String paths;
		Result result=m_spStateCatalog->getState<String>("paths.lua",paths);

		String script;
		result=m_spStateCatalog->getState<String>(
				"scene.driving_test:script",script);

		String path=System::findFile(script,paths);
		if(path.empty())
		{
			feLog("OpDriver::handle"
					" failed to find \"%s\" using paths \"%s\"\n",
					script.c_str(),paths.c_str());
			return;
		}

		String fullname=path+"/"+script;

		if(spLuaI.isValid())
		{
			spLuaI->loadFile(fullname);
		}

		m_started=TRUE;
	}

	if(spLuaI.isValid())
	{
		spLuaI->set("hive",sp<Component>(m_spNetDispatch));
//		spLuaI->set("world",m_spStateCatalog);
	}

	m_spLuaHandler->handle(a_signal);
}

}
