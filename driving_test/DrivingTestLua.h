/*	Copyright (C) 2018 Arrival Ltd
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit git.hiverepo.com */

/** @file */

#ifndef __driving_test_DrivingTestLua_h__
#define __driving_test_DrivingTestLua_h__

namespace hive
{

class FE_DL_EXPORT DrivingTestLua:
	virtual public fe::ext::HandlerI,
	public fe::Initialize<DrivingTestLua>
{
	public:
				DrivingTestLua(void);
virtual			~DrivingTestLua(void)										{}

virtual	void	initialize(void);

virtual	void	bind(fe::sp<fe::ext::SignalerI> spSignalerI,
					fe::sp<fe::Layout> spLayout)							{}
virtual	void	handle(fe::Record& a_signal);

	private:

		fe::sp<HiveHost>			m_spHiveHost;
		fe::sp<fe::ext::DispatchI>	m_spNetDispatch;
		fe::sp<fe::StateCatalog>	m_spStateCatalog;

		fe::sp<fe::ext::HandlerI>	m_spLuaHandler;

		BWORD						m_started;
};

}
#endif
