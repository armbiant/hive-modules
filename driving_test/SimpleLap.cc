/*	Copyright (C) 2018 Arrival Ltd
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit git.hiverepo.com */

/** @file */

#include "driving_test/driving_test.pmh"

using namespace fe;
using namespace fe::ext;

namespace hive
{

SimpleLap::SimpleLap():
	m_started(FALSE),
	m_timeElapsed(0),
	m_lapCompleted(FALSE),
	m_finishLocation(0,0,0)
{
};

void SimpleLap::initialize(void)
{
	m_spHiveHost=HiveHost::create();
	FEASSERT(m_spHiveHost.isValid());

	m_spStateCatalog=m_spHiveHost->accessSpace("world");
}

void SimpleLap::handle(Record& a_signal)
{
//	feLog("SimpleLap::handle layout \"%s\"\n",
//			a_signal.layout()->name().c_str());

	if(m_spStateCatalog.isNull())
	{
		feLog("SimpleLap::handle no StateCatalog\n");
		return;
	}

	if(!m_started)
	{
		Result result=m_spStateCatalog->getState<SpatialVector>(
				"track.finish.location",m_finishLocation);
		m_started=TRUE;
	}

	const Real timeStart(16);
	const Real timeMin(20);
	const Real timeMax(160);
	const Real finishThreshold(4);

	if(m_asAdsSafetyState.bind(a_signal))
	{
		if(m_lapCompleted || m_timeElapsed<timeStart || m_timeElapsed>timeMax)
		{
			m_asAdsSafetyState.driveMode(a_signal)=
					AdsVehicleSafetyState::e_stop;
		}
		else
		{
			m_asAdsSafetyState.driveMode(a_signal)=
					AdsVehicleSafetyState::e_go;
		}

//		m_asAdsSafetyState.controlMode(a_signal)=
//				AdsVehicleSafetyState::e_prohibited;
		m_asAdsSafetyState.controlMode(a_signal)=
				AdsVehicleSafetyState::e_allowed;

		return;
	}

	if(!m_asVehicle.bind(a_signal))
	{
		return;
	}

	const Real deltaTime=m_asVehicle.deltaTime(a_signal);
	m_timeElapsed+=deltaTime;

	const SpatialTransform chassisTransform=
			m_asVehicle.chassisTransform(a_signal);
	const Real finishDistance=
			magnitude(chassisTransform.translation()-m_finishLocation);
	if(m_timeElapsed>timeMin && finishDistance<finishThreshold)
	{
		m_lapCompleted=TRUE;
	}

	const SpatialVector centerOfMassVelocity=
			m_asVehicle.centerOfMassVelocity(a_signal);
	if(m_lapCompleted && magnitude(centerOfMassVelocity)<1e-3)
	{
		m_asVehicle.gearModeInput(a_signal)=AsVehicle::e_gearModePark;
	}
}

}
