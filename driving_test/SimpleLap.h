/*	Copyright (C) 2018 Arrival Ltd
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit git.hiverepo.com */

/** @file */

#ifndef __driving_test_SimpleLap_h__
#define __driving_test_SimpleLap_h__

namespace hive
{

class FE_DL_EXPORT SimpleLap:
	virtual public fe::ext::HandlerI,
	public fe::Initialize<SimpleLap>
{
	public:
				SimpleLap(void);
virtual			~SimpleLap(void)											{}

virtual	void	initialize(void);

virtual	void	bind(fe::sp<fe::ext::SignalerI> spSignalerI,
					fe::sp<fe::Layout> spLayout)							{}
virtual	void	handle(fe::Record& a_signal);

	private:
		fe::sp<HiveHost>			m_spHiveHost;
		fe::sp<fe::StateCatalog>	m_spStateCatalog;

		AsAdsSafetyState			m_asAdsSafetyState;
		AsVehicle					m_asVehicle;

		BWORD						m_started;
		fe::Real					m_timeElapsed;
		BWORD						m_lapCompleted;
		fe::SpatialVector			m_finishLocation;
};

}
#endif
