/*	Copyright (C) 2018 Arrival Ltd
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit git.hiverepo.com */

#pragma once

using namespace fe;

namespace hive
{

class FileChangeHandlerI
{
public:

    enum Action
    {
        // Sent when a file is created or renamed
        Add = 1,
        // Sent when a file is deleted or renamed
        Delete = 2,
        // Sent when a file is modified
        Modified = 4
    };

    virtual void handleFileChange(const String &dir, const String &filename, Action action) = 0;
};

} // namespace hive

