/*	Copyright (C) 2018 Arrival Ltd
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit git.hiverepo.com */

#include "filewatcher.pmh"

using namespace fe;

namespace hive
{

FileWatcherDaemon::FileWatcherDaemon()
{}

void FileWatcherDaemon::start(const unsigned int updateDelay)
{
    m_fileWatcherTask.m_updateDelay = updateDelay;

    m_pFileWatcherThread = new Thread(&m_fileWatcherTask);
}

void FileWatcherDaemon::watchFile(const String &file, FileChangeHandlerI* handler)
{
    String filename = file.basename();
    String directory = file.pathname();
    if (directory == "")
        directory = ".";

    if (m_directoryListeners.find(directory) == m_directoryListeners.end())
    {
        DirectoryListener *directoryListener = new DirectoryListener();
        directoryListener->addHandler(filename, handler);
        m_directoryListeners[directory] = directoryListener;

        m_fileWatcherTask.addListener(directory, directoryListener);
    }
    else
    {
        m_directoryListeners[directory]->addHandler(filename, handler);
    }
}

void FileWatcherDaemon::watchDirectory(const String &directory, FileChangeHandlerI *handler)
{
    if (m_directoryListeners.find(directory) == m_directoryListeners.end())
    {
        DirectoryListener *directoryListener = new DirectoryListener();
        directoryListener->addHandler(".", handler);
        m_directoryListeners[directory] = directoryListener;

        m_fileWatcherTask.addListener(directory, directoryListener);
    }
    else
    {
        m_directoryListeners[directory]->addHandler(".", handler);
    }
}

void FileWatcherDaemon::stop()
{
    m_fileWatcherTask.m_poison.start();
    m_pFileWatcherThread->join();
    delete m_pFileWatcherThread;
}

FileWatcherDaemon::~FileWatcherDaemon()
{
    stop();
}

void FileWatcherDaemon::FileWatcherTask::operate()
{
    while (!m_poison.active())
    {
        if (m_listenerBuffer.size() > 0)
        {
            // Loop over each listener in the buffer, add it to the watcher and remove it.
            for (std::map<String, DirectoryListener *>::iterator itr = m_listenerBuffer.begin(); itr != m_listenerBuffer.end(); m_listenerBuffer.erase(itr++))
            {
                m_fileWatcher.addWatch(itr->first.c_str(), itr->second);
            }
        }

        m_fileWatcher.update();

        if (m_updateDelay > 0)
        {
            milliSleep(m_updateDelay);
        }
    }
}

void FileWatcherDaemon::FileWatcherTask::addListener(const String &directory, DirectoryListener *listener)
{
    m_listenerBuffer[directory] = listener;
}

void FileWatcherDaemon::DirectoryListener::addHandler(const String &filename, FileChangeHandlerI *feFileChangeHandler)
{
    m_handlers[filename] = feFileChangeHandler;
}

void FileWatcherDaemon::DirectoryListener::handleFileAction(FW::WatchID watchid, const FW::String &dir, const FW::String &filename, FW::Action action)
{    
    FileChangeHandlerI::Action feAction;
    switch (action)
    {
    case FW::Action::Add:
        feAction = FileChangeHandlerI::Action::Add;
        break;
    case FW::Action::Delete:
        feAction = FileChangeHandlerI::Action::Delete;
        break;
    case FW::Action::Modified:
        feAction = FileChangeHandlerI::Action::Modified;
        break;
    }

    String feFilename(filename.c_str());

    if (m_handlers.find(feFilename) != m_handlers.end())
    {
        m_handlers[feFilename]->handleFileChange(dir.c_str(), filename.c_str(), feAction);
    }

    if (m_handlers.find(".") != m_handlers.end())
    {
        m_handlers["."]->handleFileChange(dir.c_str(), filename.c_str(), feAction);
    }
}

} // namespace hive
