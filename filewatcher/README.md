# File Watcher

The File Watcher module provides a Daemon for monitoring a directory or file for changes.

## Usage

You must first create a "FileChangeHandler" that will be executed whenever a file change is detected. This is where your file handling logic should go.

```c++
#include "filewatcher/filewatcher.h"

class ExampleFileChangeHandler : virtual public hive::FileChangeHandlerI
{
    void handleFileChange(const fe::String &dir, const fe::String &filename, hive::FileChangeHandlerI::Action action)
    {
        if (action == hive::FileChangeHandlerI::Action::Add)
            feLog("File %s added to %s\n", filename.c_str(), dir.c_str());
    }
};
```

Now once you create your file watcher daemon you can add your FileChangeHandler to any file or directory.

```c++
#include "filewatcher/filewatcher.h"

fe::sp<hive::FileWatcherDaemonI> spfileWatcher = spRegistry->create("FileWatcherDaemonI.*");
FEASSERT(spfileWatcher.isValid());

// Start the File Watcher with an update every 1000ms.
spfileWatcher->start(1000); 

// Watch for changes for a specific file.
spfileWatcher->watchFile("file1.txt", new ExampleFileChangeHandler());

// Watch for changes for a whole directory.
spfileWatcher->watchDirectory("./testdir", new ExampleFileChangeHandler());
```



