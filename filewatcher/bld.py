import os
import sys
forge = sys.modules["forge"]


def setup(module):

	module.includemap = { "simplefilewatcher" : os.path.join(module.modPath,'simplefilewatcher/include') }

	srcList = [
		"simplefilewatcher/source/FileWatcher.cpp",
		"simplefilewatcher/source/FileWatcherLinux.cpp",
		"simplefilewatcher/source/FileWatcherOSX.cpp",
		"simplefilewatcher/source/FileWatcherWin32.cpp",
		"filewatcher.pmh",
		"FileWatcherDL",
		"FileWatcherDaemon"
	]

	module.DLL("FileWatcherDL", srcList)

	forge.deps(["FileWatcherDLLib"], forge.corelibs)

	forge.tests += [
		("inspect.exe",	"FileWatcherDL", None, None),
	]

	module.Module('test')
