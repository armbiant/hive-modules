/*	Copyright (C) 2018 Arrival Ltd
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit git.hiverepo.com */

#pragma once

#include "fe/plugin.h"

#include "FileChangeHandlerI.h"
#include "FileWatcherDaemonI.h"
