import sys
forge = sys.modules["forge"]

import os.path

def setup(module):

	module.includemap = { "simplefilewatcher" : os.path.join(module.modPath,'../simplefilewatcher/include') }

	srcList = [
		"../simplefilewatcher/source/FileWatcher.cpp",
		"../simplefilewatcher/source/FileWatcherLinux.cpp",
		"../simplefilewatcher/source/FileWatcherOSX.cpp",
		"../simplefilewatcher/source/FileWatcherWin32.cpp",
		"xSimpleFileWatcherDemo"
	]

	# Unit tests
	module.Exe("xFileWatcherDaemonTest")
	module.Exe("xSimpleFileWatcherDemo", srcList) # This one isn't a test but a demo

	forge.deps([
		"xFileWatcherDaemonTestExe"
	], forge.corelibs)

	forge.deps([
		"xSimpleFileWatcherDemoExe"
	], forge.corelibs)

	# Autorun unit tests
	forge.tests += [
		("xFileWatcherDaemonTest.exe", "", None, None)
	]
