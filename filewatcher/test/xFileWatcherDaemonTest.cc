/*	Copyright (C) 2018 Arrival Ltd
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit git.hiverepo.com */

#include "filewatcher/filewatcher.h"

using namespace fe;
using namespace hive;

UNIT_START();
int file1_triggers = 0;
int file2_triggers = 0;
int file3_triggers = 0;

/// Processes a file action
class TestFileWatchHandler : virtual public FileChangeHandlerI
{
public:
    void handleFileChange(const String &dir, const String &filename, FileChangeHandlerI::Action action)
    {
        if (action == FileChangeHandlerI::Action::Add)
        {
            feLog("File %s added\n", filename.c_str());

            if (filename == "file1.txt")
                file1_triggers++;
            if (filename == "file2.txt")
                file2_triggers++;
            if (filename == "file3.txt")
                file3_triggers++;
        }
    }
};

int main(int argc, char **argv)
{
    remove("test/file1.txt");
    remove("test/file2.txt");
    remove("test/file3.txt");

    sp<Master> spMaster(new Master);
    sp<Registry> spRegistry = spMaster->registry();
    Result result = spRegistry->manage("feAutoLoadDL");

    {
        // Create hot reload daemon.
        sp<FileWatcherDaemonI> spfileWatcher = spRegistry->create("FileWatcherDaemonI.*");
        FEASSERT(spfileWatcher.isValid());
        
        spfileWatcher->start(0);
        spfileWatcher->watchFile("test/file1.txt", new TestFileWatchHandler());
        spfileWatcher->watchFile("test/file2.txt", new TestFileWatchHandler());
        spfileWatcher->watchDirectory("test", new TestFileWatchHandler());

        // Allow file watcher thread to spool up to avoid race conditions for this unit test.
        // Not neccessary in typical applications.
        milliSleep(1);

        // Write file.
        std::ofstream file1;
        file1.open("test/file1.txt");
        file1 << "Hello World";
        file1.close();

        std::ofstream file2;
        file2.open("test/file2.txt");
        file2 << "Hello World";
        file2.close();

        std::ofstream file3;
        file3.open("test/file3.txt");
        file3 << "Hello World";
        file3.close();

        // The destruction of the daemon forces it to join the file watcher thread which ensures that it updates.
    }

    UNIT_TEST(file1_triggers == 2);
    UNIT_TEST(file2_triggers == 2);
    UNIT_TEST(file3_triggers == 1);

    remove("test/file1.txt");
    remove("test/file2.txt");
    remove("test/file3.txt");

    UNIT_TRACK(3);
    UNIT_RETURN();
}
