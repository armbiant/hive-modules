/*	Copyright (C) 2018 Arrival Ltd
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit git.hiverepo.com */

#include "FileWatcher/SimpleFileWatcher.h"
#include <iostream>
#include <fstream>

#include "fe/plugin.h"

using namespace fe;

/// Processes a file action
class UpdateListener : public FW::FileWatchListener
{
public:
    UpdateListener() {}
    void handleFileAction(FW::WatchID watchid, const FW::String &dir, const FW::String &filename,
                          FW::Action action)
    {
        std::cout << "DIR (" << dir + ") FILE (" + filename + ") has event " << action << std::endl;
    }
};

int main(int argc, char **argv)
{
    remove("test/file1.txt");

    try
    {
        // create the file watcher object
        FW::FileWatcher fileWatcher;

        // add a watch to the system
        FW::WatchID watchID = fileWatcher.addWatch("test", new UpdateListener(), true);

        feLog("Press ^C to exit demo\n");

        fileWatcher.update();

        std::ofstream file1;
        file1.open("test/file1.txt");
        file1 << "Hello World";
        file1.close();

        fileWatcher.update();
    }
    catch (std::exception &e)
    {
        fprintf(stderr, "An exception has occurred: %s\n", e.what());
    }

    remove("test/file1.txt");

    return 0;
}
