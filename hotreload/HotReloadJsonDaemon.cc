/*	Copyright (C) 2018 Arrival Ltd
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit git.hiverepo.com */

#include "hotreload.pmh"

#define HOTRELOAD_DEBUG FALSE

using namespace fe;

namespace hive
{

void HotReloadJsonDaemon::initialize(void)
{
    m_manifestFilepath = System::getModulePath().pathname() + "/" + HOTRELOAD_MANIFEST_FILENAME + System::getBinarySuffix() + ".json";

    m_spFileWatcher = create("FileWatcherDaemonI.*");
    FEASSERT(m_spFileWatcher.isValid());
}

void HotReloadJsonDaemon::start(const unsigned int updateDelay)
{
    HotReloadJsonDaemon::JsonHotReloader *jsonHotReloader = new JsonHotReloader(registry());

    // Check for hotreload manifest and load it straightaway if it exists.
    std::ifstream file(m_manifestFilepath.c_str());
    if (file.is_open())
    {
        jsonHotReloader->hotreloadFromFile(file);
    }
    file.close();

    // Start file watcher to watch for manifest changes.
    m_spFileWatcher->start(updateDelay);
    m_spFileWatcher->watchFile(m_manifestFilepath, jsonHotReloader);

#if HOTRELOAD_DEBUG
    feLog("HotReload starting file watcher on %s\n", m_manifestFilepath.c_str());
#endif
}

HotReloadJsonDaemon::JsonHotReloader::JsonHotReloader(sp<Registry> spRegistry)
{
    m_spRegistry = spRegistry;
}

void HotReloadJsonDaemon::JsonHotReloader::handleFileChange(const String &dir, const String &filename, FileChangeHandlerI::Action action)
{
    if (action == FileChangeHandlerI::Action::Modified || action == FileChangeHandlerI::Action::Add)
    {
        String filepath = dir + "/" + filename;
        std::ifstream file(filepath.c_str());
        if (!file.is_open())
        {
            feX("HotReloadJsonDaemon::JsonHotReloader::handleFileChange failed to open \"%s\"\n", filepath.c_str());
        }

        hotreloadFromFile(file);
        file.close();
    }
}

void HotReloadJsonDaemon::JsonHotReloader::hotreloadFromFile(std::ifstream &file)
{
    // Here's what the json is expected to look like:
    // {
    //     "Modules":
    //     {
    //         "HotReloadTestDL" : "HotReloadTestDL-2"
    //     }
    // }

#if HOTRELOAD_DEBUG
    feLog("HotReload detected new manifest:\n");
#endif

    Json::Value json = new Json::Value();
    Json::Reader jsonReader;
    jsonReader.parse(file, json);

    if (json.isObject() && json.isMember("Modules") && json["Modules"].isObject())
    {
        for (Json::Value::const_iterator entry = json["Modules"].begin();
             entry != json["Modules"].end(); entry++)
        {
            String oldDlName = entry.key().asCString();
            String newDlName = (*entry).asCString();

#if HOTRELOAD_DEBUG
            feLog("    %s -> %s\n", oldDlName.c_str(), newDlName.c_str());
#endif

            m_spRegistry->substituteLibrary(oldDlName, newDlName);
        }
    }
}

} // namespace hive
