/*	Copyright (C) 2018 Arrival Ltd
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit git.hiverepo.com */

#pragma once

using namespace fe;

namespace hive
{

class FE_DL_EXPORT HotReloadJsonDaemon : virtual public HotReloadDaemonI,
                                         public Initialize<HotReloadJsonDaemon>
{
public:
    void initialize(void);

    void start(const unsigned int updateDelay = 1000) override;

    void setCustomManifestFilepath(const String &manifestFilepath) override
    { 
        m_manifestFilepath = manifestFilepath; 
    };

private:
    sp<Registry> m_spRegistry;
    sp<FileWatcherDaemonI> m_spFileWatcher;
    String m_manifestFilepath;

    class JsonHotReloader : virtual public FileChangeHandlerI
    {
    public:
        JsonHotReloader(sp<Registry> spRegistry);
        void handleFileChange(const String &dir, const String &filename, FileChangeHandlerI::Action action);
        void hotreloadFromFile(std::ifstream &file);

    private:
        sp<Registry> m_spRegistry;
    };
};

} // namespace hive
