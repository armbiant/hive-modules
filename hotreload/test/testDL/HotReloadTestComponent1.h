#pragma once

namespace hive
{
	
class FE_DL_EXPORT HotReloadTestComponent : virtual public HotReloadTestComponentI
{
public:
	fe::String getText() override
	{
		return "OUTPUT FROM DL NUMBER 1";
	};
};

} // namespace hive