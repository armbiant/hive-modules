#pragma once

namespace hive
{

class FE_DL_EXPORT HotReloadTestComponentI : virtual public fe::Component,
	public fe::CastableAs<HotReloadTestComponentI>
{
public:
	virtual fe::String getText() = 0;
};

} // namespace hive
