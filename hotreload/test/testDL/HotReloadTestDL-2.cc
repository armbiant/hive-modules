/** @file */

#include "hotreloadtest2.pmh"

#include "platform/dlCore.cc"

using namespace fe;
using namespace hive;

extern "C"
{
    FE_DL_EXPORT void ListDependencies(List<String*>& list)
    {
        list.append(new String("feDataDL"));
    }

    FE_DL_EXPORT Library* CreateLibrary(sp<Master> spMaster)
    {
        Library *pLibrary = new Library();
        pLibrary->add<HotReloadTestComponent>("HotReloadTestComponentI.HotReloadTestComponent.hive");
        return pLibrary;
    }

    FE_DL_EXPORT void InitializeLibrary(sp<Library> spLibrary)
    {
    }
}
