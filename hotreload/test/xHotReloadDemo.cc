/*	Copyright (C) 2018 Arrival Ltd
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit git.hiverepo.com */

#include "hotreload/hotreload.h"
#include "testDL/hotreloadtest.h"

using namespace fe;
using namespace hive;

int main(int argc, char **argv)
{
    sp<Master> spMaster(new fe::Master);
    sp<Registry> spRegistry = spMaster->registry();
    Result result = spRegistry->manage("feAutoLoadDL");

    // Create hot reload daemon.
    sp<HotReloadDaemonI> hotreloadDaemon = spRegistry->create("*.HotReloadJsonDaemon");
    FEASSERT(hotreloadDaemon.isValid());
    hotreloadDaemon->start(0);

    feLog("Go and modify the output in 'hive/hotreload/test/testDL/HotReloadTestComponent1.h', build it and see it change live!\n\n");

    while (true)
    {
        {
            // Create component and output implementation.
            sp<HotReloadTestComponentI> testComponent = spRegistry->create("*.HotReloadTestComponent");
            FEASSERT(testComponent.isValid());
            String string1 = testComponent->getText().c_str();
            feLog("%s\n", string1.c_str());
        }

        milliSleep(1000);
    }
}
