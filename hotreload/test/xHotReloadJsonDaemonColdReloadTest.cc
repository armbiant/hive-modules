/*	Copyright (C) 2018 Arrival Ltd
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit git.hiverepo.com */

// Test "coldreloading" the Registry with the HotReloadJsonDaemon.
// Coldreloading means that the Daemon should check for an existing
// hotreload manifest file on startup and "coldreload" the libraries
// before they're even loaded.

#include "hotreload/hotreload.h"
#include "testDL/hotreloadtest.h"

using namespace fe;
using namespace hive;

int main(int argc, char **argv)
{
    UNIT_START();

    const char *manifestFilepath = "test/FEhotreload.json";

    remove(manifestFilepath);

    // Write hotreload manifest to file.
    const std::string hotreloadManifest =
R"({
    "Modules": {
        "HotReloadTestDL": "HotReloadTestDL-2"
    }
})";
    std::ofstream file;
    file.open(manifestFilepath);
    file << hotreloadManifest;
    file.close();

    sp<Master> spMaster(new fe::Master);
    sp<Registry> spRegistry = spMaster->registry();
    Result result = spRegistry->manage("feAutoLoadDL");

    // Create hot reload daemon.
    sp<HotReloadDaemonI> hotreloadDaemon = spRegistry->create("*.HotReloadJsonDaemon");
    FEASSERT(hotreloadDaemon.isValid());
    hotreloadDaemon->setCustomManifestFilepath(manifestFilepath);
    hotreloadDaemon->start(0);

    // Create first implementation of component. It should already be "coldreloaded".
    sp<HotReloadTestComponentI> testComponent2 = spRegistry->create("*.HotReloadTestComponent");
    FEASSERT(testComponent2.isValid());
    String string2 = testComponent2->getText().c_str();
    feLog("%s\n", string2.c_str());
    UNIT_TEST(string2 == "OUTPUT FROM DL NUMBER 2");

    remove(manifestFilepath);

    UNIT_TRACK(1);
    UNIT_RETURN();
}
