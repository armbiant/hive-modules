/*	Copyright (C) 2018 Arrival Ltd
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit git.hiverepo.com */

// Test hotreloading a dynamic library in the registry.

#include "testDL/hotreloadtest.h"

using namespace fe;
using namespace hive;

int main(int argc,char** argv)
{
	UNIT_START();

	sp<Master> spMaster(new fe::Master);
    sp<Registry> spRegistry=spMaster->registry();
    Result result = spRegistry->manage("feAutoLoadDL");

	// Create first implementation of component.
	sp<HotReloadTestComponentI> testComponent1 = spRegistry->create("*.HotReloadTestComponent");
	if (!testComponent1.isValid())
	{
        feX(argv[0], "couldn't create component");
    }

	String string1 = testComponent1->getText().c_str();
	feLog("%s\n", string1.c_str());
	UNIT_TEST(string1 == "OUTPUT FROM DL NUMBER 1");

	// Hotreload the dynamic library.
	spRegistry->substituteLibrary("HotReloadTestDL", "HotReloadTestDL-2");

	// Create second implementation of component. It should now be hotreloaded.
	sp<HotReloadTestComponentI> testComponent2 = spRegistry->create("*.HotReloadTestComponent");
	if (!testComponent2.isValid())
	{
		feX(argv[0], "couldn't create component");
	}

	String string2 = testComponent2->getText().c_str();
	feLog("%s\n", string2.c_str());
	UNIT_TEST(string2 == "OUTPUT FROM DL NUMBER 2");

	// Hotreload again with the original filename to test wildcard search.
	// Even though HotReloadTestDL-2 is currently loaded, hotreloading `HotReloadTestDL`
	// should still work.
	spRegistry->substituteLibrary("HotReloadTestDL", "HotReloadTestDL-3");

	// Create third implementation of component. It should now be the same as the first one.
	sp<HotReloadTestComponentI> testComponent3 = spRegistry->create("*.HotReloadTestComponent");
	if (!testComponent3.isValid())
	{
		feX(argv[0], "couldn't create component");
	}

	String string3 = testComponent3->getText().c_str();
	feLog("%s\n", string3.c_str());
	UNIT_TEST(string3 == "OUTPUT FROM DL NUMBER 1");

	// Try hotreloading DL-3 again to test against double-loading a DL.
	spRegistry->substituteLibrary("HotReloadTestDL", "HotReloadTestDL-3");

	// Create fourth implementation of component. It should now be the same as the first/third one.
	sp<HotReloadTestComponentI> testComponent4 = spRegistry->create("*.HotReloadTestComponent");
	if (!testComponent4.isValid())
	{
		feX(argv[0], "couldn't create component");
	}

	String string4 = testComponent4->getText().c_str();
	feLog("%s\n", string4.c_str());
	UNIT_TEST(string4 == "OUTPUT FROM DL NUMBER 1");

	UNIT_TRACK(4);
	UNIT_RETURN();
}
