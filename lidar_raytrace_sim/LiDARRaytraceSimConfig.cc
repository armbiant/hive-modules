/*	Copyright (C) 2018 Arrival Ltd
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit git.hiverepo.com */

#include "apiary/apiary.h"
#include "LiDARRaytraceSimConfig.h"

namespace hive
{

//-----------------------------------------------------------------
LiDARRaytraceSimConfig::LiDARRaytraceSimConfig()
{
    SetDefaultValues();
}

//-----------------------------------------------------------------
void LiDARRaytraceSimConfig::SetDefaultValues()
{
    mPeriod = 0.1f;
    mChannels = 3;
    mStep = 100;
    mFOV_HorizontMin = 0.f;
    mFOV_HorizontMax = 360.f;
    mFOV_VerticalMin = -15.f;
    mFOV_VerticalMax = 15.f;
    mDistanseMin = 0.5f;
    mDistanseMax = 10.f;
    mAbsorptionCoefficient = 0.005f;
    mReflectionCoefficient = 0.8f;
    mLaserIntensity = 100000.f;
    mSensorSize = 0.01f;
    mEnabledGroundFilter = false;
    mDistanceToGround = 0.f;
    mDeviceID = 1;

    // Coms
    mSendIPaddress = "127.0.0.1";
    mSendPort = 12000;
    mReceivePort = 12001;

    // Debug
    mDrawTracedRays = true;
    mDrawNotTracedRays = false;
    mCreateFileDump = false;
}

//-----------------------------------------------------------------
bool LiDARRaytraceSimConfig::LoadFromJSON(const char * fileName )
{
	feLog("LiDARRaytraceSimConfig::LoadFromJSON: file name = %s\n", fileName);

    using namespace fe;

	sp<Master> spMaster(new Master);
	sp<Registry> spRegistry=spMaster->registry();

	Result result = spRegistry->manage("fexJsonDL");
    if(!successful(result))
    {
 		feLog("LiDARRaytraceSimConfig::LoadFromJSON: error load fexJsonDL\n");
        return false;
    }

	sp<CatalogReaderI> spCatalogReader = spRegistry->create("CatalogReaderI.*.*.json");
    if(!spCatalogReader.isValid())
	{
		feLog("LiDARRaytraceSimConfig::LoadFromJSON: spCatalogReader invalid\n");
        return false;
	}

	sp<Catalog> spCatalogIn = spMaster->createCatalog("input");
	if(!spCatalogIn.isValid())
	{
		feLog("LiDARRaytraceSimConfig::LoadFromJSON: spCatalogIn invalid\n");
        return false;
	}

    // Load catalog
	if( spCatalogReader->load(fe::String(fileName),spCatalogIn) == 0 )
    {
        feLog("LiDARRaytraceSimConfig::LoadFromJSON: Unable to load %s\n", fileName);
        return false;
    }

	mPeriod = spCatalogIn->catalogOrDefault<Real>("Period", mPeriod);
	mChannels = spCatalogIn->catalogOrDefault<int>("Channels", mChannels);
    mStep = spCatalogIn->catalogOrDefault<int>("Step", mStep);
    mFOV_HorizontMin = spCatalogIn->catalogOrDefault<Real>("FOV_HorizontMin", mFOV_HorizontMin);
    mFOV_HorizontMax = spCatalogIn->catalogOrDefault<Real>("FOV_HorizontMax", mFOV_HorizontMax);
    mFOV_VerticalMin = spCatalogIn->catalogOrDefault<Real>("FOV_VerticalMin", mFOV_VerticalMin);
    mFOV_VerticalMax = spCatalogIn->catalogOrDefault<Real>("FOV_VerticalMax", mFOV_VerticalMax);
    mDistanseMin = spCatalogIn->catalogOrDefault<Real>("DistanseMin", mDistanseMin);
    mDistanseMax = spCatalogIn->catalogOrDefault<Real>("DistanseMax", mDistanseMax);
    mAbsorptionCoefficient = spCatalogIn->catalogOrDefault<Real>("AbsorptionCoefficient", mAbsorptionCoefficient);
    mReflectionCoefficient = spCatalogIn->catalogOrDefault<Real>("ReflectionCoefficient", mReflectionCoefficient);
    mLaserIntensity = spCatalogIn->catalogOrDefault<Real>("LaserIntensity", mLaserIntensity);
    mSensorSize = spCatalogIn->catalogOrDefault<Real>("SensorSize", mSensorSize);
    mEnabledGroundFilter = spCatalogIn->catalogOrDefault<bool>("EnabledGroundFilter", mEnabledGroundFilter);
    mDistanceToGround = spCatalogIn->catalogOrDefault<Real>("DistanceToGround", mDistanceToGround);
    mDeviceID = spCatalogIn->catalogOrDefault<int>("DeviceID", mDeviceID);

    // Coms
    mSendIPaddress = spCatalogIn->catalogOrDefault<fe::String>("SendIPaddress", mSendIPaddress.c_str()).c_str();
    mSendPort = spCatalogIn->catalogOrDefault<int>("SendPort", mSendPort);
    mReceivePort = spCatalogIn->catalogOrDefault<int>("ReceivePort", mReceivePort);

    // Debug
    mDrawTracedRays = spCatalogIn->catalogOrDefault<bool>("DrawTracedRays", mDrawTracedRays);
    mDrawNotTracedRays = spCatalogIn->catalogOrDefault<bool>("DrawNotTracedRays", mDrawNotTracedRays);
    mCreateFileDump = spCatalogIn->catalogOrDefault<bool>("CreateFileDump", mCreateFileDump);

    return true;
}

} // namespace hive
