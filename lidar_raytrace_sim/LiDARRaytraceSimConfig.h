/*	Copyright (C) 2018 Arrival Ltd
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit git.hiverepo.com */

#pragma once

#include <stdint.h>
#include <string>

#include "apiary/apiary.h"

namespace hive
{

//-----------------------------------------------------------------
class FE_DL_EXPORT LiDARRaytraceSimConfig
{
    public:
    	float mPeriod;
	    int mChannels;
    	int mStep;
    	float mFOV_HorizontMin;
    	float mFOV_HorizontMax;
    	float mFOV_VerticalMin;
    	float mFOV_VerticalMax;
    	float mDistanseMin;
    	float mDistanseMax;
    	float mAbsorptionCoefficient;
    	float mReflectionCoefficient;
    	float mLaserIntensity;
    	float mSensorSize;
        bool mEnabledGroundFilter;
        float mDistanceToGround;
        uint8_t mDeviceID;

        // Coms
        std::string mSendIPaddress;
        int mSendPort;
        int mReceivePort;

        // Debug
    	bool mDrawTracedRays;
    	bool mDrawNotTracedRays;
        bool mCreateFileDump;

        LiDARRaytraceSimConfig();
        void SetDefaultValues();
        bool LoadFromJSON(const char *fileName);
};

} // namespace hive

