/*	Copyright (C) 2018 Arrival Ltd
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit git.hiverepo.com */

#pragma once

#include "apiary/apiary.h"

#include <stdint.h>

namespace hive
{

#include "scala/RaycastEntry.h"

//-----------------------------------------------------------------
struct LidarRaytracePoint : RaycastEntry
{
    // Extra information need to track for LiDAR coms
    int16_t layer;
    FloatXYZ tracedPoint;

    virtual int GetSize() override { return sizeof(LidarRaytracePoint); }
};

//-----------------------------------------------------------------
class FE_DL_EXPORT LiDARRaytraceSimI : virtual public fe::Component, public fe::CastableAs<LiDARRaytraceSimI>
{
    public:
        virtual void ConfigScan( float minRange, float maxRange,
                                 float horizontalMin, float horizontalMax, int step,
                                 float verticalMin, float verticalMax, int channels,
                                 int deviceID ) = 0;
        virtual void ConfigOutput( const char * sendIPaddress, int sendPort, int receivePort ) = 0;
        virtual void ConfigDebug( bool drawTracedRays, bool drawNotTracedRays, bool createFileDump ) = 0;

		virtual void Init() = 0;
		virtual void shutdown() = 0;
        virtual int GetPointCount() = 0;
        virtual LidarRaytracePoint* GetLidarPoints() = 0;
        virtual void SendScan() = 0;
        virtual bool GetDisplayTracePoints() = 0;
};

} // namespace hive
