/*	Copyright (C) 2018 Arrival Ltd
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit git.hiverepo.com */

#include "apiary/apiary.h"
#include "UDPLiDARPublisher.h"

#include <numeric>
#include <sstream>

using namespace hive;

fe::sp<fe::Master> gMaster;

//-----------------------------------------------------------------
void UDPLiDARPublisher::Advertise( const std::string IPaddress, const int port)
{
	mIPaddress = IPaddress;
	mPort = port;

	if(mIsInit || !mSendEnabled)
	{
		return;
	}

	feLog("UdpLidarPublisher::Advertise\n");

    using namespace fe;

	if( ! mMessageOut.isValid() )
	{
		if( !gMaster.isValid() )
		{
			gMaster = new Master();
		}
		sp<Registry> spRegistry = gMaster->registry();

		Result result = spRegistry->manage("hiveMessageDL");
		if(!successful(result))
		{
			feLog("UdpLidarPublisher::Advertise: error loading hiveMessageDL\n");
			return;
		}

		mMessageOut = spRegistry->create("MessageI.MessageUDP");
	}

	if(mMessageOut.isValid())
	{
		mIsInit = mMessageOut->postInit();
	}
	else
	{
		feLog("UdpLidarPublisher::Advertise: MessageI error\n");
		mSendEnabled = false; // Disable sending
	}

	return;
}

//-----------------------------------------------------------------
void UDPLiDARPublisher::shutdown()
{
	if(mMessageOut.isValid() && mIsInit)
	{
		mMessageOut->shutdown();

		fe::sp<fe::Registry> spRegistry = gMaster->registry();
		spRegistry->unmanage("hiveMessageDL");
	}
}

//-----------------------------------------------------------------
void UDPLiDARPublisher::Publish()
{
	if(!mSendEnabled || !mIsInit)
	{
		return;
	}

	std::stringstream sout(std::ios_base::out | std::ios_base::binary);
	int const points_count = mScan.Write(sout);
	if (!sout)
	{
		feLog("UDPLiDARPublisher::Publish: failed to serialize scan\n");
		return;
	}

	sout.seekp(0, std::ios_base::end);
	auto const length = sout.tellp();

	// create a Messagegram and copy payload (message) into it
	fe::ext::Messagegram mg;

	mg.dataLen = length;

	FEASSERT(sout.str().length() < sizeof(mg.data));
	memcpy(mg.data, (const uint8_t*)sout.str().data(), sout.str().length());

	if( !mMessageOut->sendLargeTo(mg, mIPaddress.c_str(), mPort) )
	{
		feLog("UDPLiDARPublisher::Publish: failed to send message\n");
	}
}

//-----------------------------------------------------------------
 void Pose2d::Write(std::ostream& out)
 {
	std::uint8_t type = 0;
	out.write(reinterpret_cast< const char* >(&type), static_cast< std::streamsize >(sizeof(type)));
	out.write(reinterpret_cast< const char* >(&m_x), static_cast< std::streamsize >(sizeof(m_x)));
	out.write(reinterpret_cast< const char* >(&m_y), static_cast< std::streamsize >(sizeof(m_y)));
	out.write(reinterpret_cast< const char* >(&m_yaw), static_cast< std::streamsize >(sizeof(m_yaw)));
 }

//-----------------------------------------------------------------
void Scan::Write(std::ostream& out, void const* data, std::size_t size)
{
	out.write(reinterpret_cast< const char* >(data), static_cast< std::streamsize >(size));
}

//-----------------------------------------------------------------
void Scan::Read(std::istream& in, void* data, std::size_t size)
{
	in.read(reinterpret_cast< char* >(data), static_cast< std::streamsize >(size));
}

//-----------------------------------------------------------------
std::size_t Scan::Write(std::ostream & out)
{
	// Write scan header
	std::stringstream sout(std::ios_base::out | std::ios_base::binary);
	Write(out, &steady_timestamp, sizeof(steady_timestamp));
	Write(out, &system_timestamp, sizeof(system_timestamp));
	Write(out, &device_start_time, sizeof(device_start_time));
	Write(out, &device_duration, sizeof(device_duration));
	Write(out, &device_id, sizeof(device_id));
	device_pose.Write(out);

	std::uint16_t const pointCount = static_cast<decltype(pointCount)>(points.size());

	assert(points.size() <= std::numeric_limits<decltype(pointCount)>::max());

	// Write point count
	Write(out, &pointCount, sizeof(pointCount));

	// Write points
	if (pointCount != 0)
	{
		Write(out, points.data(), points.size() * sizeof(ScanPoint));
	}

	sout.seekp(0, std::ios_base::end);
	auto const length = sout.tellp();

	// Write header
	scanHeader header;
	header.generation = 2;
	header.devices_count = 1;
	header.length = static_cast<decltype(header.length)>(length);

	Write(out, reinterpret_cast<char *>(&header), sizeof(header));
	Write(out, sout.str().data(), header.length);

	return static_cast<std::size_t>(pointCount);
}
