import sys
forge = sys.modules["forge"]

def prerequisites():
	return ["json"]

def setup(module):
	srcList = [	"LiDARRaytraceSim",
				"LiDARRaytraceSimConfig",
				"UDPLiDARPublisher",
				"LiDARRaytraceSim.pmh",
				"LiDARRaytraceSimDL" ]


	dll = module.DLL( "LiDARRaytraceSimDL", srcList )

	deplibs = forge.corelibs + [
				"fexSignalLib",
				"fexNetworkDLLib" ]

	if forge.fe_os == 'FE_WIN32' or forge.fe_os == 'FE_WIN64':
		deplibs += [ "fexDataToolLib" ]

	forge.objlists['lidar_raytrace_sim'] = module.SrcToObj(srcList)

	forge.deps( ["LiDARRaytraceSimDLLib"], deplibs )
