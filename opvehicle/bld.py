import os
import sys
forge = sys.modules["forge"]

def prerequisites():
	return [ "vehicle_driver"]

def setup(module):
	srcList = [	"opvehicle.pmh",
				"OpDriver",
				"opvehicleDL"]

	dll = module.DLL( "hiveOpVehicleDL", srcList )

	deplibs = forge.corelibs+ [
				"fexSignalLib",
				"fexDataToolDLLib",
				"fexDrawDLLib",
				"fexOperateDLLib",
				"fexOperatorDLLib",
				"fexSurfaceDLLib",
				"fexWindowLib",
				"hiveApiaryDLLib" ]

	if forge.fe_os == 'FE_WIN32' or forge.fe_os == 'FE_WIN64':
		deplibs += [	"fexThreadDLLib" ]

	forge.deps( ["hiveOpVehicleDLLib"], deplibs )

	forge.tests += [("inspect.exe",	"hiveOpVehicleDL",	None,	None) ]
