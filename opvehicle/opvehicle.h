/*	Copyright (C) 2018 Arrival Ltd
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit git.hiverepo.com */

/** @file */

#ifndef __hive_vehicle_driver_h__
#define __hive_vehicle_driver_h__

#include "operate/operate.h"
#include "apiary/apiary.h"

#endif
