/*	Copyright (C) 2018 Arrival Ltd
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit git.hiverepo.com */

/** @file */

#include <opvehicle/opvehicle.pmh>

#include "platform/dlCore.cc"

using namespace fe;
using namespace hive;

extern "C"
{

FE_DL_EXPORT void ListDependencies(List<String*>& list)
{
	list.append(new String("fexSignalDL"));
}

FE_DL_EXPORT Library *CreateLibrary(sp<Master> spMaster)
{
	assertMath(spMaster->typeMaster());

	Library *pLibrary = new Library();

	pLibrary->add<OpDriver>("HandlerI.OpDriver.hive");

	return pLibrary;
}

FE_DL_EXPORT void InitializeLibrary(sp<Library> spLibrary)
{
}

}

