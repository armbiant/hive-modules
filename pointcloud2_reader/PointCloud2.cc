/*	Copyright (C) 2018 Arrival Ltd
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit git.hiverepo.com */

#include "PointCloud2.h"

using namespace PointCloud2;

//------------------------------------------------------------------------------
PointCloud2Lidar::~PointCloud2Lidar()
{
    if(mCloudData != nullptr)
    {
        delete[] mCloudData;
    }
}

//------------------------------------------------------------------------------
PointCloud2Lidar::PointCloud2Lidar()
{
    mCloudData = nullptr;
}

//------------------------------------------------------------------------------
void MsgHeader::Serialize(simple::mem_ostream<std::false_type> &outputStream)
{
    outputStream << mSequence;       // sequence ID: consecutively increasing ID
    outputStream << mTimeStamp.sec;  // stamp.sec: seconds (stamp_secs) since epoch
    outputStream << mTimeStamp.nsec; // stamp.nsec: nanoseconds since stamp_secs
    outputStream << mFrameId;        // Frame this data is associated with
}

//------------------------------------------------------------------------------
void MsgHeader::Deserialize(simple::mem_istream<std::false_type> &inputStream)
{
    inputStream >> mSequence;       // sequence ID: consecutively increasing ID
    inputStream >> mTimeStamp.sec;  // stamp.sec: seconds (stamp_secs) since epoch
    inputStream >> mTimeStamp.nsec; // stamp.nsec: nanoseconds since stamp_secs
    inputStream >> mFrameId;        // Frame this data is associated with
}


//------------------------------------------------------------------------------
void PointField::Serialize(simple::mem_ostream<std::false_type> &outputStream)
{
    outputStream << mName;          // Name of field
    outputStream << mOffset;        // Offset from start of point struct
    outputStream << mDataType;      // Datatype enumeration, see above
    outputStream << mCount;         // How many elements in the field
}

//------------------------------------------------------------------------------
void PointField::Deserialize(simple::mem_istream<std::false_type> &inputStream)
{
    inputStream >> mName;          // Name of field
    inputStream >> mOffset;        // Offset from start of point struct
    inputStream >> mDataType;      // Datatype enumeration, see above
    inputStream >> mCount;         // How many elements in the field
}


//------------------------------------------------------------------------------
void CloudPoint::Serialize(simple::mem_ostream<std::false_type> &outputStream)
{
    outputStream << m_x;
    outputStream << m_y;
    outputStream << m_z;
    outputStream << m_intensity;
}

//------------------------------------------------------------------------------
void CloudPoint::Deserialize(simple::mem_istream<std::false_type> &inputStream)
{
    inputStream >> m_x;
    inputStream >> m_y;
    inputStream >> m_z;
    inputStream >> m_intensity;
}


//------------------------------------------------------------------------------
void PointCloud2Lidar::Serialize(simple::mem_ostream<std::false_type> &outputStream)
{
    mHeader.Serialize(outputStream);

    outputStream << mHeight;
    outputStream << mWidth;

    for(int i = 0; i < 4; i++)
    {
        mFields[i].Serialize(outputStream);
    }

    outputStream << (uint8_t)mIsBigendian;
    outputStream << mPointStep;
    outputStream << mRowStep;

    for( uint32_t i = 0; i <  mWidth; i++)
    {
        mCloudData[i].Serialize(outputStream);
    }

    outputStream << (uint8_t)mIsDense;
}

//------------------------------------------------------------------------------
void PointCloud2Lidar::Deserialize(simple::mem_istream<std::false_type> &inputStream)
{
    mHeader.Deserialize(inputStream);

    inputStream >> mHeight;
    inputStream >> mWidth;

    for(int i = 0; i < 4; i++)
    {
        mFields[i].Deserialize(inputStream);
    }

    inputStream >> mIsBigendian;
    inputStream >> mPointStep;
    inputStream >> mRowStep;

    mCloudData = new CloudPoint[mWidth];

    for( uint32_t i = 0; i <  mWidth; i++)
    {
        mCloudData[i].Deserialize(inputStream);
    }

    inputStream >> mIsDense;
}

