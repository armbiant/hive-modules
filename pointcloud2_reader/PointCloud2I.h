/*	Copyright (C) 2018 Arrival Ltd
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit git.hiverepo.com */

#pragma once

#include <stdint.h>

namespace PointCloud2
{

struct CloudPointI
{
    float m_x, m_y, m_z;
    uint8_t m_intensity;
};

} // namespace PointCloud2
