/*	Copyright (C) 2018 Arrival Ltd
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit git.hiverepo.com */

#include "apiary/apiary.h"

#include "UdpClient.h"
#include "ZeroMQSubscriber.h"
#include "PointCloud2Reader.h"

namespace hive
{

fe::sp<fe::Master> gMaster;

//------------------------------------------------------------------------------
PointCloud2Reader::PointCloud2Reader()
{
    m_subscriber = nullptr;
}

//------------------------------------------------------------------------------
PointCloud2Reader::~PointCloud2Reader()
{
 	if(m_subscriber)
    {
		delete m_subscriber;
		m_subscriber = nullptr;
	}
}

//------------------------------------------------------------------------------
bool PointCloud2Reader::init(const char *receiveIPAddress,
							 const int receivePort, const bool useUDP)
{
    if(useUDP)
    {
		if(m_subscriber == nullptr)
		{
        	m_subscriber = new UdpClient();
		}
    }
    else
    {
		if(m_subscriber == nullptr)
		{
        	m_subscriber = new ZeroMQSubscriber();
		}
    }

    bool result = m_subscriber->init("PointCloud2", receiveIPAddress,
									 receivePort, (2 * 1024 * 1024));
    m_buffer.resize(2 * 1024 * 1024);

	return result;
}

//------------------------------------------------------------------------------
void PointCloud2Reader::shutdown()
{
	if(m_subscriber)
	{
		m_subscriber->shutdown();
	}
}

//------------------------------------------------------------------------------
void PointCloud2Reader::read(PointCloud2::CloudPointI* cloudPoints,
							 int &numPoints, const long seconds,
							 const long uSeconds)
{
    int numberBytes = m_subscriber->readMessage(m_buffer, seconds, uSeconds);

    if( numberBytes != 0 )
    {
        simple::mem_istream<std::false_type> inputStream;
        inputStream.open(m_buffer.data(), m_buffer.size());

        PointCloud2::PointCloud2Lidar message;
        try
        {
            message.Deserialize(inputStream);
        }
        catch(const std::exception& e)
        {
			feLog("PointCloud2Reader::read: Exception %s\n", e.what());
            return;
        }

        numPoints = message.mWidth;

        for(uint32_t pointCount = 0; pointCount < numPoints ; pointCount++ )
        {
            cloudPoints[pointCount] = *(PointCloud2::CloudPointI *)&message.mCloudData[pointCount];
        }
    }
	else
	{
		numPoints = 0;
	}
}

} // namespace hive
