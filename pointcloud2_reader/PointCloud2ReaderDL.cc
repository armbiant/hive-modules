/*	Copyright (C) 2018 Arrival Ltd
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit git.hiverepo.com */

/** @file */

#include "PointCloud2Reader.h"

#include "platform/dlCore.cc"

using namespace fe;
using namespace hive;

extern "C"
{

FE_DL_EXPORT void ListDependencies(List<String*>& list)
{
	list.append(new String("feDataDL"));

}

FE_DL_EXPORT Library* CreateLibrary(sp<Master> spMaster)
{
	Library *pLibrary = new Library();
	pLibrary->add<PointCloud2Reader>("PointCloud2ReaderI.PointCloud2Reader.hive");
	return pLibrary;
}

FE_DL_EXPORT void InitializeLibrary(sp<Library> spLibrary)
{
}

}
