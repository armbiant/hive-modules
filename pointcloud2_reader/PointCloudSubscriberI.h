/*	Copyright (C) 2018 Arrival Ltd
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit git.hiverepo.com */

#pragma once

#include <string>
#include <vector>

namespace hive
{

//------------------------------------------------------------------------------
class PointCloudSubscriberI
{
public:
	virtual ~PointCloudSubscriberI() {};

	virtual bool init(const std::string &topic, const char *IPaddress,
					  const int port,
					  const int bufferSize) = 0;

	virtual void shutdown() = 0;

	virtual int readMessage(std::vector<char> &message) = 0;
	virtual int readMessage(std::vector<char> &message,
							const long seconds,
							const long uSeconds) = 0;
};

} // namespace hive
