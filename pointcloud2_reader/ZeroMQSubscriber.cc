/*	Copyright (C) 2018 Arrival Ltd
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit git.hiverepo.com */

/** @file */

#include "apiary/apiary.h"
#include "ZeroMQSubscriber.h"

namespace hive
{

extern fe::sp<fe::Master> gMaster;

//------------------------------------------------------------------------------
ZeroMQSubscriber::ZeroMQSubscriber() : m_isInit(false), m_readEnabled(true)
{
}

//------------------------------------------------------------------------------
ZeroMQSubscriber::~ZeroMQSubscriber()
{
	if(m_isInit && m_subscriber.isValid())
	{
		m_subscriber->shutdown();
	}
}

//------------------------------------------------------------------------------
bool ZeroMQSubscriber::init(const std::string &topic, const char *IPaddress,
							const int port, const int bufferSize)
{
	if(m_isInit || !m_readEnabled)
	{
		return true;
	}

	m_topic = topic;

    feLog("ZeroMQSubscriber::init\n");

    using namespace fe;

	if(!m_subscriber.isValid())
	{
		if(!gMaster.isValid())
		{
			gMaster = new Master();
		}
		sp<Registry> spRegistry = gMaster->registry();

		Result result = spRegistry->manage("hiveSensorPublishSubscribeDL");
    	if(!successful(result))
    	{
 			feLog("ZeroMQSubscriber::init: Error loading hiveSensorPublishSubscribeDL\n");
        	return false;
    	}

		m_subscriber = spRegistry->create("SensorSubscriberI.SensorZeromqSubscriber");

		if(m_subscriber.isValid())
		{
    		std::string address = "tcp://localhost:" + std::to_string(port);
			m_isInit = m_subscriber->start(topic, address);
		}
		else
		{
			feLog("ZeroMQSubscriber::init: sensor zeromq subscriber creation error\n");
			m_readEnabled = false; // Disable reading
		}
	}

	return m_isInit;
}

//------------------------------------------------------------------------------
void ZeroMQSubscriber::shutdown()
{
	if(m_subscriber.isValid())
	{
		m_subscriber->shutdown();
	}
}

//------------------------------------------------------------------------------
int ZeroMQSubscriber::readMessage(std::vector<char> &message )
{
	if (!m_readEnabled || !m_isInit)
	{
		return 0;
	}

	int cnt = m_subscriber->listen(m_topic, *(fe::Array<U8> *)&message);

	return cnt;
}

//------------------------------------------------------------------------------
int ZeroMQSubscriber::readMessage(std::vector<char> &message,
								  const long seconds, const long uSeconds)
{
	if (!m_readEnabled || !m_isInit)
	{
		return 0;
	}

	int cnt = m_subscriber->listenNonBlocking(m_topic, *(fe::Array<U8> *)&message);

	return cnt;
}


} // namespace hive
