/*	Copyright (C) 2018 Arrival Ltd
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit git.hiverepo.com */

#pragma once

#include "PointCloudSubscriberI.h"
#include "sensor_publish_subscribe/SensorSubscriberI.h"

namespace hive
{

//------------------------------------------------------------------------------
class ZeroMQSubscriber : public PointCloudSubscriberI
{
public:
	ZeroMQSubscriber();
	virtual ~ZeroMQSubscriber();

	virtual bool init(const std::string &topic, const char *IPaddress,
					  const int port, const int bufferSize) override;
	virtual void shutdown() override;
	virtual int readMessage(std::vector<char> &message) override;
	virtual int readMessage(std::vector<char> &message,
							const long seconds,
							const long uSeconds) override;

protected:
	bool m_readEnabled;
	bool m_isInit;

	fe::sp<SensorSubscriberI> m_subscriber;
	std::string m_topic;
};

} // namespace hive
