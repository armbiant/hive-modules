/*	Copyright (C) 2018 Arrival Ltd
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit git.hiverepo.com */

/** @file */

#include "rigid_body_engine.pmh"

namespace hive {

	sp<RigidBodyI> ConnectedSystemBase::createBall(const Real mass, const SpatialVector& position, const Real radius,
		bool bDynamic)
	{
		SpatialMatrix inertia = SpatialMatrix();
		setAll(inertia, 0.0);
		Real i = 2.0 / 5.0 * mass * pow(radius, 2);
		inertia(0, 0) = i;
		inertia(1, 1) = i;
		inertia(2, 2) = i;
		SpatialMatrix rotation;
		setIdentity(rotation);
		sp<RigidBodyI> spBody = createRigidBody(mass, position, rotation, inertia, bDynamic);
		return spBody;
	}

	sp<RigidBodyI> ConnectedSystemBase::createThinRod(const Real mass, const SpatialVector& position, const SpatialMatrix& rotation,
		const Real length, bool bDynamic)
	{
		SpatialMatrix inertia = SpatialMatrix();
		setAll(inertia, 0.0);
		Real i = 1.0 / 12.0 * mass * pow(length, 2);
		inertia(0, 0) = i;
		inertia(1, 1) = 0.0001; // ode requires positive definite inertia, so rod must have small nonzero thickness
		inertia(2, 2) = i;

		sp<RigidBodyI> spBody = createRigidBody(mass, position, rotation, inertia, bDynamic);
		return spBody;
	}

	sp<RigidBodyI> ConnectedSystemBase::createCylinder(const Real mass, const SpatialVector& position, const SpatialMatrix& rotation,
		const Real length, const Real radius, bool bDynamic)
	{
		SpatialMatrix inertia = SpatialMatrix();
		setAll(inertia, 0.0);
		Real i1 = 1.0 / 12.0 * mass * (3.0 * pow(radius, 2) + pow(length, 2));
		inertia(0, 0) = i1;
		inertia(1, 1) = i1;
		Real i2 = 1.0 / 2.0 * mass * pow(radius, 2);
		inertia(2, 2) = i2;
		sp<RigidBodyI> spBody = createRigidBody(mass, position, rotation, inertia, bDynamic);
		return spBody;
	}

	sp<RigidBodyI> ConnectedSystemBase::createTube(const Real mass, const SpatialVector& position, const SpatialMatrix& rotation,
		const Real length, const Real innerRadius, const Real outerRadius, bool bDynamic)
	{
		SpatialMatrix inertia = SpatialMatrix();
		setAll(inertia, 0.0);
		Real r = pow(innerRadius, 2) + pow(outerRadius, 2);
		Real i1 = 1.0 / 12.0 * mass * (3.0 * r + pow(length, 2));
		inertia(0, 0) = i1;
		inertia(1, 1) = i1;
		Real i2 = 1.0 / 2.0 * mass * r;
		inertia(2, 2) = i2;
		sp<RigidBodyI> spBody = createRigidBody(mass, position, rotation, inertia, bDynamic);
		return spBody;
	}

	sp<RigidBodyI> ConnectedSystemBase::createBar(const Real mass, const SpatialVector& position, const SpatialMatrix& rotation,
		const Real x, const Real y, const Real z, bool bDynamic)
	{
		SpatialMatrix inertia = SpatialMatrix();
		setAll(inertia, 0.0);
		Real i1 = 1.0 / 12.0 * mass * (pow(y, 2) + pow(z, 2));
		Real i2 = 1.0 / 12.0 * mass * (pow(x, 2) + pow(z, 2));
		Real i3 = 1.0 / 12.0 * mass * (pow(x, 2) + pow(y, 2));
		inertia(0, 0) = i1;
		inertia(1, 1) = i2;
		inertia(2, 2) = i3;
		sp<RigidBodyI> spBody = createRigidBody(mass, position, rotation, inertia, bDynamic);
		return spBody;
	}
}
