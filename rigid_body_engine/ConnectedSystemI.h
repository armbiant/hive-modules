/*	Copyright (C) 2018 Arrival Ltd
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit git.hiverepo.com */

/** @file */

#ifndef __rigid_body_engine_ConnectedSystemI_h__
#define __rigid_body_engine_ConnectedSystemI_h__

namespace hive
{

class FE_DL_EXPORT ConnectedSystemI : virtual public fe::Component, public fe::CastableAs<ConnectedSystemI>
{
public:
	// In addition to these functions, has:
	// sp<RigidBodyI> create*() method where
	// * = ball, rob, tube, and bar
	// already implemented in "dirty" interface inherited here

	// Rigid bodies (first method assumes sphere for inertia)

	/// General case - requires a rotation as well as inertia matrix
	virtual sp<RigidBodyI>	createRigidBody(const Real mass, const SpatialVector& position,
									const SpatialMatrix& rotation, const SpatialMatrix& inertia,
									bool bDynamic = true)												= 0;

	/// Convenience function for a spherical rigid body - rotation is always setIdentity()
	virtual sp<RigidBodyI>	createRigidBody(const Real mass, const SpatialVector& position,
									const Real radius, bool bDynamic = true)							= 0;

	virtual sp<RigidBodyI>	createBall(const Real mass, const SpatialVector& position,
									const Real radius, bool bDynamic = true)							= 0;
	virtual sp<RigidBodyI>	createThinRod(const Real mass, const SpatialVector& position,
									const SpatialMatrix& rotation, const Real length,
									bool bDynamic = true)												= 0;
	virtual sp<RigidBodyI>	createCylinder(const Real mass, const SpatialVector& position,
									const SpatialMatrix& rotation, const Real length,
									const Real radius, bool bDynamic = true)							= 0;
	virtual sp<RigidBodyI>	createTube(const Real mass, const SpatialVector& position,
									const SpatialMatrix& rotation, const Real length,
									const Real innerRadius, const Real outerRadius,
									bool bDynamic = true)												= 0;
	virtual sp<RigidBodyI>	createBar(const Real mass, const SpatialVector& position,
									const SpatialMatrix& rotation, const Real x,
									const Real y, const Real z,
									bool bDynamic = true)												= 0;

	// Joints
	/// These take the positions (and orientation, for Hinge joint) in coordinates relative to each of the rigid bodies.
	virtual sp<JointI>		createHingeJoint(const sp<RigidBodyI> bodyA, const sp<RigidBodyI> bodyB,
									const SpatialVector& pivotInA, const SpatialVector& pivotInB,
									const SpatialVector& axisInA, const SpatialVector& axisInB)			= 0;
	virtual sp<JointI>		createBallJoint(const sp<RigidBodyI> bodyA, const sp<RigidBodyI> bodyB,
									const SpatialVector& pivotInA, const SpatialVector& pivotInB)		= 0;

	/// These take the positions (and orientation, for Hinge joint) in world coordinates.
	virtual sp<JointI>		createHingeJoint(const sp<RigidBodyI> bodyA, const sp<RigidBodyI> bodyB,
									const SpatialVector& pivotPos, const SpatialVector& axisPos)		= 0;
	virtual sp<JointI>		createBallJoint(const sp<RigidBodyI> bodyA, const sp<RigidBodyI> bodyB,
									const SpatialVector& pivotPos)										= 0;

	// SpringDamper
	virtual sp<SpringDamperI>	createSpringDamper(const sp<RigidBodyI> bodyA, const SpatialVector pointA,
									sp<RigidBodyI> bodyB, SpatialVector pointB)							= 0;
	// TODO temporary back-compatibility for chassis
	// Revisit during refactor
	virtual sp<SpringDamperI>	createSpringDamper(const sp<RigidBodyI> bodyA, const SpatialVector pointA,
									const sp<RigidBodyI> bodyB, const SpatialVector pointB,
									const Real a_springRate, const Real a_dampingConstant,
									const Real a_restLength)											= 0;

	// Accessors for world parameters
	// Gravity is exclusively global
	virtual void			setGravity(const SpatialVector& gravity)									= 0;

	// These four can also be set per-joint
	// Constraint Force Mixing: A nonzero (positive) value of CFM allows the original constraint equation
	// to be violated by an amount proportional to CFM times the restoring force lambda
	virtual void			setGlobalCFM(const Real cfm)												= 0;
	// Error Reduction Parameter: Controls how much error reduction is performed at each step
	virtual void			setGlobalERP(const Real erp)												= 0;
	virtual void			setGlobalLinearDamping(const Real damping)									= 0;
	virtual void			setGlobalAngularDamping(const Real damping)									= 0;

	// Solve constraints over time step. Should also step each SpringDamperI!
	virtual void			step(Real dt)																= 0;
	virtual std::vector<sp<RigidBodyI>> getRigidBodies()												= 0;
};

} /* namespace hive */

#endif /* __rigid_body_engine_ConnectedSystemI_h__ */
