/*	Copyright (C) 2018 Arrival Ltd
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit git.hiverepo.com */

/** @file */

#ifndef __rigid_body_engine_JointI_h__
#define __rigid_body_engine_JointI_h__

namespace hive
{

using namespace fe;

class FE_DL_EXPORT JointI : virtual public fe::Component, public CastableAs<JointI>
{
public:
	virtual	void	limitRotation(Real a_angleLo, Real a_angleHi)	= 0;
	virtual void	setCFM(Real cfm)								= 0;
	virtual void	setStopCFM(Real cfm)							= 0;
	virtual void	setStopERP(Real erp)							= 0;
};

} /* namespace hive */

#endif /* __rigid_body_engine_JointI_h__ */
