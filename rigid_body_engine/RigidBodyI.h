/*	Copyright (C) 2018 Arrival Ltd
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit git.hiverepo.com */

/** @file */

#ifndef __rigid_body_engine_RigidBodyI_h__
#define __rigid_body_engine_RigidBodyI_h__

namespace hive
{

class FE_DL_EXPORT RigidBodyI : virtual public fe::Component, public fe::CastableAs<RigidBodyI>
{
public:
	virtual	void				setMass(const fe::Real mass)						= 0;
	virtual	fe::Real			getMass(void) const									= 0;
	virtual	void				setPosition(const fe::SpatialVector &pos)			= 0;
	virtual	void				setRotation(const fe::SpatialMatrix &rotation)		= 0;
	virtual void				setTransform(const fe::SpatialTransform &transform)	= 0;
	virtual	fe::SpatialVector	getPosition(void) const								= 0;
	virtual	fe::SpatialMatrix	getRotation(void) const								= 0;
	virtual fe::SpatialTransform getTransform(void) const							= 0;
	virtual	void				applyForce(	const fe::SpatialVector &force,
										const fe::SpatialVector &pos)				= 0;
	virtual	void				applyTorque(const fe::SpatialVector &torque)		= 0;
	virtual void 				setVelocity(const fe::SpatialVector &vel) 			= 0;
	virtual fe::SpatialVector	getVelocity(const fe::SpatialVector &pos)	const 	= 0;
	virtual void 				setAngularVelocity(const fe::SpatialVector &angVel) = 0;
	virtual fe::SpatialVector	getAngularVelocity(void) const						= 0;
	virtual void				translate(const fe::SpatialVector &displ)			= 0;

	virtual void				setLinearDamping(const fe::Real damping)			= 0;
	virtual void				setAngularDamping(const fe::Real damping)			= 0;
};

} /* namespace hive */

#endif /* __rigid_body_engine_RigidBodyI_h__ */
