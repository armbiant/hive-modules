/*	Copyright (C) 2018 Arrival Ltd
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit git.hiverepo.com */

/** @file */

#include "rigid_body_engine.pmh"

namespace hive
{

void SpringDamperBase::setUp(sp<RigidBodyI> bodyA, SpatialVector pointA,
		sp<RigidBodyI> bodyB, SpatialVector pointB)
{
	m_bodyA = bodyA;
	m_bodyB = bodyB;

	// Convert world coordinates to body coordinates for bodyA, pointA
	SpatialVector position = bodyA->getPosition();
	SpatialMatrix inverseRotation;
	invert(inverseRotation, bodyA->getRotation());
	SpatialVector offset = pointA - position;
	multiply(m_pointAInBodyCoordinates, inverseRotation, offset);

	// Do the same for bodyB, pointB
	position = bodyB->getPosition();
	invert(inverseRotation, bodyB->getRotation());
	offset = pointB - position;
	multiply(m_pointBInBodyCoordinates, inverseRotation, offset);
}

void SpringDamperBase::step() {}

SpatialVector SpringDamperBase::getPointA()
{
	// Rotation the offset to match the body's rotation, then add the body's offset from world origin
	SpatialVector result = multiply(m_bodyA->getRotation(), m_pointAInBodyCoordinates) + m_bodyA->getPosition();
	return result;
}

SpatialVector SpringDamperBase::getPointB()
{
	SpatialVector result = multiply(m_bodyB->getRotation(), m_pointBInBodyCoordinates) + m_bodyB->getPosition();
	return result;
}

Real SpringDamperBase::distance()
{
	SpatialVector axis = getPointA() - getPointB();
	Real dist = magnitude(axis);
	return dist;
}

SpatialVector SpringDamperBase::unitAxisA()
{
	SpatialVector axis = getPointA() - getPointB();
	SpatialVector unitAxis = unitSafe(axis); // pointing toward A
	return unitAxis;
}

SpatialVector SpringDamperBase::unitAxisB()
{
	SpatialVector axis = getPointB() - getPointA();
	SpatialVector unitAxis = unitSafe(axis); // pointing toward B
	return unitAxis;
}

}
