/*	Copyright (C) 2018 Arrival Ltd
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit git.hiverepo.com */

/** @file */

#ifndef __rigid_body_engine_SpringDamperBase_h__
#define __rigid_body_engine_SpringDamperBase_h__

namespace hive
{

class FE_DL_EXPORT SpringDamperBase : virtual public SpringDamperI, public fe::CastableAs<SpringDamperBase>
{
public:
	// Points should be given in world coordinates
	virtual void setUp(sp<RigidBodyI> bodyA, SpatialVector pointA,
			sp<RigidBodyI> bodyB, SpatialVector pointB);
			// Real springK, Real dampingC,
			// Real restLength);
	virtual Real distance();
	virtual SpatialVector unitAxisA();
	virtual SpatialVector unitAxisB();
	virtual void step();

	virtual SpatialVector getPointA();
	virtual SpatialVector getPointB();

	// Real 			m_kSpring;
	// Real 			m_cDamping;
	// Real 			m_restLength;

protected:
	sp<RigidBodyI>	m_bodyA;
	SpatialVector	m_pointAInBodyCoordinates;
	sp<RigidBodyI>	m_bodyB;
	SpatialVector	m_pointBInBodyCoordinates;
};

} /* namespace hive */

#endif /* __rigid_body_engine_SpringDamperBase_h__ */
