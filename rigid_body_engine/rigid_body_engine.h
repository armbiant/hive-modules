/*	Copyright (C) 2018 Arrival Ltd
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit git.hiverepo.com */

/** @file */

#ifndef __rigid_body_engine_h__
#define __rigid_body_engine_h__

/// @defgroup rigid_body_engine Rigid Body Engine Interfaces
/// @ingroup hive

#include "fe/plugin.h"
#include "math/math.h"

#include "rigid_body_engine/RigidBodyI.h"
#include "rigid_body_engine/JointI.h"
#include "rigid_body_engine/SpringDamperI.h"
#include "rigid_body_engine/ConnectedSystemI.h"
#include "rigid_body_engine/EngineCommonI.h"

#endif // __rigid_body_engine_h__
