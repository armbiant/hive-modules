/*	Copyright (C) 2018 Arrival Ltd
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit git.hiverepo.com */

/** @file */

#include "rigid_body_engine/rigid_body_engine.pmh"
#include "platform/dlCore.cc"

using namespace fe;

extern "C"
{

    FE_DL_EXPORT void ListDependencies(List<String *> &list)
    {
    }

    FE_DL_EXPORT Library *CreateLibrary(sp<Master> spMaster)
    {
        Library *pLibrary = new Library();

		pLibrary->add<hive::SpringDamperTest>("SpringDamperI.SpringDamperTest.hive");

        return pLibrary;
    }

    //-----------------------------------------------------------------
    // InitializeLibrary()
    //-----------------------------------------------------------------

    FE_DL_EXPORT void InitializeLibrary(sp<Library> spLibrary)
    {
    }
}
