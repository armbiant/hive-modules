/** @file */

#include <rigid_body_engine/rigid_body_engine.h>

using namespace fe;
using namespace hive;

SpatialMatrix makeRotation(Real radians, Axis axis)
{
	SpatialMatrix rotation;
	setIdentity(rotation);
	rotateMatrix(rotation, radians, axis);
	return rotation;
}

int main(int argc, char** argv)
{
	UNIT_START();

	BWORD complete = FALSE;

	try
	{
		sp<Master> spMaster(new Master);
		sp<Registry> spRegistry = spMaster->registry();

		String engineType = "bullet"; // default
		if (argc > 1) // engine type will be first argument
		{
			engineType = argv[1];
		}


		Result result = spRegistry->manage("feAutoLoadDL");
		UNIT_TEST(successful(result));

		{
			// Test component creation
			sp<JointI> spJointI(spRegistry->create("JointI.*.*." + engineType));
			sp<RigidBodyI> spRigidBodyI(spRegistry->create("RigidBodyI.*.*." + engineType));
			sp<EngineCommonI> spEngineCommonI(spRegistry->create("EngineCommonI.*.*." + engineType));
			sp<ConnectedSystemI> spConnectedSystemI(spRegistry->create("ConnectedSystemI.*.*." + engineType));
			if (!spJointI.isValid() || !spRigidBodyI.isValid() ||
				!spEngineCommonI.isValid() || !spConnectedSystemI.isValid())
			{
				feX(argv[0], "couldn't create components");
			}
		}
		{
			// Creates rigid body system and intitializes engine
			// if not already active (singleton engine instance)
			sp<ConnectedSystemI> spConnectedSystemI(spRegistry->create("ConnectedSystemI.*.*." + engineType));
			Real dt = 0.01666;

			// Create two rigid bodies and connect them via a ball joint
			SpatialVector bodyPos1 = { 0, 0, 0 };
			SpatialVector bodyPos2 = { 2, 0, 0 };
			SpatialVector joint1Pos = { 2, 0, 0 };/// absolute position of joint in world coordinates


			SpatialMatrix spatialMatrix1;
			setIdentity(spatialMatrix1);

			SpatialMatrix spatialMatrix2;
			setIdentity(spatialMatrix2);;
#if 1
			/// Hardcoded pair of spheres and a ball joint
			sp<RigidBodyI> spGround = spConnectedSystemI->createRigidBody(0, bodyPos1, 0, false);
			sp<RigidBodyI> spRB1 = spConnectedSystemI->createBall(8, bodyPos1, 2);
			sp<JointI> spJ0 = spConnectedSystemI->createBallJoint(spGround, spRB1, bodyPos1);

			sp<RigidBodyI> spRB2 = spConnectedSystemI->createBall(1, bodyPos2, 0.1);
			sp<JointI> spJ1 = spConnectedSystemI->createBallJoint(spRB1, spRB2, joint1Pos);

			/// Rotate sphere2
			/// e_zAxis is some global define of sorts
			SpatialMatrix rotate = makeRotation(0.1, e_zAxis);
			spRB2->setRotation(rotate);

			/// TODO:
			//sp<JointI> spJ = spConnectedSystemI->createBallJoint(spRB1, spRB2, jointPos);
#else
			/// TODO: read RB setup from .json file using RapidJSON, or fexJson

			/// Below is RapidJson stub, read this from .json file
			const char json[] = " { \"hello\" : \"world\", \"t\" : true , \"f\" : false, \"n\": null, \"i\":123, \"pi\": 3.1416, \"a\":[1, 2, 3, 4] } ";

			JsonHandler handler;
			Reader reader;
			StringStream ss(json);
			reader.Parse(ss, handler);

#endif
			// Apply a force and torque to the bodies
			//spRB1->applyTorque({ 0,-1,0 });

			// Print initial state, before constraints are solved
			fprintf(stderr, "\n\nAt 0.0s:\t\t RB1\t\t\t\tRB2\n");
			SpatialVector pos1 = spRB1->getPosition();
			SpatialVector pos2 = spRB2->getPosition();
			SpatialVector vel1 = spRB1->getAngularVelocity();
			SpatialVector vel2 = spRB2->getAngularVelocity();
			SpatialVector angVel1 = spRB1->getAngularVelocity();
			SpatialVector angVel2 = spRB2->getAngularVelocity();
			fprintf(stderr, "Position:\t\t %f %f %f\t%f %f %f\n", pos1[0], pos1[1], pos1[2], pos2[0], pos2[1], pos2[2]);
			fprintf(stderr, "Velocity:\t\t %f %f %f\t%f %f %f\n", vel1[0], vel1[1], vel1[2], vel2[0], vel2[1], vel2[2]);
			fprintf(stderr, "Angular velocity:\t %f %f %f\t%f %f %f\n\n", angVel1[0], angVel1[1], angVel1[2], angVel2[0], angVel2[1], angVel2[2]);

			SpatialTransform rot1 = spRB1->getRotation();
			SpatialTransform rot2 = spRB2->getRotation();
#if 0

			// Set time step and solve constraints
			spConnectedSystemI->step(dt);

			// Check if the bodies moved as expected
			fprintf(stderr, "After 0.1s:\n");
			pos1 = spRB1->getPosition();
			pos2 = spRB2->getPosition();
			vel1 = spRB1->getAngularVelocity();
			vel2 = spRB2->getAngularVelocity();
			angVel1 = spRB1->getAngularVelocity();
			angVel2 = spRB2->getAngularVelocity();
			fprintf(stderr, "Position:\t\t %f %f %f\t%f %f %f\n", pos1[0], pos1[1], pos1[2], pos2[0], pos2[1], pos2[2]);
			fprintf(stderr, "Velocity:\t\t %f %f %f\t%f %f %f\n", vel1[0], vel1[1], vel1[2], vel2[0], vel2[1], vel2[2]);
			fprintf(stderr, "Angular velocity:\t %f %f %f\t%f %f %f\n\n", angVel1[0], angVel1[1], angVel1[2], angVel2[0], angVel2[1], angVel2[2]);

			// Take another step
			spConnectedSystemI->step(dt);

			// Check state again
			fprintf(stderr, "After 0.2s:\n");
			pos1 = spRB1->getPosition();
			pos2 = spRB2->getPosition();
			vel1 = spRB1->getAngularVelocity();
			vel2 = spRB2->getAngularVelocity();
			angVel1 = spRB1->getAngularVelocity();
			angVel2 = spRB2->getAngularVelocity();
			fprintf(stderr, "Position:\t\t %f %f %f\t%f %f %f\n", pos1[0], pos1[1], pos1[2], pos2[0], pos2[1], pos2[2]);
			fprintf(stderr, "Velocity:\t\t %f %f %f\t%f %f %f\n", vel1[0], vel1[1], vel1[2], vel2[0], vel2[1], vel2[2]);
			fprintf(stderr, "Angular velocity:\t %f %f %f\t%f %f %f\n\n", angVel1[0], angVel1[1], angVel1[2], angVel2[0], angVel2[1], angVel2[2]);
			feLog("done\n");

#else
			for (int i = 0; i < 1200; i++) /// 1 minute of simulation
			{
				spConnectedSystemI->step(dt);
				fprintf(stderr, "step %d: ", i);
				pos1 = spRB1->getPosition();
				pos2 = spRB2->getPosition();
				//fprintf(stderr, "Pos: %f %f %f,  %f %f %f;\n", pos1[0], pos1[1], pos1[2], pos2[0], pos2[1], pos2[2]);

				//angVel1 = spRB1->getAngularVelocity();
				//angVel2 = spRB2->getAngularVelocity();
				//fprintf(stderr, "Pos: %1.3f %1.3f %1.3f,  %1.3f %1.3f %1.3f; AngVel: %1.3f %1.3f %1.3f, %1.3f %1.3f %1.3f\n", pos1[0], pos1[1], pos1[2], pos2[0], pos2[1], pos2[2], angVel1[0], angVel1[1], angVel1[2], angVel2[0], angVel2[1], angVel2[2]);

				rot1 = spRB1->getRotation();
				rot2 = spRB2->getRotation();
				fprintf(stderr, "Pos: %1.3f %1.3f %1.3f,  %1.3f %1.3f %1.3f; Rot: %1.3f %1.3f %1.3f, %1.3f %1.3f %1.3f\n",
					pos1[0], pos1[1], pos1[2], pos2[0], pos2[1], pos2[2], rot1(0,0), rot1(1,1), rot1(2,2), rot2(0,0), rot2(1,1), rot2(2,2));

				spRB1->applyForce({ 0,0, 10 }, pos1);
				spRB2->applyForce({ 0,0,-10 }, pos2);
			}
			feLog("done\n");
#endif
		}

		complete = TRUE;
	}
	catch (Exception & e) { e.log(); }
	catch (...) { feLog("uncaught exception\n"); }

	UNIT_TEST(complete);
	UNIT_TEST(!Counted::trackerCount());
	UNIT_TRACK(3);
	UNIT_RETURN();
}
