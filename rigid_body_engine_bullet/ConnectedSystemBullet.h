/*	Copyright (C) 2018 Arrival Ltd
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit git.hiverepo.com */

/** @file */

#ifndef __rigid_body_engine_bullet_ConnectedSystemBullet_h__
#define __rigid_body_engine_bullet_ConnectedSystemBullet_h__

using namespace fe;

namespace hive
{

class FE_DL_EXPORT ConnectedSystemBullet : virtual public ConnectedSystemBase, public Initialize<ConnectedSystemBullet>
{
public:
	void					initialize();

	/// General case - requires a rotation as well as inertia matrix
	sp<RigidBodyI>			createRigidBody(const Real mass, const SpatialVector& position, const SpatialMatrix& rotation,
									const SpatialMatrix& inertia, bool bDynamic = true) override;

	/// Special case for spherical objects
	sp<RigidBodyI>			createRigidBody(const Real mass, const SpatialVector &position, const Real radius,
									bool bDynamic = true) override;

	/// These take the positions (and orientation, for Hinge joint) in coordinates relative to each of the rigid bodies.
	sp<JointI>				createHingeJoint(const sp<RigidBodyI> bodyA, const sp<RigidBodyI> bodyB,
									const SpatialVector& pivotInA, const SpatialVector& pivotInB,
									const SpatialVector& axisInA, const SpatialVector& axisInB) override;
	sp<JointI>				createBallJoint(const sp<RigidBodyI> bodyA, const sp<RigidBodyI> bodyB,
									const SpatialVector& pivotInA, const SpatialVector& pivotInB) override;

	/// These take the positions (and orientation, for Hinge joint) in world coordinates.
	sp<JointI>				createHingeJoint(const sp<RigidBodyI> bodyA, const sp<RigidBodyI> bodyB,
									const SpatialVector& pivotPos, const SpatialVector& axisDir) override;
	sp<JointI>				createBallJoint(const sp<RigidBodyI> bodyA, const sp<RigidBodyI> bodyB,
									const SpatialVector& pivotPos) override;

	sp<SpringDamperI> 		createSpringDamper(const sp<RigidBodyI> bodyA, const SpatialVector pointA,
								const sp<RigidBodyI> bodyB, const SpatialVector pointB) override;
	sp<SpringDamperI>		createSpringDamper(const sp<RigidBodyI> bodyA, const SpatialVector pointA,
								const sp<RigidBodyI> bodyB, const SpatialVector pointB,
								const Real a_springRate, const Real a_dampingConstant,
								const Real a_restLength);

	void 					step(const fe::Real dt) override;
	void					solve(void);

	void 					setGravity(const SpatialVector& gravity);
	void 					setGlobalCFM(const Real cfm);
	void 					setGlobalERP(const Real erp);
	void 					setGlobalLinearDamping(const Real damping);
	void 					setGlobalAngularDamping(const Real damping);
	std::vector<sp<RigidBodyI>>	getRigidBodies();

//private:
	sp<EngineCommonI>					m_spEngineCommon;
	sp<RigidBodyI>						m_bodies;
	Real 								m_linearDamping;
	Real 								m_angularDamping;

	btDefaultCollisionConfiguration* 	m_collisionConfiguration;
	btCollisionAlgorithm* 				m_btCollisionAlgorithm;
	btCollisionDispatcher* 				m_dispatcher;
	btBroadphaseInterface* 				m_overlappingPairCache;
	btSequentialImpulseConstraintSolver* m_solver;
	btDiscreteDynamicsWorld* 			m_dynamicsWorld;
	btContactSolverInfo* 				m_btContactSolverInfo;

	std::vector<sp<RigidBodyI>> 		m_rigidBodyList;
	std::vector<sp<JointI>> 			m_jointList;
	std::vector<sp<SpringDamperI>> 		m_springDamperList;
};

} /* namespace hive */

#endif /* __rigid_body_engine_bullet_ConnectedSystemBullet_h__ */
