/*	Copyright (C) 2018 Arrival Ltd
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit git.hiverepo.com */

/** @file */

#include "rigid_body_engine_bullet.pmh"

namespace hive
{

JointBullet::JointBullet()
{
}

JointBullet::~JointBullet()
{
	//dJointDestroy(m_jointID);
}

void JointBullet::initBallJoint(const sp<RigidBodyI> bodyA, const sp<RigidBodyI> bodyB,
	const SpatialVector pivotInA, const SpatialVector pivotInB)
{
	m_jointType = "ball";

	sp<RigidBodyBullet> bodyA_bullet = bodyA;
	sp<RigidBodyBullet> bodyB_bullet = bodyB;

	btRigidBody* btBodyA = bodyA_bullet->m_btRigidBody;
	btRigidBody* btBodyB = bodyB_bullet->m_btRigidBody;
	btVector3 btPivotInA(pivotInA[0], pivotInA[1], pivotInA[2]);
	btVector3 btPivotInB(pivotInB[0], pivotInB[1], pivotInB[2]);
	m_btPoint2PointConstraint = new btPoint2PointConstraint(*btBodyA, *btBodyB, btPivotInA, btPivotInB);
}

void JointBullet::initHingeJoint(const sp<RigidBodyI> bodyA, const sp<RigidBodyI> bodyB,
	const SpatialVector pivotInA, const SpatialVector pivotInB,
	const SpatialVector axisInA, const SpatialVector axisInB)
{
	m_jointType = "hinge";

	sp<RigidBodyBullet> bodyA_bullet = bodyA;
	sp<RigidBodyBullet> bodyB_bullet = bodyB;

	btRigidBody* btBodyA = bodyA_bullet->m_btRigidBody;
	btRigidBody* btBodyB = bodyB_bullet->m_btRigidBody;
	btVector3 btPivotInA(pivotInA[0], pivotInA[1], pivotInA[2]);
	btVector3 btPivotInB(pivotInB[0], pivotInB[1], pivotInB[2]);
	btVector3 btAxisInA(axisInA[0], axisInA[1], axisInA[2]);
	btVector3 btAxisInB(axisInB[0], axisInB[1], axisInB[2]);
	m_btHingeConstraint = new btHingeConstraint(*btBodyA, *btBodyB, btPivotInA, btPivotInB, btAxisInA, btAxisInB);
}

void JointBullet::initBallJoint(const sp<RigidBodyI> bodyA, const sp<RigidBodyI> bodyB,
	const SpatialVector pivotPos)
{
	m_jointType = "ball";

	sp<RigidBodyBullet> bodyA_bullet = bodyA;
	sp<RigidBodyBullet> bodyB_bullet = bodyB;

	btRigidBody* btBodyA = bodyA_bullet->m_btRigidBody;
	btRigidBody* btBodyB = bodyB_bullet->m_btRigidBody;

	btVector3 btPivotInA(pivotPos[0], pivotPos[1], pivotPos[2]);
	btVector3 btPivotInB(pivotPos[0], pivotPos[1], pivotPos[2]);

	btTransform bodyAtransform = btBodyA->getCenterOfMassTransform();
	btTransform bodyBtransform = btBodyB->getCenterOfMassTransform();

	btPivotInA = bodyAtransform.invXform(btPivotInA);
	btPivotInB = bodyBtransform.invXform(btPivotInB);

	m_btPoint2PointConstraint = new btPoint2PointConstraint(*btBodyA, *btBodyB, btPivotInA, btPivotInB);
}

void JointBullet::initHingeJoint(const sp<RigidBodyI> bodyA, const sp<RigidBodyI> bodyB,
	const SpatialVector pivotPos, const SpatialVector axisDir)
{
	m_jointType = "hinge";

	sp<RigidBodyBullet> bodyA_bullet = bodyA;
	sp<RigidBodyBullet> bodyB_bullet = bodyB;

	btRigidBody* btBodyA = bodyA_bullet->m_btRigidBody;
	btRigidBody* btBodyB = bodyB_bullet->m_btRigidBody;

	btVector3 btPivotInA(pivotPos[0], pivotPos[1], pivotPos[2]);
	btVector3 btPivotInB(pivotPos[0], pivotPos[1], pivotPos[2]);

	btTransform bodyAtransform = btBodyA->getCenterOfMassTransform();
	btTransform bodyBtransform = btBodyB->getCenterOfMassTransform();

	btPivotInA = bodyAtransform.invXform(btPivotInA);
	btPivotInB = bodyBtransform.invXform(btPivotInB);

	btVector3 btAxisInA(axisDir[0], axisDir[1], axisDir[2]);
	btVector3 btAxisInB(axisDir[0], axisDir[1], axisDir[2]);

	btAxisInA = btAxisInA * bodyAtransform.getBasis();
	btAxisInB = btAxisInB * bodyBtransform.getBasis();

	m_btHingeConstraint = new btHingeConstraint(*btBodyA, *btBodyB, btPivotInA, btPivotInB, btAxisInA.normalize(), btAxisInB.normalize());
}


void JointBullet::limitRotation(Real angleLo, Real angleHi)
{
	//dJointSetHingeParam(m_jointID, dParamLoStop, angleLo);
	//dJointSetHingeParam(m_jointID, dParamHiStop, angleHi);
}

void JointBullet::setSpring(Real timestep, Real stiffness, Real damping)
{
	if (stiffness == 0.0 && damping == 0.0) { return; }
	Real erp = (timestep * stiffness) /
		(timestep * stiffness + damping);
	Real cfm = 1.0 / (timestep * stiffness + damping);
	//dJointSetHingeParam(m_jointID, dParamStopERP, erp);
	//dJointSetHingeParam(m_jointID, dParamStopCFM, cfm);
}

void JointBullet::setCFM(Real cfm)
{
	btScalar btCfm(cfm);
	setParam(BT_CONSTRAINT_CFM, btCfm);
}

void JointBullet::setStopCFM(Real cfm)
{
	btScalar btCfm(cfm);
	setParam(BT_CONSTRAINT_STOP_CFM, btCfm);
}

void JointBullet::setStopERP(Real erp)
{
	btScalar btErp(erp);
	setParam(BT_CONSTRAINT_STOP_ERP, btErp);
}

void JointBullet::setParam(int param, Real value)
{
	btScalar btVal(value);
	if (m_jointType == "ball")
	{
		m_btPoint2PointConstraint->setParam(param, btVal);
	}
	else if (m_jointType == "hinge")
	{
		m_btHingeConstraint->setParam(param, btVal);
	}
	else
	{
		// UNSUPPORTED JOINT TYPE
	}
}

} /* namespace hive */
