/*	Copyright (C) 2018 Arrival Ltd
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit git.hiverepo.com */

/** @file */

#include "rigid_body_engine_bullet.pmh"

using namespace fe;

namespace hive
{

RigidBodyBullet::RigidBodyBullet()
{

}

RigidBodyBullet::~RigidBodyBullet()
{

}

void RigidBodyBullet::init(const Real a_mass, const Real a_radius,
	const SpatialVector& a_pos, const SpatialMatrix& a_rot,
	const ConnectedSystemBullet* pConnectedSystem, bool bDynamic)
{
	btCollisionShape* colShape = new btSphereShape(btScalar(a_radius));
	// collisionShapes.push_back(colShape);

	btTransform groundTransform;
	groundTransform.setIdentity();
	groundTransform.setOrigin(btVector3(a_pos[0], a_pos[1], a_pos[2]));

	// how to properly init a matrix from a SpatialMatrix?
	btMatrix3x3 basisMatrix(a_rot(0, 0), a_rot(0, 1), a_rot(0, 2),
							a_rot(1, 0), a_rot(1, 1), a_rot(1, 2),
							a_rot(2, 0), a_rot(2, 1), a_rot(2, 2));

	groundTransform.setBasis(basisMatrix);

	btScalar mass(a_mass);
	btVector3 localInertia(0, 0, 0);
	if(bDynamic && a_mass > 0) {
		/// Create a dynamic RB
		colShape->calculateLocalInertia(mass, localInertia);
		//using motionstate is optional, it provides interpolation
		//capabilities, and only synchronizes 'active' objects
		btDefaultMotionState* myMotionState =
			new btDefaultMotionState(groundTransform);
		btRigidBody::btRigidBodyConstructionInfo
			rbInfo(mass, myMotionState, colShape, localInertia);
		m_btRigidBody = new btRigidBody(rbInfo);
		m_btRigidBody->setCollisionFlags(
			m_btRigidBody->getCollisionFlags() |
			btCollisionObject::CF_NO_CONTACT_RESPONSE);
	}
	else {
		/// Create a kinematic RB, which is also static by default
		btDefaultMotionState* myMotionState =
			new btDefaultMotionState(groundTransform);
		btRigidBody::btRigidBodyConstructionInfo
			rbInfo(mass, myMotionState, colShape, localInertia);
		m_btRigidBody = new btRigidBody(rbInfo);
		m_btRigidBody->setCollisionFlags(
			m_btRigidBody->getCollisionFlags() |
			btCollisionObject::CF_KINEMATIC_OBJECT |
			btCollisionObject::CF_NO_CONTACT_RESPONSE);
	}

	pConnectedSystem->m_dynamicsWorld->addRigidBody(m_btRigidBody);
}

void RigidBodyBullet::init(const Real a_mass, const SpatialMatrix& inertia,
	const SpatialVector& a_pos, const SpatialMatrix& a_rot,
	const ConnectedSystemBullet* pConnectedSystem, bool bDynamic)
{
	btTransform groundTransform;
	groundTransform.setIdentity();
	groundTransform.setOrigin(btVector3(a_pos[0], a_pos[1], a_pos[2]));

	/// how to properly init a matrix from a SpatialMatrix?
	btMatrix3x3 basisMatrix(a_rot(0, 0), a_rot(0, 1), a_rot(0, 2),
		a_rot(1, 0), a_rot(1, 1), a_rot(1, 2),
		a_rot(2, 0), a_rot(2, 1), a_rot(2, 2));

	groundTransform.setBasis(basisMatrix);

	btScalar mass(a_mass);
	btVector3 localInertia(0, 0, 0);
	if (bDynamic && a_mass > 0) {
		/// Create a dynamic RB
		/// we're using only the main diagonal here
		localInertia[0] = inertia(0, 0);
		localInertia[1] = inertia(1, 1);
		localInertia[2] = inertia(2, 2);
		btCollisionShape* colShape = new btSphereShape(btScalar(0.001));
		//using motionstate is optional, it provides interpolation
		//capabilities, and only synchronizes 'active' objects
		btDefaultMotionState* myMotionState =
			new btDefaultMotionState(groundTransform);
		btRigidBody::btRigidBodyConstructionInfo
			rbInfo(mass, myMotionState, colShape, localInertia);
		m_btRigidBody = new btRigidBody(rbInfo);
		m_btRigidBody->setCollisionFlags(
			m_btRigidBody->getCollisionFlags() |
			btCollisionObject::CF_NO_CONTACT_RESPONSE);
	}
	else {
		btCollisionShape* colShape = new btSphereShape(btScalar(0.001));
		//using motionstate is optional,  it provides interpolation
		//capabilities, and only synchronizes 'active' objects
		btDefaultMotionState* myMotionState =
			new btDefaultMotionState(groundTransform);
		btRigidBody::btRigidBodyConstructionInfo
			rbInfo(mass, myMotionState, colShape, localInertia);
		m_btRigidBody = new btRigidBody(rbInfo);
		m_btRigidBody->setCollisionFlags(
			m_btRigidBody->getCollisionFlags() |
			btCollisionObject::CF_KINEMATIC_OBJECT |
			btCollisionObject::CF_NO_CONTACT_RESPONSE);
	}

	pConnectedSystem->m_dynamicsWorld->addRigidBody(m_btRigidBody);
}


void RigidBodyBullet::setMass(const Real mass)
{
	// TODO
	/// doesn't do anything, and should only be set during initialization.
}

Real RigidBodyBullet::getMass(void) const
{
	return m_btRigidBody->getMass();
}

void RigidBodyBullet::setPosition(const SpatialVector& pos)
{
	btVector3 btNewPos(pos[0], pos[1], pos[2]);
	btVector3 btOldPos = m_btRigidBody->getCenterOfMassPosition();
	btVector3 btTranslate = btNewPos - btOldPos;

	m_btRigidBody->translate(btTranslate);
}

SpatialVector RigidBodyBullet::getPosition(void) const
{
	btVector3 btPos = m_btRigidBody->getCenterOfMassPosition();
	SpatialVector pos;
	pos[0] = btPos[0];
	pos[1] = btPos[1];
	pos[2] = btPos[2];

	return pos;
}

void RigidBodyBullet::setRotation(const SpatialMatrix& a_rot)
{
	btVector3 btOldPos = m_btRigidBody->getCenterOfMassPosition();
	btTransform groundTransform;
	groundTransform.setIdentity();
	groundTransform.setOrigin(btOldPos);
	btMatrix3x3 basisMatrix(a_rot(0, 0), a_rot(0, 1), a_rot(0, 2),
		a_rot(1, 0), a_rot(1, 1), a_rot(1, 2),
		a_rot(2, 0), a_rot(2, 1), a_rot(2, 2));
	groundTransform.setBasis(basisMatrix);

	m_btRigidBody->setCenterOfMassTransform(groundTransform);
}

SpatialMatrix RigidBodyBullet::getRotation(void) const
{
	SpatialMatrix rotation;
	btTransform transform = m_btRigidBody->getCenterOfMassTransform();
	btMatrix3x3 basis = transform.getBasis();

	rotation(0, 0) = basis[0][0];
	rotation(0, 1) = basis[0][1];
	rotation(0, 2) = basis[0][2];
	rotation(1, 0) = basis[1][0];
	rotation(1, 1) = basis[1][1];
	rotation(1, 2) = basis[1][2];
	rotation(2, 0) = basis[2][0];
	rotation(2, 1) = basis[2][1];
	rotation(2, 2) = basis[2][2];

	return rotation;
}

void RigidBodyBullet::setTransform(const SpatialTransform &transform)
{
	SpatialMatrix rotation;
	rotation(0,0) = transform(0,0);
	rotation(0,1) = transform(0,1);
	rotation(0,2) = transform(0,2);
	rotation(1,0) = transform(1,0);
	rotation(1,1) = transform(1,1);
	rotation(1,2) = transform(1,2);
	rotation(2,0) = transform(2,0);
	rotation(2,1) = transform(2,1);
	rotation(2,2) = transform(2,2);

	setRotation(rotation);
	setPosition(transform.column(3));
}

SpatialTransform RigidBodyBullet::getTransform(void) const
{
	SpatialTransform transform;
	fe::setRotation(transform, getRotation());
	fe::setTranslation(transform, getPosition());
	return transform;
}

void RigidBodyBullet::applyForceRelative(const SpatialVector& force,
	const SpatialVector& pos)
{
	btVector3 btForce(force[0], force[1], force[2]);
	btVector3 btPos(pos[0], pos[1], pos[2]);
	m_btRigidBody->applyForce(btForce, btPos);
}

void RigidBodyBullet::applyForce(const SpatialVector& force,
	const SpatialVector& pos)
{
	btVector3 btGlobalForce(force[0], force[1], force[2]);
	btVector3 btGlobalForcePos(pos[0], pos[1], pos[2]);

	btTransform btBodyTransform = m_btRigidBody->getCenterOfMassTransform();
	btTransform btBodyTransformInverse = btBodyTransform.inverse();

	btVector3 btLocalForce	  = btBodyTransformInverse * btGlobalForce;
	btVector3 btLocalForcePos = btBodyTransformInverse * btGlobalForcePos;

	m_btRigidBody->applyForce(btGlobalForce, btLocalForcePos);
}

void RigidBodyBullet::applyTorque(const SpatialVector& torque)
{
	btVector3 btTorque(torque[0], torque[1], torque[2]);
	m_btRigidBody->applyTorque(btTorque);
}

void RigidBodyBullet::setVelocity(const SpatialVector &vel)
{
	btVector3 btVel(vel[0], vel[1], vel[2]);
	m_btRigidBody->setLinearVelocity(btVel);
}

SpatialVector RigidBodyBullet::getVelocity(const SpatialVector& pos) const
{
	SpatialVector velocity;
	btVector3 btVelocity = m_btRigidBody->getLinearVelocity();
	velocity[0] = btVelocity[0];
	velocity[1] = btVelocity[1];
	velocity[2] = btVelocity[2];
	return velocity;
}

void RigidBodyBullet::setAngularVelocity(const SpatialVector &angVel)
{
	btVector3 btAngVel(angVel[0], angVel[1], angVel[2]);
	m_btRigidBody->setAngularVelocity(btAngVel);
}

SpatialVector RigidBodyBullet::getAngularVelocity(void) const
{
	SpatialVector angularVelocity;
	btVector3 btAngularVelocity = m_btRigidBody->getAngularVelocity();
	angularVelocity[0] = btAngularVelocity[0];
	angularVelocity[1] = btAngularVelocity[1];
	angularVelocity[2] = btAngularVelocity[2];
	return angularVelocity;
}

void RigidBodyBullet::translate(const SpatialVector& displ)
{
	btVector3 v(displ[0], displ[1], displ[2]);
	m_btRigidBody->translate(v);
}

void RigidBodyBullet::setLinearDamping(const Real damping)
{
	btScalar ld = btScalar(damping);
	btScalar ad = m_btRigidBody->getAngularDamping();
	m_btRigidBody->setDamping(ld, ad);
}

void RigidBodyBullet::setAngularDamping(const Real damping)
{
	btScalar ld = m_btRigidBody->getLinearDamping();
	btScalar ad = btScalar(damping);
	m_btRigidBody->setDamping(ld, ad);
}

} /* namespace hive */
