env = Environment(TARGET_ARCH = 'x86_64', CXXFLAGS = '/MDd')

source_TaskScheduler = Glob('LinearMath/TastScheduler/*.cpp')
env.StaticLibrary('LinearMath', [Glob('LinearMath/*.cpp'), source_TaskScheduler])

source_BroadphaseCollision	= Glob('BulletCollision/BroadphaseCollision/*.cpp')
source_CollisionDispatch	= Glob('BulletCollision/CollisionDispatch/*.cpp')
source_CollisionShapes		= Glob('BulletCollision/CollisionShapes/*.cpp')
source_Gimpact				= Glob('BulletCollision/Gimpact/*.cpp')
source_NarrowPhaseCollision	= Glob('BulletCollision/NarrowPhaseCollision/*.cpp')
source_BulletCollisionSource = [source_BroadphaseCollision,	source_CollisionDispatch, source_CollisionShapes, source_Gimpact, source_NarrowPhaseCollision]
env.StaticLibrary('BulletCollision', source_BulletCollisionSource, CPPPATH = '.')

source_Character			= Glob('BulletDynamics/Character/*.cpp')
source_ConstraintSolver		= Glob('BulletDynamics/ConstraintSolver/*.cpp')
source_Dynamics				= Glob('BulletDynamics/Dynamics/*.cpp')
source_Featherstone			= Glob('BulletDynamics/Featherstone/*.cpp')
source_MLCPSolvers			= Glob('BulletDynamics/MLCPSolvers/*.cpp')
source_Vehicle				= Glob('BulletDynamics/Vehicle/*.cpp')
source_BulletDynamicsSource = [source_Character, source_ConstraintSolver, source_Dynamics, source_Featherstone, source_MLCPSolvers, source_Vehicle]
env.StaticLibrary('BulletDynamics', source_BulletDynamicsSource, CPPPATH = '.')
