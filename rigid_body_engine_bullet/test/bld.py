import sys
forge = sys.modules["forge"]

def setup(module):
	tests = [ 'xRigidBodyEngineBullet' ]

	deplibs = forge.basiclibs + [
								"fexDataToolLib",
								"feDataLib",
								"fePluginLib",
								"feMathLib"]

	if forge.fe_os == 'FE_WIN32' or forge.fe_os == 'FE_WIN64':
		deplibs += [ "hiveRigidBodyEngineDLLib" ]

	for t in tests:
		exe = module.Exe(t)
		forge.deps([t + "Exe"], deplibs)
