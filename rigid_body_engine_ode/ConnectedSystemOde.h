/*	Copyright (C) 2018 Arrival Ltd
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit git.hiverepo.com */

/** @file */

#ifndef __rigid_body_engine_ode_ConnectedSystemOde_h__
#define __rigid_body_engine_ode_ConnectedSystemOde_h__

using namespace fe;

namespace hive
{

class FE_DL_EXPORT ConnectedSystemOde : virtual public ConnectedSystemBase, public Initialize<ConnectedSystemOde>
{
public:
	void				initialize();

	// In addition to these functions, has:
	// sp<RigidBodyI> create*() method where
	// * = ball, rod, tube, and bar
	// already implemented in "dirty" interface which inherits ConnectedSystemI
	// These functions in turn call this first createRigidBody() method

	/// General case - requires a rotation as well as inertia matrix
	sp<RigidBodyI>			createRigidBody(const Real mass, const SpatialVector& position,
									const SpatialMatrix &rotation, const SpatialMatrix& inertia,
									bool bDynamic = true);

	/// Convenience function for a spherical rigid body - rotation is always setIdentity()
	sp<RigidBodyI>			createRigidBody(const Real mass, const SpatialVector &position,
									const Real radius, bool bDynamic = true);

	// Joints
	/// These take the positions (and orientation, for Hinge joint) in coordinates relative to each of the rigid bodies.
	sp<JointI>				createHingeJoint(const sp<RigidBodyI> bodyA, const sp<RigidBodyI> bodyB,
									const SpatialVector& pivotInA, const SpatialVector& pivotInB,
									const SpatialVector& axisInA, const SpatialVector& axisInB);
	sp<JointI>				createBallJoint(const sp<RigidBodyI> bodyA, const sp<RigidBodyI> bodyB,
									const SpatialVector& pivotInA, const SpatialVector& pivotInB);

	/// These take the positions (and orientation, for Hinge joint) in world coordinates.
	sp<JointI>				createHingeJoint(const sp<RigidBodyI> bodyA, const sp<RigidBodyI> bodyB,
									const SpatialVector& pivotPos, const SpatialVector& axisDir);
	sp<JointI>				createBallJoint(const sp<RigidBodyI> bodyA, const sp<RigidBodyI> bodyB,
									const SpatialVector& pivotPos);

	sp<SpringDamperI> 		createSpringDamper(const sp<RigidBodyI> bodyA, const SpatialVector pointA,
								const sp<RigidBodyI> bodyB, const SpatialVector pointB) override;
	sp<SpringDamperI>		createSpringDamper(const sp<RigidBodyI> bodyA, const SpatialVector pointA,
								const sp<RigidBodyI> bodyB, const SpatialVector pointB,
								const Real a_springRate, const Real a_dampingConstant,
								const Real a_restLength);

	// Accessors for world parameters
	void					setGravity(const SpatialVector& gravity);
	void					setGlobalCFM(const Real cfm);
	void					setGlobalERP(const Real erp);
	void					setGlobalLinearDamping(const Real damping);
	void					setGlobalAngularDamping(const Real damping);

	// Advance the simulation by dt
	void					step(Real dt);

	std::vector<sp<RigidBodyI>>	getRigidBodies();

private:
	sp<EngineCommonI>				m_spCommon;
	dWorldID						m_worldID;
	std::vector<sp<RigidBodyI>>		m_rigidBodyList;
	std::vector<sp<JointI>>			m_jointList;
	std::vector<sp<SpringDamperI>> 	m_springDamperList;
	Real 							m_linearDamping;
	Real 							m_angularDamping;
};

} /* namespace hive */

#endif /* __rigid_body_engine_ode_ConnectedSystemOde_h__ */
