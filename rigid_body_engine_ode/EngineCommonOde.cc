/*	Copyright (C) 2018 Arrival Ltd
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit git.hiverepo.com */

/** @file */

#include "rigid_body_engine_ode.pmh"

namespace hive {

EngineCommonOde::EngineCommonOde()
{

}

EngineCommonOde::~EngineCommonOde()
{
	dCloseODE();
}

void EngineCommonOde::initialize()
{
	dInitODE();
}

}
