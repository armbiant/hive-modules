/*	Copyright (C) 2018 Arrival Ltd
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit git.hiverepo.com */

/** @file */

#ifndef __rigid_body_engine_ode_EngineCommonOde_h__
#define __rigid_body_engine_ode_EngineCommonOde_h__

using namespace fe;

namespace hive
{

class FE_DL_EXPORT EngineCommonOde : virtual public EngineCommonI, public Initialize<EngineCommonOde>
{
public:
	EngineCommonOde();
	~EngineCommonOde();

	void					initialize();
};

} /* namespace hive */

#endif /* __rigid_body_engine_ode_ConnectedSystemOde_h__ */
