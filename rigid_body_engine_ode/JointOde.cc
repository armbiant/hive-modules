/*	Copyright (C) 2018 Arrival Ltd
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit git.hiverepo.com */

/** @file */

#include "rigid_body_engine_ode.pmh"

namespace hive
{

JointOde::JointOde()
{
}

JointOde::~JointOde()
{
	//dJointDestroy(m_jointID);
}

void JointOde::specifyBallJoint(const sp<RigidBodyI> bodyA, const sp<RigidBodyI> bodyB,
	const SpatialVector pivotInA, const SpatialVector pivotInB,
	const sp<EngineCommonOde> spCommon)
{
	m_jointType = "ball";

	sp<RigidBodyOde> spBodyOde = bodyA;
	m_worldID = spBodyOde->getWorldID();
	m_spCommon = spCommon;

	m_jointID = dJointCreateBall(m_worldID, 0);

	sp<RigidBodyOde> bodyA_ode = bodyA;
	sp<RigidBodyOde> bodyB_ode = bodyB;

	dBodyID id_a = 0;
	if (bodyA_ode.isValid())
	{
		id_a = bodyA_ode->getBodyID();
	}

	dBodyID id_b = 0;
	if (bodyB_ode.isValid())
	{
		id_b = bodyB_ode->getBodyID();
	}

	dJointAttach(m_jointID, id_a, id_b);

	// Translate anchor from body space to world space
	dVector3 pivotInWorld;
	dBodyGetRelPointPos(id_b, dReal(pivotInB[0]), dReal(pivotInB[1]), dReal(pivotInB[2]), pivotInWorld);
	dJointSetBallAnchor(m_jointID, pivotInWorld[0], pivotInWorld[1], pivotInWorld[2]);
}

void JointOde::specifyHingeJoint(const sp<RigidBodyI> bodyA, const sp<RigidBodyI> bodyB,
	const SpatialVector pivotInA, const SpatialVector pivotInB,
	const SpatialVector axisInA, const SpatialVector axisInB,
	const sp<EngineCommonOde> spCommon)
{
	m_jointType = "hinge";

	sp<RigidBodyOde> spBodyOde = bodyA;
	m_worldID = spBodyOde->getWorldID();
	m_spCommon = spCommon;

	m_jointID = dJointCreateHinge(m_worldID, 0);

	sp<RigidBodyOde> bodyA_ode = bodyA;
	sp<RigidBodyOde> bodyB_ode = bodyB;

	dBodyID id_a = bodyA_ode->getBodyID();
	dBodyID id_b = bodyB_ode->getBodyID();

	dJointAttach(m_jointID, id_a, id_b);

	// Translate axis from body space to world space
	dVector3 axisInWorld;
	dBodyGetRelPointPos(id_a, dReal(axisInA[0]), dReal(axisInA[1]), dReal(axisInA[2]), axisInWorld);
	dJointSetHingeAxis(m_jointID, axisInWorld[0], axisInWorld[1], axisInWorld[2]);

	// Translate anchor from body space to world space
	dVector3 pivotInWorld;
	dBodyGetRelPointPos(id_a, dReal(pivotInA[0]), dReal(pivotInA[1]), dReal(pivotInA[2]), pivotInWorld);
	dJointSetHingeAnchor(m_jointID, pivotInWorld[0], pivotInWorld[1], pivotInWorld[2]);
}

void JointOde::specifyBallJoint(const sp<RigidBodyI> bodyA, const sp<RigidBodyI> bodyB,
	const SpatialVector pivotPos, const sp<EngineCommonOde> spCommon)
{
	sp<RigidBodyOde> spBodyOde = bodyA;
	m_worldID = spBodyOde->getWorldID();
	m_spCommon = spCommon;

	m_jointID = dJointCreateBall(m_worldID, 0);

	sp<RigidBodyOde> bodyA_ode = bodyA;
	sp<RigidBodyOde> bodyB_ode = bodyB;

	dBodyID id_a = 0;
	if (bodyA_ode.isValid())
	{
		id_a = bodyA_ode->getBodyID();
	}

	dBodyID id_b = 0;
	if (bodyB_ode.isValid())
	{
		id_b = bodyB_ode->getBodyID();
	}

	dJointAttach(m_jointID, id_a, id_b);

	dVector3 pivotInWorld;
	pivotInWorld[0] = dReal(pivotPos[0]);
	pivotInWorld[1] = dReal(pivotPos[1]);
	pivotInWorld[2] = dReal(pivotPos[2]);
	dJointSetBallAnchor(m_jointID, pivotInWorld[0], pivotInWorld[1], pivotInWorld[2]);
}

void JointOde::specifyHingeJoint(const sp<RigidBodyI> bodyA, const sp<RigidBodyI> bodyB,
	const SpatialVector pivotPos, const SpatialVector axisDir,
	const sp<EngineCommonOde> spCommon)
{
	sp<RigidBodyOde> spBodyOde = bodyA;
	m_worldID = spBodyOde->getWorldID();
	m_spCommon = spCommon;

	m_jointID = dJointCreateHinge(m_worldID, 0);

	sp<RigidBodyOde> bodyA_ode = bodyA;
	sp<RigidBodyOde> bodyB_ode = bodyB;

	dBodyID id_a = bodyA_ode->getBodyID();
	dBodyID id_b = bodyB_ode->getBodyID();

	dJointAttach(m_jointID, id_a, id_b);

	dVector3 axisInWorld;
	axisInWorld[0] = dReal(axisDir[0]);
	axisInWorld[1] = dReal(axisDir[1]);
	axisInWorld[2] = dReal(axisDir[2]);
	dJointSetHingeAxis(m_jointID, axisInWorld[0], axisInWorld[1], axisInWorld[2]);

	dVector3 pivotInWorld;
	pivotInWorld[0] = dReal(pivotPos[0]);
	pivotInWorld[1] = dReal(pivotPos[1]);
	pivotInWorld[2] = dReal(pivotPos[2]);
	dJointSetHingeAnchor(m_jointID, pivotInWorld[0], pivotInWorld[1], pivotInWorld[2]);
}


void JointOde::limitRotation(Real angleLo, Real angleHi)
{
	dJointSetHingeParam(m_jointID, dReal(dParamLoStop), dReal(angleLo));
	dJointSetHingeParam(m_jointID, dReal(dParamHiStop), dReal(angleHi));
}

void JointOde::setSpring(Real timestep, Real stiffness, Real damping)
{
	if (dReal(stiffness) == 0.0 && dReal(damping == 0.0)) { return; }
	Real erp = (timestep * stiffness) /
		(timestep * stiffness + damping);
	Real cfm = 1.0 / (timestep * stiffness + damping);
	dJointSetHingeParam(m_jointID, dParamStopERP, dReal(erp));
	dJointSetHingeParam(m_jointID, dParamStopCFM, dReal(cfm));
}

void JointOde::setCFM(Real cfm)
{
	setParam(dParamCFM, cfm);
}

void JointOde::setStopCFM(Real cfm)
{
	setParam(dParamStopCFM, cfm);
}

void JointOde::setStopERP(Real erp)
{
	setParam(dParamStopERP, erp);
}

void JointOde::setParam(int param, Real value)
{
	dReal dValue = dReal(value);

	if (m_jointType == "ball")
	{
		dJointSetBallParam(m_jointID, param, dValue);
	}
	else if (m_jointType == "hinge")
	{
		dJointSetHingeParam(m_jointID, param, dValue);
	}
	else
	{
		// UNSUPPORTED JOINT TYPE
	}
}

} /* namespace hive */
