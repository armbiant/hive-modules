/*	Copyright (C) 2018 Arrival Ltd
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit git.hiverepo.com */

/** @file */

#include "rigid_body_engine_ode.pmh"

using namespace fe;

namespace hive
{

RigidBodyOde::RigidBodyOde()
{

}

RigidBodyOde::~RigidBodyOde()
{
	// Should not destroy body in constructor, since it is already destroyed
	// when the RigidBodyEngineContextOde is destroyed.
}

void RigidBodyOde::specify(const Real mass, const SpatialVector& position,
	const SpatialMatrix& rotation, const SpatialMatrix& inertia,
	dWorldID worldID, const sp<EngineCommonI> spCommon, bool bDynamic)
{
	/// Eigen - insert eigen solve and rotation here
	Eigen::Matrix3d eigenInertia;
	eigenInertia(0, 0) = inertia(0, 0);	eigenInertia(0, 1) = inertia(0, 1);	eigenInertia(0, 2) = inertia(0, 2);
	eigenInertia(1, 0) = inertia(1, 0);	eigenInertia(1, 1) = inertia(1, 1);	eigenInertia(1, 2) = inertia(1, 2);
	eigenInertia(2, 0) = inertia(2, 0); eigenInertia(2, 1) = inertia(2, 1); eigenInertia(2, 2) = inertia(2, 2);

	Eigen::SelfAdjointEigenSolver<Eigen::Matrix3d> inertiaSolver;
	inertiaSolver.compute(eigenInertia, Eigen::ComputeEigenvectors);

	Eigen::Matrix3d eigenVectors = inertiaSolver.eigenvectors();	// This IS the rotation matrix!
	/// SpatialMatrix m_eigenRotation = eigenVectors;
	m_eigenRotation(0, 0) = eigenVectors(0, 0);	m_eigenRotation(0, 1) = eigenVectors(0, 1);	m_eigenRotation(0, 2) = eigenVectors(0, 2);
	m_eigenRotation(1, 0) = eigenVectors(1, 0);	m_eigenRotation(1, 1) = eigenVectors(1, 1);	m_eigenRotation(1, 2) = eigenVectors(1, 2);
	m_eigenRotation(2, 0) = eigenVectors(2, 0); m_eigenRotation(2, 1) = eigenVectors(2, 1); m_eigenRotation(2, 2) = eigenVectors(2, 2);

	/// SpatialMatrix m_eigenRotationInverse = eigenVectors.inverse();
	Eigen::Matrix3d eigenVectorsInverse = eigenVectors.inverse();
	m_eigenRotationInverse(0, 0) = eigenVectorsInverse(0, 0);	m_eigenRotationInverse(0, 1) = eigenVectorsInverse(0, 1);	m_eigenRotationInverse(0, 2) = eigenVectorsInverse(0, 2);
	m_eigenRotationInverse(1, 0) = eigenVectorsInverse(1, 0);	m_eigenRotationInverse(1, 1) = eigenVectorsInverse(1, 1);	m_eigenRotationInverse(1, 2) = eigenVectorsInverse(1, 2);
	m_eigenRotationInverse(2, 0) = eigenVectorsInverse(2, 0);	m_eigenRotationInverse(2, 1) = eigenVectorsInverse(2, 1);	m_eigenRotationInverse(2, 2) = eigenVectorsInverse(2, 2);

	m_worldID = worldID;
	m_spCommon = spCommon;
	m_bodyID = dBodyCreate(m_worldID);

	// Must cast all inputs to dReal
	if (bDynamic) {
		dMass* dm = new dMass();
		dMassSetParameters(dm, dReal(mass), 0, 0, 0,
			dReal(inertia(0, 0)), dReal(inertia(1, 1)), dReal(inertia(2, 2)),
			dReal(inertia(0, 1)), dReal(inertia(0, 2)), dReal(inertia(1, 2)));
		dBodySetMass(m_bodyID, dm);
		setPosition(position);
		setRotation(rotation);
	}
	else {
		dBodySetKinematic(m_bodyID);
	}
}

void RigidBodyOde::specify(const Real mass, const SpatialVector& position,
	const Real radius, dWorldID worldID,
	const sp<EngineCommonI> spCommon, bool bDynamic)
{
	dReal d_radius = dReal(radius);

	m_worldID = worldID;
	m_spCommon = spCommon;
	m_bodyID = dBodyCreate(m_worldID);

	if (bDynamic) {
		setMass(mass);
		setPosition(position);

		// Assume sphere because radius was provided instead of inertia tensor
		dMass dm;
		dBodyGetMass(m_bodyID, &dm);
		dReal volume = (4 * M_PI / 3) * pow(d_radius, 3);
		dReal density = dm.mass / volume;
		dMassSetSphere(&dm, density, d_radius);
		dBodySetMass(m_bodyID, &dm);
	}
	else {
		dBodySetKinematic(m_bodyID);
	}
}

void RigidBodyOde::setMass(const Real mass)
{
	dMass dm;
	dBodyGetMass(m_bodyID, &dm);
	dm.adjust(dReal(mass));
	dBodySetMass(m_bodyID, &dm);
}

Real RigidBodyOde::getMass(void) const
{
	dMass dm;
	dBodyGetMass(m_bodyID, &dm);
	return dm.mass;
}

void RigidBodyOde::setPosition(const SpatialVector& pos)
{
	dBodySetPosition(m_bodyID, dReal(pos[0]), dReal(pos[1]), dReal(pos[2]));
}

void RigidBodyOde::setRotation(const SpatialMatrix& rotation)
{
	/// Apply internal m_eigenRotation BEFORE applying user-given rotation, so it's a right-hand-side argument;
	SpatialMatrix internalRotation = rotation * m_eigenRotation;

	dMatrix3 r;
	r[0] = dReal(internalRotation(0, 0));
	r[1] = dReal(internalRotation(0, 1));
	r[2] = dReal(internalRotation(0, 2));
	r[4] = dReal(internalRotation(1, 0));
	r[5] = dReal(internalRotation(1, 1));
	r[6] = dReal(internalRotation(1, 2));
	r[8] = dReal(internalRotation(2, 0));
	r[9] = dReal(internalRotation(2, 1));
	r[10] = dReal(internalRotation(2, 2));
	dBodySetRotation(m_bodyID, r);
}

void RigidBodyOde::setTransform(const SpatialTransform &transform)
{
	SpatialMatrix rotation;
	rotation(0,0) = transform(0,0);
	rotation(0,1) = transform(0,1);
	rotation(0,2) = transform(0,2);
	rotation(1,0) = transform(1,0);
	rotation(1,1) = transform(1,1);
	rotation(1,2) = transform(1,2);
	rotation(2,0) = transform(2,0);
	rotation(2,1) = transform(2,1);
	rotation(2,2) = transform(2,2);

	setRotation(rotation);
	setPosition(transform.column(3));
}

SpatialVector RigidBodyOde::getPosition(void) const
{
	const dReal* p = dBodyGetPosition(m_bodyID);
	SpatialVector pos;
	pos[0] = p[0];
	pos[1] = p[1];
	pos[2] = p[2];
	return pos;
}

SpatialMatrix RigidBodyOde::getRotation(void) const
{
	SpatialMatrix rotation;
	const dReal* r = dBodyGetRotation(m_bodyID);
	rotation(0, 0) = r[0];
	rotation(0, 1) = r[1];
	rotation(0, 2) = r[2];
	rotation(1, 0) = r[4];
	rotation(1, 1) = r[5];
	rotation(1, 2) = r[6];
	rotation(2, 0) = r[8];
	rotation(2, 1) = r[9];
	rotation(2, 2) = r[10];

	/// "unwind" by applying m_eigenRotationInverse rotation prior to retrieving user's rotation
	/// !!! the m_eigenRotationInverse must be applied in coordinates LOCAL to the 'internal' dBody!
	SpatialMatrix externalRot = rotation * m_eigenRotationInverse;
	return externalRot;
}

SpatialTransform RigidBodyOde::getTransform(void) const
{
	SpatialTransform transform;
	fe::setRotation(transform, getRotation());
	fe::setTranslation(transform, getPosition());
	return transform;
}

void RigidBodyOde::applyForce(
	const SpatialVector& force, const SpatialVector& pos)
{
	dBodyAddForceAtPos(	m_bodyID, dReal(force[0]),
						dReal(force[1]), dReal(force[2]),
						dReal(pos[0]), dReal(pos[1]),
						dReal(pos[2]));
}

void RigidBodyOde::applyTorque(const SpatialVector& torque)
{
	dBodyAddTorque(	m_bodyID, dReal(torque[0]),
					dReal(torque[1]), dReal(torque[2]));
}

void RigidBodyOde::setVelocity(const SpatialVector &vel)
{
	dBodySetLinearVel(	m_bodyID, dReal(vel[0]),
						dReal(vel[1]), dReal(vel[2]));
}

SpatialVector RigidBodyOde::getVelocity(const SpatialVector& pos) const
{
	dVector3 result;
	dBodyGetPointVel(m_bodyID, pos[0], pos[1], pos[2], result);
	SpatialVector velocity;
	velocity[0] = result[0];
	velocity[1] = result[1];
	velocity[2] = result[2];
	return velocity;
}

void RigidBodyOde::setAngularVelocity(const SpatialVector &angVel)
{
	dBodySetAngularVel(	m_bodyID, dReal(angVel[0]),
						dReal(angVel[1]), dReal(angVel[2]));
}

SpatialVector RigidBodyOde::getAngularVelocity(void) const
{
	const dReal* result = dBodyGetAngularVel(m_bodyID);
	SpatialVector velocity;
	velocity[0] = result[0];
	velocity[1] = result[1];
	velocity[2] = result[2];
	return velocity;
}

void RigidBodyOde::translate(const SpatialVector& displ)
{
	dMass dm;
	dBodyGetMass(m_bodyID, &dm);
	dMassTranslate(&dm, dReal(displ[0]), dReal(displ[1]), dReal(displ[2]));
	dBodySetMass(m_bodyID, &dm);
}

void RigidBodyOde::setLinearDamping(const Real damping)
{
	dBodySetLinearDamping(m_bodyID, dReal(damping));
}

void RigidBodyOde::setAngularDamping(const Real damping)
{
	dBodySetAngularDamping(m_bodyID, dReal(damping));
}

} /* namespace hive */
