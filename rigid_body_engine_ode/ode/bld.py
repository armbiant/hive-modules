import sys
import os
forge = sys.modules["forge"]

def setup(module):
	opcode = module.Module('OPCODE')
	module.AddDep(opcode)

	ou = module.Module('ou')
	module.AddDep(ou)

	libccd = module.Module('libccd')
	module.AddDep(libccd)

	ode = module.Module('ode')
	module.AddDep(ode)
