import sys
import os
forge = sys.modules["forge"]

def setup(module):	
	joints = module.Module('joints')
	module.AddDep(joints)

	srcList = [	"array.cpp",
				"box.cpp",
				"capsule.cpp",
				"collision_convex_trimesh.cpp",		
				"collision_cylinder_box.cpp",
				"collision_cylinder_plane.cpp",
				"collision_cylinder_sphere.cpp",
				"collision_cylinder_trimesh.cpp",
				"collision_kernel.cpp",
				"collision_libccd.cpp",
				"collision_quadtreespace.cpp",
				"collision_sapspace.cpp",
				"collision_space.cpp",
				"collision_transform.cpp",
				"collision_trimesh_box.cpp",
				"collision_trimesh_ccylinder.cpp",
				"collision_trimesh_disabled.cpp",
				"collision_trimesh_gimpact.cpp",
				"collision_trimesh_internal.cpp",
				"collision_trimesh_opcode.cpp",
				"collision_trimesh_plane.cpp",
				"collision_trimesh_ray.cpp",
				"collision_trimesh_sphere.cpp",
				"collision_trimesh_trimesh.cpp",
				"collision_trimesh_trimesh_old.cpp",
				"collision_util.cpp",
				"convex.cpp",
				"cylinder.cpp",
				"default_threading.cpp",
				"error.cpp",
				"export-dif.cpp",
				"fastdot.cpp",
				"fastldltfactor.cpp",
				"fastldltsolve.cpp",
				"fastlsolve.cpp",
				"fastltsolve.cpp",
				"fastvecscale.cpp",
				"gimpact_contact_export_helper.cpp",
				"heightfield.cpp",
				"lcp.cpp",
				"mass.cpp",
				"mat.cpp",
				"matrix.cpp",
				"memory.cpp",
				"misc.cpp",
				"objects.cpp",
				"obstack.cpp",
				"ode.cpp",
				"odeinit.cpp",
				"odemath.cpp",
				"odeou.cpp",
				"odetls.cpp",
				"plane.cpp",
				"quickstep.cpp",
				"ray.cpp",
				"resource_control.cpp",
				"rotation.cpp",
				"simple_cooperative.cpp",
				"sphere.cpp",
				"step.cpp",
				"threading_base.cpp",
				"threading_impl.cpp",
				"threading_pool_posix.cpp",
				"threading_pool_win.cpp",
				"timer.cpp",
				"util.cpp"
				]

	deplibs = forge.basiclibs + ["odeJointsLib",
								 "odeOuLib",
								 "odeCcdLib",
								 "odeIceLib",
								 "odeOpCodeLib"]

	lib = module.Lib( "odeMain", srcList )

	module.includemap = { 'ode' : os.path.join(module.modPath,'..','..','include'),
						'OPCODE' : os.path.join(module.modPath,'..','..'),
						'Ice' : os.path.join(module.modPath,'..','..','OPCODE'),
						'ou' : os.path.join(module.modPath,'..','..','ou','include'),
						'ccd' : os.path.join(module.modPath,'..','..','libccd','src'),
						'ccdcustom' : os.path.join(module.modPath,'..','..','libccd','src','custom')}

	module.cppmap = { 'visibility' : "" }
