/*	Copyright (C) 2018 Arrival Ltd
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit git.hiverepo.com */

/** @file */

#include "rigid_body_engine_ode/rigid_body_engine_ode.pmh"
#include "platform/dlCore.cc"

using namespace fe;

extern "C"
{

    FE_DL_EXPORT void ListDependencies(List<String *> &list)
    {
    }

    FE_DL_EXPORT Library *CreateLibrary(sp<Master> spMaster)
    {
        Library *pLibrary = new Library();

		pLibrary->addSingleton<hive::EngineCommonOde>("EngineCommonI.EngineCommonOde.hive.ode");
		pLibrary->add<hive::ConnectedSystemOde>("ConnectedSystemI.ConnectedSystemOde.hive.ode");
		pLibrary->add<hive::JointOde>("JointI.JointOde.hive.ode");
		pLibrary->add<hive::RigidBodyOde>("RigidBodyI.RigidBodyOde.hive.ode");

        return pLibrary;
    }

    //-----------------------------------------------------------------
    // InitializeLibrary()
    //-----------------------------------------------------------------

    FE_DL_EXPORT void InitializeLibrary(sp<Library> spLibrary)
    {
    }
}
