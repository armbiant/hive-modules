/*	Copyright (C) 2018 Arrival Ltd
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit git.hiverepo.com */

/** @file */

#ifndef __surface_hive_SurfaceAccessibleJsonFull_h__
#define __surface_hive_SurfaceAccessibleJsonFull_h__

		//* json_full seems to lack altitude,
		//* so presuming otherwise can make things much worse
#define FE_JSON_FULL_USE_ALTITUDE	FALSE

namespace hive
{

/**************************************************************************//**
    @brief Track Data Surface Binding

	@ingroup surface_hive
*//***************************************************************************/
class FE_DL_EXPORT SurfaceAccessibleJsonFull:
	public fe::ext::SurfaceAccessibleBase,
	public fe::ClassSafe<SurfaceAccessibleJsonFull>
{
	public:
							SurfaceAccessibleJsonFull(void)
							{	set(m_originOffset); }
virtual						~SurfaceAccessibleJsonFull(void)				{}

							//* As Protectable
virtual	fe::Protectable*	clone(fe::Protectable* pInstance=NULL);

							//* as SurfaceAccessibleI

							using fe::ext::SurfaceAccessibleBase::bind;

virtual	void				bind(fe::Instance a_instance)
							{
								m_spJsonRoot=a_instance
										.cast< fe::sp<fe::Component> >();
							}
virtual	BWORD				isBound(void)
							{	return (m_spJsonRoot.isValid()); }

							using fe::ext::SurfaceAccessibleBase::accessor;

virtual fe::sp<fe::ext::SurfaceAccessorI>	accessor(fe::String a_node,
									Element a_element,fe::String a_name,
									Creation a_create);

virtual fe::sp<fe::ext::SurfaceAccessorI>	accessor(fe::String a_node,
									Element a_element,Attribute a_attribute,
									Creation a_create);

							using fe::ext::SurfaceAccessibleBase::load;

virtual	BWORD				load(fe::String a_filename,
									fe::sp<fe::Catalog> a_spSettings);

							using fe::ext::SurfaceAccessibleBase::save;

virtual	BWORD				save(fe::String a_filename,
											fe::sp<fe::Catalog> a_spSettings);

							using
							fe::ext::SurfaceAccessibleBase::attributeSpecs;

virtual	void				attributeSpecs(
									fe::Array<SurfaceAccessibleI::Spec>&
									a_rSpecs,
									fe::String a_node,
									SurfaceAccessibleI::Element
									a_element) const;

							//* JSON specific
		void				setJsonRoot(fe::sp<fe::ext::JsonValue> a_spJsonRoot)
							{	m_spJsonRoot=a_spJsonRoot; }

		fe::sp<fe::ext::JsonValue>	jsonRoot(void)		{ return m_spJsonRoot; }

	private:

virtual	void				reset(void);

		fe::sp<fe::ext::JsonValue>	m_spJsonRoot;
		fe::SpatialVector			m_originOffset;	//* for different LLA
};

} /* namespace hive */

#endif /* __surface_hive_SurfaceAccessibleJsonFull_h__ */
