/*	Copyright (C) 2018 Arrival Ltd
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit git.hiverepo.com */

/** @file */

#include <surface_hive/surface_hive.pmh>

#define FE_SAT_DEBUG	TRUE

using namespace fe;
using namespace fe::ext;

namespace hive
{

SurfaceAccessibleTrack::SurfaceAccessibleTrack(void)
{
#if FE_SAT_DEBUG
	feLog("SurfaceAccessibleTrack::SurfaceAccessibleTrack\n");
#endif
}

SurfaceAccessibleTrack::~SurfaceAccessibleTrack(void)
{
#if FE_SAT_DEBUG
	feLog("SurfaceAccessibleTrack::~SurfaceAccessibleTrack\n");
#endif
}

BWORD SurfaceAccessibleTrack::load(String a_filename,sp<Catalog> a_spSettings)
{
#if FE_SAT_DEBUG
	feLog("SurfaceAccessibleTrack::load \"%s\"\n",a_filename.c_str());
#endif

	m_spSurfaceI=registry()->create("*.SurfaceTrack");

#if FE_SAT_DEBUG
	feLog("SurfaceAccessibleTrack::load valid %d\n",m_spSurfaceI.isValid());
#endif

	return TRUE;
}

sp<SurfaceI> SurfaceAccessibleTrack::surface(String a_group)
{
#if FE_SAT_DEBUG
	feLog("SurfaceAccessibleTrack::surface group \"%s\"\n",a_group.c_str());
#endif

	return m_spSurfaceI;
}

} /* namespace hive */
