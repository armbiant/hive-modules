/*	Copyright (C) 2018 Arrival Ltd
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit git.hiverepo.com */

/** @file */

#ifndef __surface_hive_SurfaceAccessibleTrack_h__
#define __surface_hive_SurfaceAccessibleTrack_h__

namespace hive
{

/**************************************************************************//**
    @brief Hive Track Surface Loader

	@ingroup surface_hive
*//***************************************************************************/
class FE_DL_EXPORT SurfaceAccessibleTrack:
	public fe::ext::SurfaceAccessibleBase
{
	public:
					SurfaceAccessibleTrack(void);
virtual				~SurfaceAccessibleTrack(void);

					//* as SurfaceAccessibleI
virtual	void		reset(void)										{}

virtual	BWORD		isBound(void)
					{	return TRUE; }

					using fe::ext::SurfaceAccessibleBase::load;

virtual	BWORD		load(fe::String a_filename,
							fe::sp<fe::Catalog> a_spSettings);

					using fe::ext::SurfaceAccessibleBase::attributeSpecs;

virtual	void		attributeSpecs(
							fe::Array<fe::ext::SurfaceAccessibleI::Spec>&
							a_rSpecs,
							fe::String a_node,
							fe::ext::SurfaceAccessibleI::Element
							a_element) const
					{	a_rSpecs.clear(); }

					using fe::ext::SurfaceAccessibleBase::surface;

virtual	fe::sp<fe::ext::SurfaceI>	surface(fe::String a_group);

	private:
		fe::sp<fe::ext::SurfaceI>			m_spSurfaceI;
};

} /* namespace hive */

#endif /* __surface_hive_SurfaceAccessibleTrack_h__ */
