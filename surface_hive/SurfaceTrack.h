/*	Copyright (C) 2018 Arrival Ltd
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit git.hiverepo.com */

/** @file */

#ifndef __surface_hive_SurfaceTrack_h__
#define __surface_hive_SurfaceTrack_h__

namespace hive
{

/**************************************************************************//**
	@brief HiveHost Track

	@ingroup surface_hive
*//***************************************************************************/
class FE_DL_EXPORT SurfaceTrack:
	public fe::ext::SurfaceSphere,
	public fe::Initialize<SurfaceTrack>
{
	public:
							SurfaceTrack(void);
virtual						~SurfaceTrack(void);

		void				initialize(void);

							//* As Protectable
virtual	fe::Protectable*	clone(fe::Protectable* pInstance=NULL);

							//* As SurfaceI

virtual	void				prepareForSearch(void);

							using fe::ext::SurfaceSphere::nearestPoint;

virtual	fe::sp<fe::ext::SurfaceI::ImpactI>
							nearestPoint(
								const fe::SpatialVector& a_origin,
								fe::Real a_maxDistance) const;

							using SurfaceBase::rayImpact;

virtual	fe::sp<fe::ext::SurfaceI::ImpactI>
							rayImpact(const fe::SpatialVector& a_origin,
									const fe::SpatialVector& a_direction,
									fe::Real a_maxDistance,
									BWORD a_anyHit) const;

							using fe::ext::SurfaceSphere::draw;

virtual	void				draw(const fe::SpatialTransform&,
								fe::sp<fe::ext::DrawI> a_spDrawI,
								const fe::Color* a_color,
								fe::sp<fe::ext::DrawBufferI> a_spDrawBuffer,
								fe::sp<fe::ext::PartitionI> a_spPartition) const;

	protected:

virtual	void				cache(void);

		void				updateState(void);

	private:

		fe::sp<HiveHost>							m_spHiveHost;
		fe::sp<fe::StateCatalog>					m_spStateCatalog;
		fe::sp<fe::ext::ImageI>						m_spImageI;
		fe::sp<fe::ext::DrawMode>					m_spDrawTexture;

		fe::sp<fe::ext::SurfaceAccessibleI>			m_spTrackAccessible;
		fe::Array< fe::sp<fe::ext::SurfaceI> >		m_surfaceArray;
		fe::Array<fe::Color>						m_colorArray;
		fe::Array<fe::String>						m_nameArray;
		fe::Array< fe::sp<fe::ext::DrawBufferI> >	m_bufferArray;

		fe::sp<fe::ext::SurfaceI>					m_spSurfaceFallback;
};

} /* namespace hive */

#endif /* __surface_hive_SurfaceTrack_h__ */
