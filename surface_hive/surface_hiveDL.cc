/*	Copyright (C) 2018 Arrival Ltd
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit git.hiverepo.com */

/** @file */

#include <surface_hive/surface_hive.pmh>

#include "platform/dlCore.cc"

using namespace fe;
using namespace hive;

extern "C"
{

FE_DL_EXPORT void ListDependencies(List<String*>& list)
{
	list.append(new String("fexSurfaceDL"));
}

FE_DL_EXPORT Library* CreateLibrary(sp<Master> spMaster)
{
	Library *pLibrary = new Library();

	pLibrary->add<SurfaceAccessibleJsonFull>(
			"SurfaceAccessibleI.SurfaceAccessibleJsonFull.hive.json_full");

	pLibrary->add<SurfaceAccessiblePuppet>(
			"SurfaceAccessibleI.SurfaceAccessiblePuppet.hive.pup");

	pLibrary->add<SurfaceAccessibleTrack>(
			"SurfaceAccessibleI.SurfaceAccessibleTrack.hive.track");

	pLibrary->add<SurfacePuppet>("SurfaceI.SurfacePuppet.hive");

	pLibrary->add<SurfaceTrack>("SurfaceI.SurfaceTrack.hive");

	return pLibrary;
}

FE_DL_EXPORT void InitializeLibrary(sp<Library> spLibrary)
{
}

}
