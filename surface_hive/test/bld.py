import sys
forge = sys.modules["forge"]

import os.path

def setup(module):
	deplibs = forge.corelibs[:]

	deplibs += [	"fexNetworkHostDLLib",
					"hiveApiaryDLLib" ]

	if forge.fe_os == 'FE_WIN32' or forge.fe_os == 'FE_WIN64':
		deplibs += ["fexSignalLib",
					"fexDataToolLib",
					"fexDrawDLLib",
					"fexThreadDLLib",
					"fexNetworkDLLib"]

	tests = [	"xJoyServer",
				"xNullClient",
				"xTextClient" ]

	for t in tests:
		exe = module.Exe(t)
		if forge.fe_os == "FE_LINUX":
			exe.linkmap = { "stdthread": "-lpthread" }
		forge.deps([t + "Exe"], deplibs)

	# NOTE can take too long during test run
	if forge.codegen == 'optimize':
		forge.tests += [
			("xJoyServer.exe",	"scene/event/vegasNorthRoad.joytest/scene.yaml",	"",
			("xTextClient.exe",	"silent",					"",	None)) ]
