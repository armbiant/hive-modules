/*	Copyright (C) 2018-2021 Arrival Ltd
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit git.hiverepo.com */

/** @file */

#include "math/math.h"
#include "draw/draw.h"

#include "apiary/apiary.h"

using namespace fe;
using namespace fe::ext;
using namespace hive;

int main(int argc,char** argv)
{
	UNIT_START();

	BWORD complete=FALSE;

	try
	{
		{
			sp<HiveHost> spHiveHost(HiveHost::create());
			FEASSERT(spHiveHost.isValid());

			sp<Master> spMaster=spHiveHost->master();
			sp<Registry> spRegistry=spMaster->registry();

			Result result=spRegistry->manage("feAutoLoadDL");
			UNIT_TEST(successful(result));

			sp<CatalogReaderI> spCatalogReader=
					spRegistry->create("CatalogReaderI.*.*.yaml");
			UNIT_TEST(spCatalogReader.isValid());
			if(spCatalogReader.isNull())
			{
				feLog("spCatalogReader invalid");
			}

			sp<Catalog> spPreCatalog=spMaster->createCatalog("yaml");
			if(spCatalogReader.isValid())
			{
				for(I32 arg=1;arg<argc;arg++)
				{
					spCatalogReader->load(argv[arg],spPreCatalog);
				}
			}

			String space("world");

			const String implementation=
					spPreCatalog->catalogOrDefault<String>(
					"net:implementation","ConnectedCatalog");

			sp<StateCatalog> spStateCatalog=
					spHiveHost->accessSpace(space,implementation);
			FEASSERT(spStateCatalog.isValid());

			spStateCatalog->overlayState(spPreCatalog);

			result=spStateCatalog->start();
			UNIT_TEST(successful(result));

			feLog("StateCatalog started\n");

			if(successful(result))
			{
//				spStateCatalog->catalogDump();

				spStateCatalog->waitForConnection();

//				spStateCatalog->catalogDump();

				spStateCatalog->stop();
			}

			spHiveHost=NULL;
		}

		complete=TRUE;
	}
	catch(Exception &e) { e.log(); }
	catch(...)			{ feLog("uncaught exception\n"); }

	UNIT_TEST(complete);
	UNIT_TEST(!Counted::trackerCount());
	UNIT_TRACK(5);
	UNIT_RETURN();
}
