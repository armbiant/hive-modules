/*	Copyright (C) 2018 Arrival Ltd
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit git.hiverepo.com */

/** @file */

#include "math/math.h"
#include "draw/draw.h"
#include "apiary/apiary.h"

#define FE_XTC_BEACON			TRUE

#if FE_XTC_BEACON
#include "guide/guide.h"
#endif

using namespace fe;
using namespace fe::ext;
using namespace hive;

int main(int argc,char** argv)
{
	UNIT_START();

	BWORD complete=FALSE;

	try
	{
		{
			milliSleep(1000);

			sp<HiveHost> spHiveHost(HiveHost::create());
			FEASSERT(spHiveHost.isValid());

			sp<Master> spMaster=spHiveHost->master();
			sp<Registry> spRegistry=spMaster->registry();

			Result result=spRegistry->manage("feAutoLoadDL");
			UNIT_TEST(successful(result));

			const BWORD silent=(argc>1 && argv[1]==String("silent"));

			sp<CatalogReaderI> spCatalogReader=
					spRegistry->create("CatalogReaderI.*.*.yaml");
			UNIT_TEST(spCatalogReader.isValid());
			if(spCatalogReader.isNull())
			{
				feLog("spCatalogReader invalid");
			}

			sp<Catalog> spPreCatalog=spMaster->createCatalog("yaml");
			if(spCatalogReader.isValid())
			{
				for(I32 arg=1+silent;arg<argc;arg++)
				{
					spCatalogReader->load(argv[arg],spPreCatalog);
				}
			}

			sp<DrawI> spDrawConsole;
			if(!silent)
			{
				spDrawConsole=spRegistry->create("*.DrawConsole");
			}
			UNIT_TEST(silent || spDrawConsole.isValid());

			const String space("world");
			sp<StateCatalog> spClientState;

			//* NOTE loaded yaml only use for implementation

#if FE_XTC_BEACON
			sp<GuidePostI> spGuidePostI(spRegistry->create("GuidePostI"));

			if(!spGuidePostI->connect("localhost"))
			{
				feLog("failed to connect to beacon\n");
				spGuidePostI=NULL;
			}

			sp<StateCatalog> spStateCatalog=spPreCatalog;

			if(spGuidePostI.isValid())
			{
				//* NOTE replace
				spStateCatalog=spGuidePostI->connectToSpace(space);
				feLog("accessed space %p\n",spStateCatalog.raw());

				//* start a client space
				String clientSpace;
				clientSpace.sPrintf("client%d",spGuidePostI->getID());
				spClientState=spGuidePostI->createSpace(
						clientSpace,sp<Catalog>(NULL));
			}

			if(spStateCatalog.isNull())
			{
				feX(e_invalidPointer,argv[0],"StateCatalog is invalid");
			}
#else

			const String implementation=
					spPreCatalog->catalogOrDefault<String>(
					"net:implementation","ConnectedCatalog");

			sp<StateCatalog> spStateCatalog=
					spHiveHost->accessSpace(space,implementation);

			if(spStateCatalog.isNull())
			{
				feX(e_invalidPointer,argv[0],"StateCatalog is invalid");
			}

			spStateCatalog->catalogDump();

			spStateCatalog->overlayState(spPreCatalog);

			result=spStateCatalog->start();
			UNIT_TEST(successful(result));
#endif

			feLog("StateCatalog started\n");

			if(successful(result))
			{
				spStateCatalog->catalogDump();

				spStateCatalog->waitForConnection();

				spStateCatalog->catalogDump();

				spStateCatalog->setState<Real>("nanoDelay",0);
				spStateCatalog->setState<Real>("nanoStep",100e6);
				spStateCatalog->flush();

				String netID;
				spStateCatalog->getState<String>("net:id",netID);
				feLog("net:id \"%s\"\n",netID.c_str());

				Array<SpatialTransform> xformArray;

				const I32 maxPolls=2000;
				I32 polls=0;
				I32 serial=0;
				I32 frame= -1;
				bool running=true;
				while(running)
				{
					spStateCatalog->flush();

					const I32 lastSerial=serial;
					sp<StateCatalog::Snapshot> spSnapshot;
					while(serial==lastSerial)
					{
						result=spStateCatalog->snapshot(spSnapshot);
						serial=spSnapshot->serial();
					}

					spSnapshot->getState<bool>("running",running);
					spSnapshot->getState<I32>("frame",frame);

					I32 vehicleCount=0;
					while(TRUE)
					{
						String key;
						key.sPrintf("vehicles.%d.chassis.transform",
								vehicleCount);

						SpatialTransform xform;
						result=spSnapshot->getState<SpatialTransform>(
								key,xform);
						if(failure(result))
						{
							break;
						}

						if(vehicleCount>=I32(xformArray.size()))
						{
							xformArray.resize(vehicleCount+1);
						}

						xformArray[vehicleCount++]=xform;
					}

#if TRUE
					feLog("\nrunning %d frame %d\n",running,frame);
//					if(vehicleCount)
//					{
//						feLog("transform\n%s\n",c_print(xformArray[0]));
//					}
#endif

					if(spDrawConsole.isValid())
					{
						const Color white(1,1,1,1);

						for(I32 m=0;m<vehicleCount;m++)
						{
							String text;
							text.sPrintf("%d",m);

							spDrawConsole->drawAlignedText(
									xformArray[m].translation(),text,white);
						}

						spDrawConsole->flush();
					}

#if FE_XTC_BEACON
					Array<String> spaceList;
					spGuidePostI->listSpaces(spaceList);
					const I32 spaceCount=spaceList.size();
					for(I32 spaceIndex=0;spaceIndex<spaceCount;spaceIndex++)
					{
						feLog("Space %d/%d \"%s\"\n",spaceIndex,spaceCount,
								spaceList[spaceIndex].c_str());

					}
#endif

					if(++polls > maxPolls)
					{
						feLog("reached max polls %d\n",maxPolls);
						break;
					}
				}

				feLog("final %d polls %d\n",frame,polls);

				UNIT_TEST(!running);

				spStateCatalog->stop();
			}

			if(spClientState.isValid())
			{
				feLog("stop client state\n");
				spClientState->stop();
			}

#if FE_XTC_BEACON
			if(spGuidePostI.isValid())
			{
				feLog("shutdown guide post\n");
				spGuidePostI->shutdown();
			}
#endif

			feLog("HiveHost -> NULL\n");
			spHiveHost=NULL;
			feLog("HiveHost is NULL\n");
		}

		complete=TRUE;
	}
	catch(Exception &e) { e.log(); }
	catch(...)			{ feLog("uncaught exception\n"); }

	UNIT_TEST(complete);
	UNIT_TEST(!Counted::trackerCount());
	UNIT_TRACK(7);
	UNIT_RETURN();
}
