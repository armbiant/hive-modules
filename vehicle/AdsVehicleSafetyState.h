/*	Copyright (C) 2018 Arrival Ltd
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit git.hiverepo.com */

/** @file */

#ifndef __vehicle_AdsSafetyState_h__
#define __vehicle_AdsSafetyState_h__

namespace hive
{

struct AdsVehicleSafetyState
{
	/***
	| DriveMode | ControlMode | SafeStop  | Result  |
	|  ignored  | prohibited  | No Action | No action |
	|  ignored  | prohibited  |  Stop     | Brakes applied, HV system broken |

	|   stop    |   allowed   | No Action | Blue AI allowed light flashing
											on vehicle, vehicle ready to launch
											on DriveMode command |
	|    go     |   allowed   | No Action | Vehicle driving |

	|   stop    |   allowed   |  Stop     | Brakes applied, HV system broken |
	|    go     |   allowed   |  Stop     | Vehicle decelerates at ~1.0g with
											brakes applied, HV system broken |

	if ControlMode=/=1, Drive Mode==1 is ignored
	if Safestop==1, it forces Drive Mode and Control Mode to 0
	The pit wall controls the values in all situations,
	the majority of the time.
	The human driver has access to Control and Drive mode
	in some scenarios, but never Safestop.
	*/

	enum	DriveMode
			{
				e_stop=0,			//* ADS wait to drive
				e_go				//* ADS drive as you please
			};

	enum	ControlMode
			{
				e_prohibited=0,		//* Human only
				e_allowed			//* ADS allowed
			};

	enum	SafeStop
			{
				e_noAction = 0,		//* no effect
				e_safeStop			//* forced braking
			};

			//* NOTE RR SIL values: 0=neutral 1=drive 2=reverse 3=park
			//* TODO maybe negative for manual gears
	enum	Gear
			{
				e_neutral=0,
				e_drive,
				e_reverse,
				e_park
			};

	DriveMode		m_driveMode;
	ControlMode		m_controlMode;
	SafeStop		m_safeStop;
	Gear			m_gear;

					//* rolling, increments in each message
					//* if safety layer is fully operational
	uint8_t			m_watchdog;
};

}
#endif
