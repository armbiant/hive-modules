/*	Copyright (C) 2018 Arrival Ltd
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit git.hiverepo.com */

/** @file */

#ifndef __vehicle_AsAdsTelemetry2_h__
#define __vehicle_AsAdsTelemetry2_h__

namespace hive
{

class FE_DL_EXPORT AsAdsTelemetry2:
	public fe::AccessorSet,
	public fe::Initialize<AsAdsTelemetry2>
{
	public:
		void initialize(void)
		{
			add(positionNorthDesired,
					FE_USE("ads:telemetry.position.north.desired"));
			add(positionEastDesired,
					FE_USE("ads:telemetry.position.east.desired"));
			add(headingDesired,
					FE_USE("ads:telemetry.heading.desired"));
		}

		fe::Accessor<F32>	positionNorthDesired;
		fe::Accessor<F32>	positionEastDesired;
		fe::Accessor<F32>	headingDesired;
};

}
#endif
