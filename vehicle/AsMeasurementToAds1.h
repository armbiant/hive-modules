/*	Copyright (C) 2018 Arrival Ltd
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit git.hiverepo.com */

/** @file */

#ifndef __vehicle_AsMeasurementToAds1_h__
#define __vehicle_AsMeasurementToAds1_h__

namespace hive
{

//-----------------------------------------------
// NOTE: All units in radians, meters, seconds, bar, Nw-m
//       Status = Good = 1;
//-----------------------------------------------

class FE_DL_EXPORT AsMeasurementToAds1:
	public fe::AccessorSet,
	public fe::Initialize<AsMeasurementToAds1>
{
	public:
		void initialize(void)
		{
			add(loggingTriggers,
					FE_USE("ads:measurement.loggingTriggers"));
			add(longitudinalAccelerationStatus,
					FE_USE("ads:measurement.acceleration.longitudinal.status"));
			add(yawStatus,
					FE_USE("ads:measurement.yaw.status"));
			add(brakesRearStatus,
					FE_USE("ads:measurement.brakes.rear.status"));
			add(brakesFrontStatus,
					FE_USE("ads:measurement.brakes.front.status"));
			add(longitudinalAcceleration,
					FE_USE("ads:measurement.acceleration.longitudinal"));
			add(yaw,
					FE_USE("ads:measurement.yaw"));
			add(brakeRearPressure,
					FE_USE("ads:measurement.brakes.rear.pressure"));
			add(brakesFrontPressure,
					FE_USE("ads:measurement.brakes.front.pressure"));
		}

		fe::Accessor<U8>	loggingTriggers;
		fe::Accessor<U8>	longitudinalAccelerationStatus;
		fe::Accessor<U8>	yawStatus;
		fe::Accessor<U8>	brakesRearStatus;
		fe::Accessor<U8>	brakesFrontStatus;
		fe::Accessor<F32>	longitudinalAcceleration;
		fe::Accessor<F32>	yaw;
		fe::Accessor<F32>	brakeRearPressure;
		fe::Accessor<F32>	brakesFrontPressure;
};

}
#endif
