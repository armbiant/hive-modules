/*	Copyright (C) 2018 Arrival Ltd
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit git.hiverepo.com */

/** @file */

#ifndef __vehicle_AsMeasurementToAds2_h__
#define __vehicle_AsMeasurementToAds2_h__

namespace hive
{

//-----------------------------------------------
// NOTE: All units in radians, meters, seconds, bar, Nw-m
//       Status = Good = 1;
//-----------------------------------------------

class FE_DL_EXPORT AsMeasurementToAds2:
	public fe::AccessorSet,
	public fe::Initialize<AsMeasurementToAds2>
{
	public:
		void initialize(void)
		{
			add(lateralAccelerationStatus,
				FE_USE("ads:measurement.acceleration.lateral.status"));
			add(WheelRearLeftStatus,
				FE_USE("ads:measurement.wheels.RL.angularVelocity.status"));
			add(WheelRearRightStatus,
				FE_USE("ads:measurement.wheels.RR.angularVelocity.status"));
			add(WheelFrontLeftStatus,
				FE_USE("ads:measurement.wheels.FL.angularVelocity.status"));
			add(WheelFrontRightStatus,
				FE_USE("ads:measurement.wheels.FR.angularVelocity.status"));

			add(lateralAcceleration,
				FE_USE("ads:measurement.acceleration.lateral"));
			add(WheelAngularVelocityRL,
				FE_USE("ads:measurement.wheels.RL.angularVelocity"));
			add(WheelAngularVelocityRR,
				FE_USE("ads:measurement.wheels.RR.angularVelocity"));
			add(WheelAngularVelocityFL,
				FE_USE("ads:measurement.wheels.FL.angularVelocity"));
			add(WheelAngularVelocityFR,
				FE_USE("ads:measurement.wheels.FR.angularVelocity"));
		}

		fe::Accessor<I32>	lateralAccelerationStatus;
		fe::Accessor<I32>	WheelRearLeftStatus;
		fe::Accessor<I32>	WheelRearRightStatus;
		fe::Accessor<I32>	WheelFrontLeftStatus;
		fe::Accessor<I32>	WheelFrontRightStatus;

		fe::Accessor<F32>	lateralAcceleration;
		fe::Accessor<F32>	WheelAngularVelocityRL;
		fe::Accessor<F32>	WheelAngularVelocityRR;
		fe::Accessor<F32>	WheelAngularVelocityFL;
		fe::Accessor<F32>	WheelAngularVelocityFR;
};

}
#endif
