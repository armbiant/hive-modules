/*	Copyright (C) 2018 Arrival Ltd
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit git.hiverepo.com */

/** @file */

#ifndef __vehicle_AsMeasurementToAds3_h__
#define __vehicle_AsMeasurementToAds3_h__

namespace hive
{

//-----------------------------------------------
// NOTE: All units in radians, meters, seconds, bar, Nw-m
//       Status = Good = 1;
//-----------------------------------------------


class FE_DL_EXPORT AsMeasurementToAds3:
	public fe::AccessorSet,
	public fe::Initialize<AsMeasurementToAds3>
{
	public:
		void initialize(void)
		{
			add(slipAngleOssStatus,
					FE_USE("ads:measurement.oss.slipAngle.status"));
			add(lateralVelocityOssStatus,
					FE_USE("ads:measurement.oss.velocity.lateral.status"));
			add(longitudinalVelocityOssStatus,
					FE_USE("ads:measurement.oss.velocity.longitudinal.status"));

			add(slipAngleOss,
					FE_USE("ads:measurement.oss.slipAngle"));
			add(lateralVelocityOss,
					FE_USE("ads:measurement.oss.velocity.lateral"));
			add(longitudinalVelocityOss,
					FE_USE("ads:measurement.oss.velocity.longitudinal"));
		}

		fe::Accessor<U8>	slipAngleOssStatus;
		fe::Accessor<U8>	lateralVelocityOssStatus;
		fe::Accessor<U8>	longitudinalVelocityOssStatus;

		fe::Accessor<F32>	slipAngleOss;
		fe::Accessor<F32>	lateralVelocityOss;
		fe::Accessor<F32>	longitudinalVelocityOss;
};

}
#endif
