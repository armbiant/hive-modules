/*	Copyright (C) 2018 Arrival Ltd
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit git.hiverepo.com */

/** @file */

#ifndef __vehicle_AsMeasurementToAds4_h__
#define __vehicle_AsMeasurementToAds4_h__

namespace hive
{

//-----------------------------------------------
// NOTE: All units in radians, meters, seconds, bar, Nw-m
//       Status = Good = 1;
//-----------------------------------------------

class FE_DL_EXPORT AsMeasurementToAds4:
	public fe::AccessorSet,
	public fe::Initialize<AsMeasurementToAds4>
{
	public:
		void initialize(void)
		{
			add(vehicleVelocityStatus,
				FE_USE("ads:measurement.velocity.status"));
			add(batteryChargeStatus,
				FE_USE("ads:measurement.battery.charge.status"));
			add(motorTorqueRLStatus,
				FE_USE("ads:measurement.motors.RL.status"));
			add(motorTorqueRRStatus,
				FE_USE("ads:measurement.motors.RR.status"));

			add(vehicleVelocity,
				FE_USE("ads:measurement.velocity"));
			add(batteryCharge,
				FE_USE("ads:measurement.battery.charge"));
			add(motorTorqueRL,
				FE_USE("ads:measurement.motors.RL.torque"));
			add(motorTorqueRR,
				FE_USE("ads:measurement.motors.RR.torque"));
		}

		fe::Accessor<U8>	vehicleVelocityStatus;
		fe::Accessor<U8>	batteryChargeStatus;
		fe::Accessor<U8>	motorTorqueRLStatus;
		fe::Accessor<U8>	motorTorqueRRStatus;

		fe::Accessor<U32>	vehicleVelocity;
		fe::Accessor<F32>	batteryCharge;
		fe::Accessor<F32>	motorTorqueRL;
		fe::Accessor<F32>	motorTorqueRR;
};

}
#endif
