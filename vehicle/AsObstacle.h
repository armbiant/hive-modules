/*	Copyright (C) 2018 Arrival Ltd
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit git.hiverepo.com */

/** @file */

#ifndef __vehicle_AsObstacle_h__
#define __vehicle_AsObstacle_h__

namespace hive
{

class FE_DL_EXPORT AsObstacle:
	public fe::AccessorSet,
	public fe::Initialize<AsObstacle>
{
	public:
		void initialize(void)
		{
			add(deltaTime,			FE_USE("sim:timestep"));
		}

		fe::Accessor<fe::Real>		deltaTime;
};

}
#endif
