/*	Copyright (C) 2018 Arrival Ltd
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit git.hiverepo.com */

/** @file */

#ifndef __vehicle_AsSafetyToAds1_h__
#define __vehicle_AsSafetyToAds1_h__

namespace hive
{

class FE_DL_EXPORT AsSafetyToAds1:
	public fe::AccessorSet,
	public fe::Initialize<AsSafetyToAds1>
{
	public:
		void initialize(void)
		{
			add(gear,			FE_USE("ads:safety.gearState"));
			add(driveMode,		FE_USE("ads:safety.driverModeState"));
			add(controlStatus,	FE_USE("ads:safety.controModeState"));
			add(safeStop,		FE_USE("ads:safety.safeStopState"));
			add(watchdog,		FE_USE("ads:safety.localWatchdog"));
		}

		fe::Accessor<U8>	gear;
		fe::Accessor<U8>	driveMode;
		fe::Accessor<U8>	controlStatus;
		fe::Accessor<U8>	safeStop;
		fe::Accessor<U8>	watchdog;
};

}
#endif
