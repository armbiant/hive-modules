/*	Copyright (C) 2018 Arrival Ltd
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit git.hiverepo.com */

/** @file */

#ifndef __vehicle_AsSafetyToAds3_h__
#define __vehicle_AsSafetyToAds3_h__

namespace hive
{

class FE_DL_EXPORT AsSafetyToAds3:
	public fe::AccessorSet,
	public fe::Initialize<AsSafetyToAds3>
{
	public:
		void initialize(void)
		{
			add(instMaxBreakRear,
					FE_USE("ads:safety.brakes.rear.maxPressure"));
			add(instMaxBreakFront,
					FE_USE("ads:safety.brakes.front.maxPressure"));
			add(frontSteer,
					FE_USE("ads:safety.steer.angle"));
		}

		fe::Accessor<F32>	instMaxBreakRear;
		fe::Accessor<F32>	instMaxBreakFront;
		fe::Accessor<F32>	frontSteer;
};
}
#endif
