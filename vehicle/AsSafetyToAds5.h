/*	Copyright (C) 2018 Arrival Ltd
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit git.hiverepo.com */

/** @file */

#ifndef __vehicle_AsSafetyToAds5_h__
#define __vehicle_AsSafetyToAds5_h__

namespace hive
{

class FE_DL_EXPORT AsSafetyToAds5:
	public fe::AccessorSet,
	public fe::Initialize<AsSafetyToAds5>
{
	public:
		void initialize(void)
		{
			add(motorTorquePositiveFL,
					FE_USE("ads:safety.motors.FL.torque.positive"));
			add(motorTorqueNegativeFL,
					FE_USE("ads:safety.motors.FL.torque.negative"));
			add(motorTorquePositiveFR,
					FE_USE("ads:safety.motors.FR.torque.positive"));
			add(motorTorqueNegativeFR,
					FE_USE("ads:safety.motors.FR.torque.negative"));
		}

		fe::Accessor<F32>	motorTorquePositiveFL;
		fe::Accessor<F32>	motorTorqueNegativeFL;
		fe::Accessor<F32>	motorTorquePositiveFR;
		fe::Accessor<F32>	motorTorqueNegativeFR;
};

}
#endif
