/*	Copyright (C) 2018 Arrival Ltd
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit git.hiverepo.com */

/** @file */

#ifndef __vehicle_AsSimTick_h__
#define __vehicle_AsSimTick_h__

// HACK: most likely not a permanent solution.
namespace hive
{

class FE_DL_EXPORT AsSimTick:
	public fe::AccessorSet,
	public fe::Initialize<AsSimTick>
{
	public:
		void initialize(void)
		{
			add(deltaTime,		FE_USE("sim:timestepHelper"));
			add(surfaceTrack,	FE_USE("sim:surface.track"));
		}


		fe::Accessor<fe::Real>					deltaTime;
		fe::Accessor< fe::sp<fe::Component> >	surfaceTrack;
};

} //namespace
#endif
