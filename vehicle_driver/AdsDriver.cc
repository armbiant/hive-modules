#include "vehicle_driver/vehicle_driver.pmh"

using namespace fe;
using namespace fe::ext;

namespace hive
{

AdsDriver::AdsDriver(void):
	m_adsStatus(0),
	m_torqueNmFL(0.0),
	m_torqueNmFR(0.0),
	m_torqueNmRL(0.0),
	m_torqueNmRR(0.0),
	m_brakeBar(0.0),
	m_steeringAngle(0.0)
{
};

AdsDriver::~AdsDriver(void)
{
}

void AdsDriver::handle(Record& a_signal)
{
	/*
	feLog("AdsDriver::handle layout \"%s\"\n",
			a_signal.layout()->name().c_str());
	*/
	sp<Scope> spScope=a_signal.layout()->scope();
	if(m_spScope!=spScope)
	{
		m_asAdsSafety1.bind(spScope);
		m_asAdsSafety2.bind(spScope);
		m_asAdsSafety3.bind(spScope);
		m_asVehicle.bind(spScope);
		m_asAdsSafetyState.bind(spScope);
		m_spScope=spScope;
	}

	if(m_asAdsSafetyState.check(a_signal))
	{
		m_controlMode=AdsVehicleSafetyState::ControlMode(
				m_asAdsSafetyState.controlMode(a_signal));
		m_safetyGear=m_asAdsSafetyState.gear(a_signal);

//		feLog("controlMode %d\n",m_controlMode);
//		feLog("gear %d\n",m_safetyGear);
		return;
	}

	if(m_asAdsSafety1.check(a_signal))
	{
		//* 0=not ready 1=ready 2=active 3=error
		m_adsStatus=m_asAdsSafety1.adsStatus(a_signal);

		m_adsDriveMode=m_asAdsSafety1.adsDriveMode(a_signal);

		//feLog("  adsStatus %d\n",m_adsStatus);
		//feLog("  adsDriveMode %d\n",m_adsDriveMode);
		return;
	}

	if(m_asAdsSafety2.check(a_signal))
	{
		m_curvatureRequest = -m_asAdsSafety2.curvatureRequest(a_signal);
		m_gLongRequest = m_asAdsSafety2.gLongRequest(a_signal);
		m_torqueNmFL = m_asAdsSafety2.motorFLRequestNm(a_signal);
		m_torqueNmFR = m_asAdsSafety2.motorFRRequestNm(a_signal);

		//feLog("  curvatureRequest %.6G\n",m_curvatureRequest);
		//feLog("  gLongRequest %.6G\n",m_gLongRequest);
		//feLog("  motorFLRequestNm %.6G\n",m_torqueNmFL);
		//feLog("  motorFRRequestNm %.6G\n",m_torqueNmFR);
		return;
	}

	if(m_asAdsSafety3.check(a_signal))
	{
		m_torqueNmRL=m_asAdsSafety3.motorRLRequestNm(a_signal);
		m_torqueNmRR=m_asAdsSafety3.motorRRRequestNm(a_signal);

		m_brakeBar=0.5*
				(m_asAdsSafety3.brakeFRequestBar(a_signal)+
				m_asAdsSafety3.brakeRRequestBar(a_signal));

		m_steeringAngle=m_asAdsSafety3.steerFMeanRequestDeg(a_signal);

		/*
		feLog("  motorRLRequestNm %.6G\n",m_torqueNmRL);
		feLog("  motorRRRequestNm %.6G\n",m_torqueNmRR);
		feLog("  brakeBar %.6G\n",m_brakeBar);
		feLog("  steerFMeanRequestDeg %.6G\n",m_steeringAngle);
		*/
		return;
	}

	if(m_asVehicle.check(a_signal))
	{
		switch(m_safetyGear)
		{
			case AdsVehicleSafetyState::e_neutral:
				m_asVehicle.gearModeInput(a_signal)=
						AsVehicle::e_gearModeNeutral;
				break;
			case AdsVehicleSafetyState::e_drive:
				m_asVehicle.gearModeInput(a_signal)=
						AsVehicle::e_gearModeDrive;
				break;
			case AdsVehicleSafetyState::e_reverse:
				m_asVehicle.gearModeInput(a_signal)=
						AsVehicle::e_gearModeReverse;
				break;
			case AdsVehicleSafetyState::e_park:
				m_asVehicle.gearModeInput(a_signal)=
						AsVehicle::e_gearModePark;
				break;
			default:
				m_asVehicle.gearModeInput(a_signal)=
						AsVehicle::e_gearModeUnknown;
				break;
		}

		m_asVehicle.gearIndexInput(a_signal)=1;	//* TODO manual gearing

		if(m_controlMode==AdsVehicleSafetyState::e_prohibited)
		{
			//* ADS prohibited (don't affect controls)
			return;
		}

		//* 0=simple 1=advanced
		if(m_adsDriveMode)
		{
			//* Advanced Mode

			//* NOTE back-compute pseudo-inputs

			const Real max_torque=0.5*(m_asVehicle.motorTorqueMaxRL(a_signal)+
									m_asVehicle.motorTorqueMaxRR(a_signal));

			//* torqueNm: -300 to 300 Nm
//			m_asVehicle.throttleInput(a_signal)=(1.0/300.0)*
//					0.25*(m_torqueNmFL+m_torqueNmFR+m_torqueNmRL+m_torqueNmRR);
			m_asVehicle.throttleInput(a_signal)=(1.0/max_torque)*
					0.5*(m_torqueNmRL+m_torqueNmRR);


			//* brakesBar: 0 to 60 bar
			m_asVehicle.brakeInput(a_signal)=(1.0/60.0)*m_brakeBar;

			//* steeringAngle: -20 to 20 degrees
			m_asVehicle.steeringFInput(a_signal)=(1.0/20.0)*m_steeringAngle;
			/*
			feLog("AdsDriver::handle AsVehicle\n");
			feLog("  throttle %.6G\n",m_asVehicle.throttleInput(a_signal));
			feLog("  brake    %.6G\n",m_asVehicle.brakeInput(a_signal));
			feLog("  steering %.6G\n",m_asVehicle.steeringFInput(a_signal));
			*/
		}
		else
		{
			//* Simple Mode

			//* gLongRequest: -5 to +5
			m_asVehicle.throttleInput(a_signal)=(1.0/1.5)*m_gLongRequest;

			m_asVehicle.brakeInput(a_signal)=0.0;

			//* curvatureRequest: listed -1 to +1, actually +-0.13 (RC)
			Real steering=(1.0/0.13)*m_curvatureRequest;
			steering=fe::maximum(Real(-1),fe::minimum(Real(1),steering));
			m_asVehicle.steeringFInput(a_signal)=steering;

			feLog("CV: %.6G\tST: %.6G\tgLong: %.6G\tThr: %.6G\tGear: %d\n"
				,m_curvatureRequest, steering, m_gLongRequest,
				m_asVehicle.throttleInput(a_signal),
				m_safetyGear);
		}
		return;

	}
}

}
