#ifndef hive_ads_vehicle_driver_h
#define hive_ads_vehicle_driver_h

namespace hive
{

class FE_DL_EXPORT AdsDriver:
	virtual public fe::ext::HandlerI
{
public:
					AdsDriver(void);
	virtual			~AdsDriver(void);

virtual	void		handle(fe::Record& a_signal);

private:

	fe::sp<fe::Scope>	m_spScope;
	AsAdsSafetyState	m_asAdsSafetyState;
	AsAdsSafety1		m_asAdsSafety1;
	AsAdsSafety2		m_asAdsSafety2;
	AsAdsSafety3		m_asAdsSafety3;
	AsVehicle			m_asVehicle;

	AdsVehicleSafetyState::ControlMode	m_controlMode;

	I32					m_adsStatus;
	I32					m_adsDriveMode;
	I32					m_safetyGear;

	fe::Real			m_curvatureRequest;
	fe::Real			m_gLongRequest;
	fe::Real			m_torqueNmFL;
	fe::Real			m_torqueNmFR;
	fe::Real			m_torqueNmRL;
	fe::Real			m_torqueNmRR;
	fe::Real			m_brakeBar;
	fe::Real			m_steeringAngle;
	fe::sp<fe::Catalog>	m_spSettings;
};

}
#endif
