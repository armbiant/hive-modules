#include "vehicle_driver/vehicle_driver.pmh"

using namespace fe;
using namespace fe::ext;

namespace hive
{

AdsStub::AdsStub(void):
	m_time(0.0),
	m_torqueNmFL(0.0),
	m_torqueNmFR(0.0),
	m_torqueNmRL(0.0),
	m_torqueNmRR(0.0),
	m_brakeBar(0.0),
	m_steeringAngle(0.0)
{
};

AdsStub::~AdsStub(void)
{
}

void AdsStub::handle(Record& a_signal)
{
//	feLog("AdsStub::handle layout \"%s\"\n",
//			a_signal.layout()->name().c_str());

	sp<Scope> spScope=a_signal.layout()->scope();
	if(m_spScope!=spScope)
	{
		m_asAdsSafety1.bind(spScope);
		m_asAdsSafety2.bind(spScope);
		m_asAdsSafety3.bind(spScope);
		m_asAdsSafetyState.bind(spScope);
		m_asVehicle.bind(spScope);
		m_spScope=spScope;
	}

	if(m_asAdsSafetyState.check(a_signal))
	{
		m_controlMode=AdsVehicleSafetyState::ControlMode(
				m_asAdsSafetyState.controlMode(a_signal));
		return;
	}

	if(m_asAdsSafety1.check(a_signal))
	{
		return;
	}

	if(m_asAdsSafety2.check(a_signal))
	{
		m_asAdsSafety2.motorFLRequestNm(a_signal)=m_torqueNmFL;
		m_asAdsSafety2.motorFRRequestNm(a_signal)=m_torqueNmFR;
		return;
	}

	if(m_asAdsSafety3.check(a_signal))
	{
		m_asAdsSafety3.motorRLRequestNm(a_signal)=m_torqueNmRL;
		m_asAdsSafety3.motorRRRequestNm(a_signal)=m_torqueNmRR;

		m_asAdsSafety3.brakeFRequestBar(a_signal)=m_brakeBar;
		m_asAdsSafety3.brakeRRequestBar(a_signal)=m_brakeBar;

		m_asAdsSafety3.steerFMeanRequestDeg(a_signal)=m_steeringAngle;
		return;
	}

	if(m_asVehicle.check(a_signal))
	{
		const Real deltaTime=m_asVehicle.deltaTime(a_signal);

		if(m_controlMode==AdsVehicleSafetyState::e_prohibited)
		{
			//* ADS prohibited (don't send requests)
			return;
		}

		//* torqueNm: -300 to 300 Nm
		m_torqueNmFL=300.0*(0.3+0.2*sin(m_time));
		m_torqueNmFR=m_torqueNmFL;
		m_torqueNmRL=m_torqueNmFL;
		m_torqueNmRR=m_torqueNmFL;

		//* brakesBar: 0 to 60 bar
		m_brakeBar=0.0;

		//* steeringAngle: -20 to 20 degrees
		m_steeringAngle=10.0*cos(2.7*m_time);

//		feLog("AdsStub::handle AsVehicle\n");
//		feLog("  throttle %.6G\n",m_asVehicle.throttleInput(a_signal));
//		feLog("  brake    %.6G\n",m_asVehicle.brakeInput(a_signal));
//		feLog("  steering %.6G\n",m_asVehicle.steeringFInput(a_signal));

		m_time+=deltaTime;

		return;
	}
}

}
