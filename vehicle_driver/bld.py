import os
import sys
forge = sys.modules["forge"]

def prerequisites():
	return [ "datatool", "window", "apiary" ]

def setup(module):
	srcList = [	"vehicle_driver.pmh",
				"AdsDriver",
				"AdsPidDriver",
				"AdsStub",
				"HumanDriver",
				"vehicleDriverDL"]

	dll = module.DLL( "hiveVehicleDriverDL", srcList )

	deplibs = forge.corelibs+ [
				"hiveApiaryDLLib",
				"fexSignalLib",
				"fexNetworkDLLib"]

	deplibs +=	[
				"fexDataToolDLLib",
				"fexWindowLib",]

	forge.deps( ["hiveVehicleDriverDLLib"], deplibs )

	forge.tests += [("inspect.exe",	"hiveVehicleDriverDL",	None,	None) ]

#	module.Module('test')
