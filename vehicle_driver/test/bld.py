import sys
forge = sys.modules["forge"]

import os.path

def prerequisites():
	return ["sensor_publish_subscribe"]

def setup(module):
	module.cppmap = { "std": "-std=c++11" }

	deplibs = forge.corelibs[:]

	deplibs += [	"fexSignalLib",
					"fexDataToolDLLib",
					"hiveVehicleDriverDLLib"]

	tests = [	"xVehicleDriver"
			]

	for t in tests:
		exe = module.Exe(t)
		if forge.fe_os == "FE_LINUX":
			exe.linkmap = { "stdthread": "-lpthread" }
		forge.deps([t + "Exe"], deplibs)

