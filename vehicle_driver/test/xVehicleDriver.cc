#include "vehicle_driver/vehicle_driver.pmh"

using namespace fe;
using namespace fe::ext;
using namespace hive;


//-----------------------------------------------------------------
//
//-----------------------------------------------------------------
void handleSteer(float steer)
{
	int state = (I32)(steer*10.0);
	printf("\r[");
	//Left
	for (int i = 0; i < state + 10; ++i)
	{
		printf("-");
	}
	//Mid
	printf("+");
	//Right
	for (int i = state; i < 10; ++i)
	{
		printf("-");
	}
	printf("]");
}

//-----------------------------------------------------------------
//
//-----------------------------------------------------------------
void handleThrottle(float val)
{
	printf("    TT: %1.4f", val);
}

//-----------------------------------------------------------------
//
//-----------------------------------------------------------------
void handleClutch(float val)
{
	printf("    CL: %1.4f", val);
}

//-----------------------------------------------------------------
//
//-----------------------------------------------------------------
void handleBrake(float val)
{
	printf("    BK: %1.4f", val);
}


void inputFunc(bool & running)
{
	char c;
	while(running)
	{
		std::cin >> c;
		if(c == 'q') {running = false;}
	}
}

//-----------------------------------------------------------------
//							MAIN()
//-----------------------------------------------------------------
int main()
{
	sp<Master> spMaster(new Master);
	sp<Registry> spRegistry=spMaster->registry();

	spRegistry->manage("feAutoLoadDL");
	spRegistry->manage("fexWindowDL");

	fe::sp<fe::ext::HandlerI> driver = spRegistry->create("*.HumanDriver");

	bool running = true;
	feLog("Press 'q' to quit\n");
	char c;

	std::thread inputT(inputFunc, std::ref(running));
	while (running)
	{
		fe::milliSleep(50);

		handleSteer(driver->getSteering());
		handleClutch(driver->getClutch());
		handleBrake(driver->getBrake());
		handleThrottle(driver->getThrottle());
	}

	return 0;
}

