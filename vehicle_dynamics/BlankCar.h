/*	Copyright (C) 2018 Arrival Ltd
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit git.hiverepo.com */

/** @file */

#ifndef blankcar_vehicle_dynamics_H
#define blankcar_vehicle_dynamics_H

namespace hive
{

class FE_DL_EXPORT BlankCar:
	virtual public fe::ext::HandlerI,
	public fe::Initialize<BlankCar>
{
public:
				BlankCar(void);
virtual			~BlankCar(void)												{}

		void	initialize(void);

virtual	void	bind(fe::sp<fe::ext::SignalerI> spSignalerI,
					fe::sp<fe::Layout> spLayout)							{}
virtual	void	handle(fe::Record& a_signal);

private:
		void	tick(float_t a_deltaTime);

	float_t						m_throttle;
	float_t						m_brake;
	float_t						m_steering;

	fe::SpatialTransform		m_chassis;
	fe::SpatialTransform		m_wheels[4];

	fe::SpatialVector			m_centerOfMass;
	fe::SpatialVector			m_centerOfMassVelocity;
	fe::SpatialEuler			m_angularVelocity;
	fe::SpatialTransform		m_axlesRearTransform;

	fe::sp<fe::ext::SurfaceI>	m_spSurfaceTrack;

	AsVehicle					m_asVehicle;
};

} // namespace hive

#endif
