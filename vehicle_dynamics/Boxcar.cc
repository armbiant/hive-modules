/*	Copyright (C) 2018 Arrival Ltd
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit git.hiverepo.com */

/** @file */

#include "vehicle_dynamics.pmh"

using namespace fe;
using namespace fe::ext;

namespace hive
{

void printTransform(String variable, const SpatialTransform& transform);

void Boxcar::initialize()
{
	// z position = 1/2 height + clearance (wheel radius)
	SpatialVector position(5, 0, BOXCAR_HEIGHT/2.0 + BOXCAR_WHEEL_RADIUS);
	m_previousPosition = position;

	m_spSurfaceTrack=registry()->create("*.SurfaceTrack");

	// TODO determine why bullet doesn't behave correctly
	m_spConnectedSystem = registry()->create(
			"ConnectedSystemI.ConnectedSystemOde");
	if(m_spConnectedSystem.isValid())
	{
		// To account for drag, friction
		m_spConnectedSystem->setGlobalLinearDamping(0.001);

		SpatialVector gravity(0, 0, -9.8);
		m_spConnectedSystem->setGravity(gravity);
		SpatialMatrix rotation;
		setIdentity(rotation);

		m_theBox = m_spConnectedSystem->createBar(
				BOXCAR_MASS, position, rotation,
				BOXCAR_LENGTH, BOXCAR_WIDTH, BOXCAR_HEIGHT, true);
	}

	// Tires at the corners of the car
	SpatialVector fl( BOXCAR_LENGTH/2.0, BOXCAR_WIDTH/2.0, -BOXCAR_HEIGHT/2.0);
	SpatialVector fr( BOXCAR_LENGTH/2.0,-BOXCAR_WIDTH/2.0, -BOXCAR_HEIGHT/2.0);
	SpatialVector rl(-BOXCAR_LENGTH/2.0, BOXCAR_WIDTH/2.0, -BOXCAR_HEIGHT/2.0);
	SpatialVector rr(-BOXCAR_LENGTH/2.0,-BOXCAR_WIDTH/2.0, -BOXCAR_HEIGHT/2.0);
	m_wheelOffsets[0] = fl;
	m_wheelOffsets[1] = fr;
	m_wheelOffsets[2] = rl;
	m_wheelOffsets[3] = rr;

	m_throttle = 0;
	m_brake = 0;
	m_steering = 0;

	m_cumulativeTimeStep = 0;

	feLog("[Boxcar] constructed\n");
}

void Boxcar::handle(fe::Record& a_signal)
{
	if(!m_asVehicle.bind(a_signal))
	{
		feLog("Boxcar::handle not AsVehicle\n");
		return;
	}

	m_throttle = m_asVehicle.throttleInput(a_signal);
	m_brake = m_asVehicle.brakeInput(a_signal);
	m_steering = m_asVehicle.steeringFInput(a_signal);

	feLog("  throttle %.6G\n",m_throttle);
	feLog("  brake    %.6G\n",m_brake);
	feLog("  steering %.6G\n",m_steering);

	const Real deltaTime = m_asVehicle.deltaTime(a_signal);
	tick(deltaTime);

	SpatialVector vel = m_theBox->getVelocity(m_theBox->getPosition());
	m_asVehicle.centerOfMassVelocity(a_signal) = vel;

	m_asVehicle.speed(a_signal) = magnitude(vel);
	feLog("  speed %.6G\n",m_asVehicle.speed(a_signal));

	m_asVehicle.centerOfMassAngularVelocity(a_signal)= m_theBox->getAngularVelocity();

	SpatialTransform transform;
	transform = m_theBox->getTransform();
	m_asVehicle.chassisTransform(a_signal) = transform;

	SpatialVector offset(-2.5, 0, -0.75);
	translate(transform, offset);
	m_asVehicle.axlesRearTransform(a_signal) = transform;

	m_asVehicle.centerOfMassOffset(a_signal) = -offset;

	transform = getWheelTransform(0);
	m_asVehicle.wheelFLTransform(a_signal) = transform;

	transform = getWheelTransform(1);
	m_asVehicle.wheelFRTransform(a_signal) = transform;

	transform = getWheelTransform(2);
	m_asVehicle.wheelRLTransform(a_signal) = transform;

	transform = getWheelTransform(3);
	m_asVehicle.wheelRRTransform(a_signal) = transform;

	//* circum = 2*pi*radius => angle = distance / radius
	float_t wheelAngVel=m_displacement*(1.0/BOXCAR_WHEEL_RADIUS)*(1.0/deltaTime);

	m_asVehicle.wheelFLAngularVelocity(a_signal)=wheelAngVel;
	m_asVehicle.wheelFRAngularVelocity(a_signal)=wheelAngVel;
	m_asVehicle.wheelRLAngularVelocity(a_signal)=wheelAngVel;
	m_asVehicle.wheelRRAngularVelocity(a_signal)=wheelAngVel;
}

void Boxcar::tick(float_t a_deltaTime)
{
	// Get heading (unit vector in vehicle's forward direction)
	SpatialVector defaultHeading(1, 0, 0);
	SpatialTransform transform = m_theBox->getTransform();
	SpatialVector heading;
	rotateVector(transform, defaultHeading, heading);

	// Update velocity to be in forward direction
	SpatialVector vel = m_theBox->getVelocity(m_theBox->getPosition());
	Real speed = magnitude(vel);
	feLog("speed %.6G\n",speed);
	vel = speed * heading;

	m_theBox->setVelocity(vel);

	// Apply steering (about z/up axis)
	Real angleRadians = m_steering * BOXCAR_MAX_STEER_DEGREES * M_PI / 180;
	angleRadians *= fe::maximum(0.5, 1.0-speed/150.0);
	SpatialVector angVel(0, 0, -angleRadians);
	m_steerAngle = angleRadians;
	m_theBox->setAngularVelocity(angVel);

	// Apply acceleration based on throttle/brake
	float acc = (m_throttle>0.0) ? m_throttle : -m_brake;
	SpatialVector forceFromAcceleration = (acc * m_theBox->getMass()) * heading;
	feLog("acceleration %.6G %.6G %.6G\n",forceFromAcceleration[0],forceFromAcceleration[1],forceFromAcceleration[2]);
	m_theBox->applyForce(forceFromAcceleration, m_theBox->getPosition());

	// Ground force
	SpatialVector groundForce(0, 0, BOXCAR_MASS * 9.8);
	m_theBox->applyForce(groundForce, m_theBox->getPosition());

	m_spConnectedSystem->step(a_deltaTime);

	SpatialVector position = m_theBox->getPosition();
	m_displacement = magnitude(m_previousPosition - position);
	m_previousPosition = position;
}

fe::SpatialTransform Boxcar::getWheelTransform(uint8_t idx) // 0 = FL; 1 = FR; 2 = RL; 3 = RR.
{
	SpatialTransform transform = m_theBox->getTransform();
	SpatialVector position = m_wheelOffsets[idx];
	SpatialVector transformedPosition;
	transformVector(transform, position, transformedPosition);
	translate(transform, m_wheelOffsets[idx]);
	if (idx == 0 || idx == 1)
	{
		rotate(transform, -m_steerAngle, e_zAxis);
	}
	setTranslation(transform, transformedPosition);
	return transform;
}

void printTransform(String variable, const SpatialTransform& transform)
{
	feLog("%s: %f %f %f\n%f %f %f\n%f %f %f\n%f %f %f\n",
		variable.c_str(),
		transform(0,0), transform(1,0), transform(2,0),
		transform(0,1), transform(1,1), transform(2,1),
		transform(0,2), transform(1,2), transform(2,2),
		transform(0,3), transform(1,3), transform(2,3));
}

} // namespace hive
