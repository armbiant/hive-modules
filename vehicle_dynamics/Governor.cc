/*	Copyright (C) 2018 Arrival Ltd
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit git.hiverepo.com */

/** @file */

#include "vehicle_dynamics.pmh"

using namespace fe;
using namespace fe::ext;

#define HIVE_GOVERNOR_VERBOSE	FALSE

namespace hive
{

Governor::Governor(void)
{
}

Governor::~Governor(void)
{
}

void Governor::initialize(void)
{
}

void Governor::handle(fe::Record& a_signal)
{
#if HIVE_GOVERNOR_VERBOSE
	feLog("Governor::handle layout \"%s\"\n",
			a_signal.layout()->name().c_str());
#endif

	if(!m_asVehicle.bind(a_signal))
	{
		//feLog("Governor::handle not AsVehicle\n");
		return;
	}

	const F64 deltaTime=m_asVehicle.deltaTime(a_signal);
	if(deltaTime<=0.0)
	{
		return;
	}

#if FALSE
	//* four seconds on, four seconds off
	static Real totalTime(0);
	totalTime+=deltaTime;
	const F64 maxSpeed=((I32(totalTime)/4)%2)? 10.0: 100.0;
#else
	const F64 maxSpeed=m_asVehicle.maxSpeed(a_signal);
#endif

	const F64 speed=m_asVehicle.speed(a_signal);
	const F64 throttle=m_asVehicle.throttleInput(a_signal);
	const F64 brake=m_asVehicle.brakeInput(a_signal);
	const AsVehicle::GearMode gearMode=
			AsVehicle::GearMode(m_asVehicle.gearModeInput(a_signal));

#if HIVE_GOVERNOR_VERBOSE
	feLog("Governor::handle"
			" throttle %.3f brake %.3f gearMode %d deltaTime %.3f\n",
			throttle,brake,gearMode,deltaTime);
#endif

#if HIVE_GOVERNOR_VERBOSE
	feLog("Governor::handle speed %.3f/%.3f\n",speed,maxSpeed);
#endif

	//* NOTE powered braking always allowed
	const BWORD powered=(speed<=maxSpeed ||
			gearMode==AsVehicle::e_gearModeNeutral ||
			(brake>=0.0 && throttle<=0.01));

	m_asVehicle.powerScale(a_signal)= powered? 1.0: 0.0;

	const Real minimumBrake(0.4);	//* TODO param
	if(!powered && brake<minimumBrake)
	{
		m_asVehicle.brakeInput(a_signal)=minimumBrake;
	}
}

}
