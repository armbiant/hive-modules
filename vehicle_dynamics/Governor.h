/*	Copyright (C) 2018 Arrival Ltd
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit git.hiverepo.com */

/** @file */

#ifndef governor_vehicle_dynamics_H
#define governor_vehicle_dynamics_H

namespace hive
{

class FE_DL_EXPORT Governor:
	virtual public fe::ext::HandlerI,
	public Initialize<Governor>
{
public:
				Governor(void);
virtual			~Governor(void);

		void	initialize(void);

virtual	void	bind(fe::sp<fe::ext::SignalerI> spSignalerI,
					fe::sp<fe::Layout> spLayout)							{}
virtual	void	handle(fe::Record& a_signal);

private:

	AsVehicle					m_asVehicle;
};

} // namespace hive

#endif
