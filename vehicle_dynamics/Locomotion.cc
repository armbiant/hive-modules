/*	Copyright (C) 2018 Arrival Ltd
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit git.hiverepo.com */

/** @file */

#include "vehicle_dynamics.pmh"

using namespace fe;
using namespace fe::ext;

#define HIVE_LOCO_VERBOSE	FALSE

namespace hive
{

Locomotion::Locomotion(void):
	m_started(FALSE)
{
}

void Locomotion::initialize(void)
{
	m_spHiveHost=HiveHost::create();
	if(m_spHiveHost.isNull())
	{
		feLog("Locomotion::handle no HiveHost\n");
		return;
	}

	m_spStateCatalog=m_spHiveHost->accessSpace("world");
}

void Locomotion::handle(fe::Record& a_signal)
{
	if(m_spStateCatalog.isNull())
	{
		feLog("Locomotion::handle no StateCatalog\n");
		return;
	}

	if(!m_asObstacle.bind(a_signal))
	{
		feLog("Locomotion::handle not AsObstacle\n");
		return;
	}

	const Real deltaTime=m_asObstacle.deltaTime(a_signal);
	tick(deltaTime);
}

void Locomotion::tick(float_t a_deltaTime)
{
	Array<String> keys;
	m_spStateCatalog->getStateKeys(keys,"obstacles\\.[^.]*\\.speed$");

#if HIVE_LOCO_VERBOSE
	feLog("Locomotion::tick %.3f\n",a_deltaTime);
#endif

	const I32 keyCount=keys.size();
	for(I32 keyIndex=0;keyIndex<keyCount;keyIndex++)
	{
		String key=keys[keyIndex];
		key.parse("",".");
		const String obstacleID=key.parse("",".");

		const String prefix="obstacles."+obstacleID+".";

		//* TODO cache things

		const String timeKey=prefix+"time";

		Real timeTotal(0);
		m_spStateCatalog->getState<Real>(timeKey,timeTotal);

		timeTotal+=a_deltaTime;
		m_spStateCatalog->setState<Real>(timeKey,timeTotal);

#if HIVE_LOCO_VERBOSE
		feLog("Locomotion::handle prefix \"%s\" time %.3f\n",
				prefix.c_str(),timeTotal);
#endif

		Array<String> waypointKeys;
		m_spStateCatalog->getStateKeys(waypointKeys,
				"obstacles\\.[^.]*\\.waypoints\\..*");

		const I32 waypointCount=waypointKeys.size();
		if(!waypointCount)
		{
			continue;
		}

		Real speed(0);
		m_spStateCatalog->getState<Real>(prefix+"speed",speed);

		SpatialVector* pControlPoints=new SpatialVector[waypointCount*3];

		Real loopLength(0);
		SpatialVector lastWaypoint;

		for(I32 waypointIndex=0;waypointIndex<waypointCount;waypointIndex++)
		{
			const String& waypointKey=waypointKeys[waypointIndex];

			SpatialVector waypoint(0,0,0);
			m_spStateCatalog->getState<SpatialVector>(waypointKey,waypoint);

#if HIVE_LOCO_VERBOSE
			feLog("Locomotion::handle \"%s\" %s\n",
					waypointKey.c_str(),c_print(waypoint));
#endif

			pControlPoints[waypointIndex]=waypoint;
			pControlPoints[waypointIndex+waypointCount]=waypoint;
			pControlPoints[waypointIndex+waypointCount*2]=waypoint;

			if(waypointIndex)
			{
				loopLength+=magnitude(waypoint-lastWaypoint);
			}

			lastWaypoint=waypoint;
		}

		const Real extraSegment=
				magnitude(pControlPoints[0]-pControlPoints[waypointCount-1]);
		loopLength+=extraSegment;

		Real distance=timeTotal*speed;
		distance-=loopLength*I32(distance/loopLength);

		const Real t1=loopLength-0.5*extraSegment+distance;
		const Real t2=(distance<0.5*loopLength)? t1+loopLength: t1-loopLength;

		const Real w2=fabs(distance-0.5*loopLength)/loopLength;
		const Real w1=1.0-w2;

		ConvergentSpline spline;
		spline.configure(waypointCount*3,pControlPoints,NULL);

		delete[] pControlPoints;

		const SpatialVector location=
				w1*spline.solve(t1)+
				w2*spline.solve(t2);
		const SpatialVector ahead=
				w1*spline.solve(t1+0.001)+
				w2*spline.solve(t2+0.001);

		const SpatialVector dirX=unitSafe(ahead-location);

#if HIVE_LOCO_VERBOSE
		feLog("t %.3f %.3f dist %.3f/%.3f location %s dirX %s\n",
				t1,t2,distance,loopLength,c_print(location),c_print(dirX));
#endif

		const SpatialVector up(0,0,1);
		SpatialTransform xForm;
		makeFrameNormalZ(xForm,location,dirX,up);

		m_spStateCatalog->setState<SpatialTransform>(prefix+"transform",xForm);
	}
}

}
