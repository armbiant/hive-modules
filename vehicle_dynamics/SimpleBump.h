/*	Copyright (C) 2018 Arrival Ltd
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit git.hiverepo.com */

/** @file */

#ifndef hive_simple_bump_h
#define hive_simple_bump_h

namespace hive
{

class FE_DL_EXPORT SimpleBump:
	virtual public fe::ext::HandlerI,
	public fe::Initialize<SimpleBump>
{
public:
				SimpleBump(void);
virtual			~SimpleBump(void)											{}

		void	initialize(void);

virtual	void	bind(fe::sp<fe::ext::SignalerI> spSignalerI,
					fe::sp<fe::Layout> spLayout)							{}
virtual	void	handle(fe::Record& a_signal);

private:
		void	tick(float_t a_deltaTime);

	fe::sp<HiveHost>			m_spHiveHost;
	fe::sp<fe::StateCatalog>	m_spStateCatalog;
	BWORD						m_started;

	fe::String					m_vehicleID;
	fe::Vector3d				m_velocity;
	fe::Real					m_mass;
	fe::Vector3d				m_pivot;

	fe::Matrix3x4d				m_chassis;
	fe::Vector3d				m_impulse;
	fe::Quaterniond				m_angularImpulse;

	AsVehicle					m_asVehicle;
};

} // namespace hive

#endif
