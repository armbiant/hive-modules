/*	Copyright (C) 2018 Arrival Ltd
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit git.hiverepo.com */

/** @file */

#ifndef track_sampler_vehicle_dynamics_H
#define track_sampler_vehicle_dynamics_H

namespace hive
{

class FE_DL_EXPORT TrackSampler:
	virtual public fe::ext::HandlerI,
	public Initialize<TrackSampler>
{
public:
				TrackSampler(void);
virtual			~TrackSampler(void);

		void	initialize(void);

virtual	void	bind(fe::sp<fe::ext::SignalerI> spSignalerI,
					fe::sp<fe::Layout> spLayout)							{}
virtual	void	handle(fe::Record& a_signal);

private:

		fe::sp<HiveHost>			m_spHiveHost;
		fe::sp<fe::StateCatalog>	m_spStateCatalog;
		fe::sp<fe::ext::ImageI>		m_spImageI;

		AsVehicle					m_asVehicle;
};

} // namespace hive

#endif
