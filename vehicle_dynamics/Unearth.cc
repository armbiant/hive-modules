/*	Copyright (C) 2018 Arrival Ltd
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit git.hiverepo.com */

/** @file */

#include "vehicle_dynamics.pmh"

using namespace fe;
using namespace fe::ext;

#define HIVE_BUMP_VERBOSE	FALSE

namespace hive
{

Unearth::Unearth(void)
{
}

void Unearth::initialize(void)
{
	m_spHiveHost=HiveHost::create();
	if(m_spHiveHost.isNull())
	{
		feLog("Unearth::handle no HiveHost\n");
		return;
	}

	m_spStateCatalog=m_spHiveHost->accessSpace("world");
}

void Unearth::handle(fe::Record& a_signal)
{
	if(m_spStateCatalog.isNull())
	{
		feLog("Unearth::handle no StateCatalog\n");
		return;
	}

	if(!m_asVehicle.bind(a_signal))
	{
		feLog("Unearth::handle not AsVehicle\n");
		return;
	}

	sp<SurfaceI> spSurfaceTrack=
			sp<SurfaceI>(m_asVehicle.surfaceTrack(a_signal));
	if(spSurfaceTrack.isNull())
	{
		feLog("Unearth::handle null track\n");
		return;
	}

	Matrix3x4d chassis=m_asVehicle.chassisTransform(a_signal);
	if(magnitude(chassis.direction())<0.99)
	{
		feLog("Unearth::handle chassis transform not set\n");
		return;
	}

	const SpatialVector rayOrigin=
			SpatialVector(chassis.translation())-SpatialVector(0,0,100);
	const SpatialVector rayDirection(0,0,1);
	const Real maxDistance(-1);
	sp<SurfaceI::ImpactI> spImpact=spSurfaceTrack.asConst()->rayImpact(
			rayOrigin,rayDirection,maxDistance);
	if(spImpact.isNull())
	{
		spImpact=spSurfaceTrack.asConst()->nearestPoint(rayOrigin,maxDistance);
	}

	if(spImpact.isNull())
	{
		feLog("Unearth::handle no ground near %s\n",c_print(rayOrigin));
		return;
	}

	const SpatialVector intersection=spImpact->intersection();

	const Real margin(1.0);
	if(chassis.translation()[2]<intersection[2]-margin)
	{
		feLog("Unearth::handle \"%s\" snap chassis from %s to %s\n",
				m_asVehicle.vehicleID(a_signal).c_str(),
				c_print(chassis.translation()),c_print(intersection));

		m_asVehicle.chassisTransform(a_signal).translation()=intersection;
	}
}

}
