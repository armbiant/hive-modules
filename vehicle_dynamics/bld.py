import os
import sys
import string
forge = sys.modules["forge"]

def prerequisites():
	return [ "rigid_body_engine" ]

def setup(module):
	srcList = [	"vehicle_dynamics.pmh",
				"BlankCar",
				"Boxcar",
				"Governor",
				"Locomotion",
				"Scooter",
				"SimpleBump",
				"TrackSampler",
				"Unearth",
				"vehicleDynamicsDL" ]

	dll = module.DLL( "hiveVehicleDynamicsDL", srcList )

	deplibs = forge.corelibs + [
				"fexGeometryDLLib",
				"hiveApiaryDLLib",
				"hiveRigidBodyEngineDLLib"]

	if forge.fe_os == 'FE_WIN32' or forge.fe_os == 'FE_WIN64':
		deplibs += [	"fexDrawDLLib",
						"fexMechanicsDLLib",
						"fexMoaDLLib",
						"fexSolveDLLib",
						"fexThreadDLLib" ]

	forge.deps( ["hiveVehicleDynamicsDLLib"], deplibs )

	forge.tests += [
		("inspect.exe",	"hiveVehicleDynamicsDL",	None,	None) ]

	#module.Module('test')
