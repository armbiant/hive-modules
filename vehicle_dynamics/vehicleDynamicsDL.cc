/*	Copyright (C) 2018 Arrival Ltd
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit git.hiverepo.com */

/** @file */

#include "vehicle_dynamics.pmh"
#include "platform/dlCore.cc"

using namespace fe;
using namespace hive;
extern "C"
{

//-----------------------------------------------------------------
// ListDependencies()
//-----------------------------------------------------------------
FE_DL_EXPORT void ListDependencies(List<String*>& list)
{
}


//-----------------------------------------------------------------
// CreateLibrary()
//-----------------------------------------------------------------

FE_DL_EXPORT Library* CreateLibrary(sp<Master> spMaster)
{
	Library *pLibrary = new Library();

	pLibrary->add<BlankCar>("HandlerI.BlankCar.hive");

	pLibrary->add<Boxcar>("HandlerI.Boxcar.hive");

	pLibrary->add<Governor>("HandlerI.Governor.hive");

	pLibrary->add<Locomotion>("HandlerI.Locomotion.hive");

	pLibrary->add<Scooter>("HandlerI.Scooter.hive");

	pLibrary->add<SimpleBump>("HandlerI.SimpleBump.hive");

	pLibrary->add<TrackSampler>("HandlerI.TrackSampler.hive");

	pLibrary->add<Unearth>("HandlerI.Unearth.hive");

	return pLibrary;
}


//-----------------------------------------------------------------
// InitializeLibrary()
//-----------------------------------------------------------------

FE_DL_EXPORT void InitializeLibrary(sp<Library> spLibrary)
{
}

}
