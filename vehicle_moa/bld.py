import sys
forge = sys.modules["forge"]

def prerequisites():
	return [ ]

def setup(module):
	srcList = [ "vehicle_moa.pmh",
				"vehicleMoaDL" ]

	dll = module.DLL( "hiveVehicleMoaDL", srcList )

	deplibs = forge.corelibs + [
				"feMathLib" ]

	forge.deps( ["hiveVehicleMoaDLLib"], deplibs )

	forge.tests += [
		("inspect.exe", "vehicleMoaDL",			None,	None) ]

	module.Module('test')
