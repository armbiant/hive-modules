import sys
forge = sys.modules["forge"]

import os.path

def setup(module):
	deplibs = [
		'fexSignalLib',
		'fexSolveDLLib',
		'hiveTireHiveDLLib',
		'fexDataToolLib',
		'fexEvaluateDLLib',
		"fexThreadDLLib",
		"fexWindowLib",
		"fexViewerDLLib",
		"hiveViewerSystemDLLib",
		'feMathLib' ] + forge.corelibs

	tests = [ 'xMoaVehicleViewer' ]

	for t in tests:
		exe = module.Exe(t)
		exe.linkmap = { "gfxlibs": forge.gfxlibs }

		#exe.linkmap['tbb'] = ' -ltbb'

		forge.deps([t + "Exe"], deplibs)

	plugin_path = os.path.join(module.modPath, 'plugin')
