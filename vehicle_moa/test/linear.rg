INFO 5

###################################################################
# Linear Parameters
ATTRIBUTE	name									string
ATTRIBUTE	tire:component							string		"=TireI.Linear"
ATTRIBUTE	tire:z:stiffness						real		=200000
ATTRIBUTE	tire:z:damping							real		=10000
ATTRIBUTE	tire:xy:stiffness						real		=10.1
ATTRIBUTE	tire:friction							real		=0.43
ATTRIBUTE	tire:Fmax								real		=1e5
ATTRIBUTE	tire:pneumatic_trail					real		=0.05
ATTRIBUTE	tire:do_rollover						boolean		=true

ATTRIBUTE tire:model:upper string
ATTRIBUTE tire:model:lower string
ATTRIBUTE tire:slow_blend vector4 "=2.0 1.0 100.0 1.0"

ATTRIBUTE tire:stick:bias	real		=0.5
ATTRIBUTE tire:stick:stiffness	real	=1.0e2

LAYOUT linear_tire_model
	name
	tire:component
	tire:z:stiffness
	tire:z:damping
	tire:xy:stiffness
	tire:friction
	tire:Fmax
	tire:pneumatic_trail
	tire:do_rollover

LAYOUT stick_tire_model
	name
	tire:component
	tire:z:stiffness
	tire:z:damping
	tire:friction
	tire:stick:bias
	tire:stick:stiffness
	tire:xy:stiffness

LAYOUT blend_tire_model
	name
	tire:component
	tire:model:upper
	tire:model:lower
	tire:slow_blend

DEFAULTGROUP 1

RECORD * linear_tire_model
	name "Linear Default"

RECORD * linear_tire_model
	name "Linear Flat"
	tire:z:stiffness 100000
	tire:z:damping 10000

RECORD * stick_tire_model
	name "Stick Default"
	tire:component "TireI.Stick"
	tire:friction			1.5
	tire:stick:bias			0.5
	tire:stick:stiffness	1.0e5
	tire:xy:stiffness		1.0e2

RECORD * blend_tire_model
	name "Blend Default"
	tire:component "TireI.Blend"
	tire:model:lower "Stick Default"
	tire:model:upper "Linear Default"

END

