INFO 5

ATTRIBUTE name string
ATTRIBUTE composer:linger real =1.0
ATTRIBUTE composer:frequency real =600.0
ATTRIBUTE loop:duration real "=10000.1"

ATTRIBUTE dataset:active boolean =false
ATTRIBUTE dataset:group group

ATTRIBUTE mixer:inputs stringarray

ATTRIBUTE spc:at vector3 spatial_vector
ATTRIBUTE spc:velocity vector3 spatial_vector "=0 0 0"
ATTRIBUTE sim:force vector3 spatial_vector "=0 0 0"
ATTRIBUTE sim:moment vector3 spatial_vector "=0 0 0"
ATTRIBUTE sim:mass real =10
ATTRIBUTE bnd:radius real
ATTRIBUTE sim:color color
ATTRIBUTE pair:left WeakRecord
ATTRIBUTE pair:right WeakRecord
ATTRIBUTE bdy:length real =-1
ATTRIBUTE mat:stiffness real =1e+9
ATTRIBUTE mat:damping real =0e+7
ATTRIBUTE mat:flags integer int I32 =0

ATTRIBUTE soft:solve_extract boolean =true
ATTRIBUTE soft:structure group

ATTRIBUTE tire:model string
ATTRIBUTE tire:radius real "=0.22"
ATTRIBUTE tire:width real "=0.3"
ATTRIBUTE tire:inclination real "=0.0"

ATTRIBUTE mount:tire string
ATTRIBUTE mount:stripped_force boolean =false
ATTRIBUTE mount:p0 WeakRecord
ATTRIBUTE mount:p1 WeakRecord
ATTRIBUTE mount:p2 WeakRecord
ATTRIBUTE mount:at vector3 spatial_vector
ATTRIBUTE spc:transform_inv transform "=0 0 0 0 0 0 0 0 0 0 0 0"

ATTRIBUTE ground:height real

ATTRIBUTE quad:a WeakRecord
ATTRIBUTE quad:b WeakRecord
ATTRIBUTE quad:c WeakRecord
ATTRIBUTE quad:d WeakRecord
ATTRIBUTE is:bend void

ATTRIBUTE strand:a	WeakRecord
ATTRIBUTE strand:b	WeakRecord
ATTRIBUTE strand:c	WeakRecord
ATTRIBUTE strand:bend void

ATTRIBUTE spc:transform transform "=1 0 0 0   0 1 0 0   0 0 1 0"
ATTRIBUTE moa:contact spatial_vector "=0 0 -10000"
ATTRIBUTE spc:normal spatial_vector
ATTRIBUTE is:plane void
ATTRIBUTE frame:zyx vector3

LAYOUT loop
	name
	composer:linger
	composer:frequency
	loop:duration
LAYOUT dataset
	name
	dataset:group
	dataset:active
LAYOUT l_rig_particle
	name
	spc:at
	spc:velocity
	sim:force
	sim:mass
	bnd:radius
	sim:color
LAYOUT l_rig_constraint
	spc:at
	spc:velocity
	sim:force
	bnd:radius
	sim:color
LAYOUT l_rig_spring
	pair:left
	pair:right
	bdy:length
	mat:stiffness
	mat:damping
	mat:flags
LAYOUT l_bend
	quad:a
	quad:b
	quad:c
	quad:d
	mat:stiffness
	mat:damping
	is:bend
LAYOUT l_strand
	strand:a
	strand:b
	strand:c
	mat:stiffness
	mat:damping
	strand:bend
LAYOUT l_soft
	name
	soft:solve_extract
	soft:structure
	
LAYOUT l_tire
	name
	tire:radius
	tire:width
	tire:model
	spc:transform
	moa:contact
	spc:normal
LAYOUT l_mount
	mount:tire
	mount:p0
	mount:p1
	mount:p2
	mount:stripped_force
	spc:transform_inv
	mount:at
	tire:inclination
LAYOUT l_just_a_locator_from_points
	mount:p0
	mount:p1
	mount:p2
	spc:transform
	sim:force
	sim:moment
	spc:at
	frame:zyx
LAYOUT l_locator_tire
	mount:p0
	mount:p1
	mount:p2
	spc:transform
	sim:force
	sim:moment
	spc:at
	frame:zyx
	tire:radius
	tire:width
	tire:model
	moa:contact
	spc:normal
	#spc:velocity

LAYOUT l_surface
	spc:transform
	spc:velocity
	is:plane
	
LAYOUT l_ground
	ground:height

DEFAULTGROUP 1

RECORD * dataset
	name		"Generic Vehicle"
	dataset:group RG_GENERIC_VEHICLE
	dataset:active true

DEFAULTGROUP RG_GENERIC_VEHICLE

TEMPLATE T_SPRINGDAMPER l_rig_spring
	mat:stiffness 40000
	mat:damping 10000

TEMPLATE T_SHEET l_bend
	mat:stiffness 10000
	mat:damping 1000

TEMPLATE T_STRAND l_strand
	mat:stiffness 10000
	mat:damping 10

TEMPLATE T_TIRE l_tire
	tire:radius 0.22
	tire:model "Blend Default"

RECORD * l_surface
	spc:transform  "1 0 0 0 0 1 0 0 0 0 1 -0.50"

RECORD * l_ground
	ground:height -0.50

DEFAULTGROUP 0
RECORD * T_TIRE
	name "FR tire"

RECORD * l_mount
	mount:tire "FR tire"
	mount:p0	FR_upright_R
	mount:p1	upright[FR][U]
	mount:p2	FR_upright_L
	mount:at	"0.1 -0.8 0.0"
	tire:inclination 0.0
DEFAULTGROUP RG_GENERIC_VEHICLE


RECORD just_a_locator_from_points l_just_a_locator_from_points
	name "Just a Locator"
	mount:p0		FR_upright_R
	mount:p1		upright[FR][U]
	mount:p2		FR_upright_L

	sim:force		"0.0 0.0 0.0"
	sim:moment		"0.0 0.0 0.0"

	# AsEulerLocator:
	spc:at			"0.1 -0.8 1.2"
	frame:zyx		"0.0 0.0 -0.2"

DEFAULTGROUP RG_GENERIC_VEHICLE
RECORD * l_locator_tire
	name "Locator Tire"
	mount:p0		FR_upright_R
	mount:p1		upright[FR][U]
	mount:p2		FR_upright_L

	sim:force		"0.0 0.0 0.0"
	sim:moment		"0.0 0.0 0.0"

	# AsEulerLocator:
	spc:at			"0.1 -0.8 0.0"
	frame:zyx		"0.4 0.4 0.4"

	tire:radius 0.22
	tire:model "Blend Default"
DEFAULTGROUP RG_GENERIC_VEHICLE

RECORD * T_TIRE
	name "FL tire"

RECORD * l_mount
	mount:tire "FL tire"
	mount:p0	FL_upright_R
	mount:p1	FL_upright_U
	mount:p2	FL_upright_L
	mount:at	"0.1 0.8 0.0"
	tire:inclination 3.14159
	#tire:inclination 2.7
	#tire:inclination 0.0

RECORD * T_TIRE
	name "RR tire"

RECORD * l_mount
	mount:tire "RR tire"
	mount:p0	RRU_apex
	mount:p1	RLU_apex
	mount:p2	FRL_wishbone_root_R
	mount:at	"-1.3 -0.8 0.0"
	tire:inclination 0.0

RECORD * T_TIRE
	name "RL tire"
	tire:radius 0.30
	tire:width 0.2
	tire:model "Blend Default"

RECORD * l_mount
	mount:tire "RL tire"
	mount:p0	RLU_apex
	mount:p1	RRU_apex
	mount:p2	FLL_wishbone_root_R
	mount:at	"-1.3 0.8 0.0"
	tire:inclination 0.0

RECORD * loop
	name				"loop"

RECORD R_SELF l_soft
	name "base_soft"
	soft:solve_extract 1
	soft:structure RG_SOFT

DEFAULTGROUP RG_GENERIC_VEHICLE RG_SOFT

RECORD FR_tie_rod_root l_rig_particle
	spc:at "	-0.15	-0.1	0.05"
RECORD FR_spring_root l_rig_particle
	spc:at "	0		-0.1	0.3"
RECORD FR_upright_R l_rig_particle
	spc:at "	0		-0.7	0"
#RECORD FR_upright_U l_rig_particle
RECORD upright[FR][U] l_rig_particle
	name upright.FR.U
	spc:at "	0.1		-0.7	0.1"
RECORD FR_upright_L l_rig_particle
	spc:at "	0.1		-0.7	-0.1"
RECORD FRU_wishbone_root_F l_rig_particle
	name "something wrong"
	spc:at "	0.2		-0.2	0.1"
RECORD FRU_wishbone_root_R l_rig_particle
	spc:at "	-0.2	-0.2	0.1"
RECORD FRL_wishbone_root_F l_rig_particle
	spc:at "	0.2		-0.2	-0.1"
RECORD FRL_wishbone_root_R l_rig_particle
	spc:at "	-0.2	-0.2	-0.1"


RECORD * l_rig_spring
	pair:left upright[FR][U]
	pair:right FRU_wishbone_root_F
RECORD * l_rig_spring
	pair:left upright[FR][U]
	pair:right FRU_wishbone_root_R

RECORD * l_rig_spring
	pair:left FR_upright_L
	pair:right FRL_wishbone_root_F
RECORD * l_rig_spring
	pair:left FR_upright_L
	pair:right FRL_wishbone_root_R

RECORD * l_rig_spring
	pair:left upright[FR][U]
	pair:right FR_upright_R
RECORD * l_rig_spring
	pair:left FR_upright_L
	pair:right FR_upright_R
RECORD * l_rig_spring
	pair:left upright[FR][U]
	pair:right FR_upright_L


RECORD * T_SPRINGDAMPER
	pair:left FR_upright_L
	pair:right FR_spring_root

RECORD * l_rig_spring
	pair:left FR_tie_rod_root
	pair:right FR_upright_R

RECORD * l_rig_spring
	pair:left FR_tie_rod_root
	pair:right FRL_wishbone_root_F
RECORD * l_rig_spring
	pair:left FR_tie_rod_root
	pair:right FRU_wishbone_root_R
RECORD * l_rig_spring
	pair:left FR_tie_rod_root
	pair:right FRL_wishbone_root_R


#RECORD * l_rig_spring
#	pair:left FRU_wishbone_root_F
#	pair:right FRU_wishbone_root_R
RECORD * l_rig_spring
	pair:left FRU_wishbone_root_F
	pair:right FRL_wishbone_root_F
#RECORD * l_rig_spring
#	pair:left FRU_wishbone_root_R
#	pair:right FRL_wishbone_root_F

RECORD * l_rig_spring
	pair:left FRL_wishbone_root_F
	pair:right FRL_wishbone_root_R
#RECORD * l_rig_spring
#	pair:left FRL_wishbone_root_F
#	pair:right FRU_wishbone_root_R
RECORD * l_rig_spring
	pair:left FRL_wishbone_root_R
	pair:right FRU_wishbone_root_R

RECORD * l_rig_spring
	pair:left FRU_wishbone_root_F
	pair:right FR_spring_root
RECORD * l_rig_spring
	pair:left FRU_wishbone_root_R
	pair:right FR_spring_root


RECORD * T_SHEET
	quad:a	FR_spring_root
	quad:b	FRL_wishbone_root_F
	quad:c	FRU_wishbone_root_F
	quad:d	FRU_wishbone_root_R

RECORD * T_SHEET
	quad:a	FRL_wishbone_root_R
	quad:b	FRU_wishbone_root_F
	quad:c	FRU_wishbone_root_R
	quad:d	FRL_wishbone_root_F

#RECORD * l_strand
#	strand:a	FRU_wishbone_root_R
#	strand:b	FRL_wishbone_root_R
#	strand:c	FR_upright_L
#	mat:stiffness 1000000
#	mat:damping 0

#RECORD * l_strand
#	strand:a	FRU_wishbone_root_F
#	strand:b	FRL_wishbone_root_F
#	strand:c	FR_upright_L
#	mat:stiffness 1000000
#	mat:damping 0


RECORD FL_tie_rod_root l_rig_particle
	spc:at "	-0.15	0.1		0.05"
RECORD FL_spring_root l_rig_particle
	spc:at "	0		0.1		0.3"
RECORD FL_upright_R l_rig_particle
	spc:at "	0		0.7		0"
RECORD FL_upright_U l_rig_particle
	spc:at "	0.1		0.7		0.1"
RECORD FL_upright_L l_rig_particle
	spc:at "	0.1		0.7		-0.1"
RECORD FLU_wishbone_root_F l_rig_particle
	spc:at "	0.2		0.2		0.1"
RECORD FLU_wishbone_root_R l_rig_particle
	spc:at "	-0.2	0.2		0.1"
RECORD FLL_wishbone_root_F l_rig_particle
	spc:at "	0.2		0.2		-0.1"
RECORD FLL_wishbone_root_R l_rig_particle
	spc:at "	-0.2	0.2		-0.1"


RECORD * l_rig_spring
	pair:left FL_upright_U
	pair:right FLU_wishbone_root_F
RECORD * l_rig_spring
	pair:left FL_upright_U
	pair:right FLU_wishbone_root_R

RECORD * l_rig_spring
	pair:left FL_upright_L
	pair:right FLL_wishbone_root_F
RECORD * l_rig_spring
	pair:left FL_upright_L
	pair:right FLL_wishbone_root_R

RECORD * l_rig_spring
	pair:left FL_upright_U
	pair:right FL_upright_R
RECORD * l_rig_spring
	pair:left FL_upright_L
	pair:right FL_upright_R
RECORD * l_rig_spring
	pair:left FL_upright_U
	pair:right FL_upright_L


RECORD * T_SPRINGDAMPER
	pair:left FL_upright_L
	pair:right FL_spring_root

RECORD * l_rig_spring
	pair:left FL_tie_rod_root
	pair:right FL_upright_R

RECORD * l_rig_spring
	pair:left FL_tie_rod_root
	pair:right FLL_wishbone_root_F
RECORD * l_rig_spring
	pair:left FL_tie_rod_root
	pair:right FLU_wishbone_root_R
RECORD * l_rig_spring
	pair:left FL_tie_rod_root
	pair:right FLL_wishbone_root_R


RECORD * l_rig_spring
	pair:left FLU_wishbone_root_F
	pair:right FLU_wishbone_root_R
RECORD * l_rig_spring
	pair:left FLU_wishbone_root_F
	pair:right FLL_wishbone_root_F
RECORD * l_rig_spring
	pair:left FLU_wishbone_root_R
	pair:right FLL_wishbone_root_F

RECORD * l_rig_spring
	pair:left FLL_wishbone_root_F
	pair:right FLL_wishbone_root_R
#RECORD * l_rig_spring
#	pair:left FLL_wishbone_root_F
#	pair:right FLU_wishbone_root_R
RECORD * l_rig_spring
	pair:left FLL_wishbone_root_R
	pair:right FLU_wishbone_root_R

RECORD * l_rig_spring
	pair:left FLU_wishbone_root_F
	pair:right FL_spring_root
RECORD * l_rig_spring
	pair:left FLU_wishbone_root_R
	pair:right FL_spring_root



RECORD * l_rig_spring
	pair:left FLU_wishbone_root_F
	pair:right FRU_wishbone_root_F
RECORD * l_rig_spring
	pair:left FLL_wishbone_root_F
	pair:right FRL_wishbone_root_F
RECORD * l_rig_spring
	pair:left FLU_wishbone_root_R
	pair:right FRU_wishbone_root_R
RECORD * l_rig_spring
	pair:left FLL_wishbone_root_R
	pair:right FRL_wishbone_root_R

RECORD * l_rig_spring
	pair:left FLU_wishbone_root_F
	pair:right FRL_wishbone_root_F
RECORD * l_rig_spring
	pair:left FLU_wishbone_root_R
	pair:right FRL_wishbone_root_R
RECORD * l_rig_spring
	pair:left FLU_wishbone_root_R
	pair:right FRU_wishbone_root_F
RECORD * l_rig_spring
	pair:left FLL_wishbone_root_R
	pair:right FRL_wishbone_root_F

RECORD * T_SHEET
	quad:a	FL_spring_root
	quad:b	FLL_wishbone_root_F
	quad:c	FLU_wishbone_root_F
	quad:d	FLU_wishbone_root_R

RECORD * T_SHEET
	quad:a	FLL_wishbone_root_R
	quad:b	FLU_wishbone_root_F
	quad:c	FLU_wishbone_root_R
	quad:d	FLL_wishbone_root_F


RECORD RRU_apex l_rig_particle
	spc:at "	-1.3	-0.4	0.0"
	sim:mass	100.0
RECORD RLU_apex l_rig_particle
	spc:at "	-1.3	0.4		0.0"
	sim:mass	100.0

RECORD * l_rig_spring
	pair:left RRU_apex
	pair:right FRL_wishbone_root_R
#RECORD * l_rig_spring
#	pair:left RLU_apex
#	pair:right FLL_wishbone_root_R
RECORD * l_rig_spring
	pair:left RLU_apex
	pair:right RRU_apex


# some bolt ons
RECORD * l_rig_spring
	pair:left RLU_apex
	pair:right FLL_wishbone_root_R
RECORD * l_rig_spring
	pair:left RLU_apex
	pair:right FRL_wishbone_root_R
RECORD * l_rig_spring
	pair:left RRU_apex
	pair:right FLL_wishbone_root_R


RECORD * T_STRAND
	strand:a	FRU_wishbone_root_R
	strand:b	FRL_wishbone_root_R
	strand:c	RRU_apex

RECORD * T_STRAND
	strand:a	FLL_wishbone_root_R
	strand:b	FRL_wishbone_root_R
	strand:c	RRU_apex

RECORD * T_STRAND
	strand:a	FRL_wishbone_root_R
	strand:b	RRU_apex
	strand:c	RLU_apex


END




