/*	Copyright (C) 2018 Arrival Ltd
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit git.hiverepo.com */

/** @file */

#include "fe/data.h"
#include "tire/tire.h"
#include "vehicle_moa/vehicle_moa.h"
#include "evaluate/evaluate.h"
#include "surface/surface.h"

#include "viewer_system/viewer_system.h"

#define TEST_SURFACEI

using namespace fe;
using namespace ext;
using namespace hive;

int main(int argc, char *argv[])
{
	UnitTest unitTest;
	try
	{
		feLogger()->clear(".*");
		feLogger()->clear("error");
		feLogger()->clear("info");
		feLogger()->clear("warning");

		sp<Master> spMaster(new Master);
		assertMath(spMaster->typeMaster());
		assertInterface<DrawI>(spMaster, "DrawI");
		sp<Registry> spRegistry = spMaster->registry();
		spRegistry->manage("fexMechanicsDL");
		spRegistry->manage("hiveMoaDL");
		spRegistry->manage("hiveTireHiveDL");
		spRegistry->manage("fexEvaluateDL");
		spRegistry->manage("fexSurfaceDL");
		spRegistry->manage("feDataDL");
		spRegistry->manage("fexViewerDL");
		spRegistry->manage("hiveViewerSystemDL");

		sp<Scope> spScope = spRegistry->create("Scope");

		Overlayer overlayer(spScope);

		overlayer.load("vehicle.rg");
		overlayer.load("linear.rg");

		for(sp<RecordGroup> rg_clone : overlayer.bakedDatasets())
		{
			// >>>>>>>>> CREATE ORCHESTRATOR
			sp<OrchestratorI> spOrchestrator(
				spRegistry->create("OrchestratorI.Orchestrator"));
			spOrchestrator->datasetInitialize(rg_clone);

//TODO: work through leapfrog artifacting
			spOrchestrator->append(spRegistry->create("SystemI.LocatorFromEuler"));
			spOrchestrator->append(spRegistry->create("SystemI.SoftSystem"));
			spOrchestrator->append(spRegistry->create("SystemI.SimulatorLoop"));
			spOrchestrator->append(spRegistry->create("SystemI.ParticleMountSystem"));
			spOrchestrator->append(spRegistry->create("SystemI.LocatorFromPoints"));
			spOrchestrator->append(spRegistry->create("SystemI.TireISystem"));

#ifdef TEST_SURFACEI
			spOrchestrator->append(spRegistry->create("SystemI.SurfaceContact"));
#else
			spOrchestrator->append(spRegistry->create("SystemI.ClosestGroundContact"));
#endif
			spOrchestrator->append(spRegistry->create("SystemI.TireContact"));

			sp<ViewerSystem> spViewerSystem = spRegistry->create("SystemI.PointViewer");
			spOrchestrator->append(spViewerSystem);

			if(!spOrchestrator->compile())
			{
				fprintf(stderr, "compile failed\n");
				exit(101);
			}

			sp<ThreadedViewerI> spViewer = spRegistry->create("ThreadedViewerI.MOAViewer");
			spViewer->open(spOrchestrator);

			t_note_id note_start = spOrchestrator->note_id(HIVE_NOTE_START);
			spOrchestrator->perform(note_start);

			//snapshot(rg_clone, "dataset.rg");

			while(true)
			{
				sleep(1);
			}
		}
	}
	catch(Exception &e)
	{
		e.log();
	}
	return unitTest.failures();
}

