/*	Copyright (C) 2018 Arrival Ltd
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit git.hiverepo.com */

/** @file */

#ifndef __vehicle_moa_vehicle_moa_h__
#define __vehicle_moa_vehicle_moa_h__

#include "fe/plugin.h"
#include "fe/data.h"
#include "math/math.h"
#include "mechanics/mechanics.h"
#include "surface/surface.h"
#include "moa/moa.h"

#endif /* __vehicle_moa_vehicle_moa_h__ */
