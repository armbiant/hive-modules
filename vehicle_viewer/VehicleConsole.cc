/*	Copyright (C) 2018 Arrival Ltd
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit git.hiverepo.com */

/** @file */

#include "vehicle_viewer.pmh"

using namespace fe;
using namespace fe::ext;

#define VEHICLE_CONSOLE_VERBOSE	FALSE

namespace hive
{

void VehicleConsole::KeyHandler::handle(fe::Record& a_signal)
{
	m_event.bind(a_signal);

#if VEHICLE_CONSOLE_VERBOSE
	feLog("VehicleConsole::KeyHandler::handle\n  %s\n",
			c_print(m_event));
#endif

	if(m_event.isKeyboard() && m_spVehicleConsole.isValid())
	{
		m_spVehicleConsole->relay(a_signal);
	}
}

void VehicleConsole::DrawHandler::handle(fe::Record& a_signal)
{
	if(!m_asViewer.scope().isValid())
	{
		m_asViewer.bind(a_signal.layout()->scope());
	}

	if(m_asViewer.render_draw.check(a_signal) &&
			m_asViewer.viewer_layer.check(a_signal) &&
			m_asViewer.viewer_layer(a_signal)==2)
	{
		const Color yellow(1.0f,1.0f,0.0f,1.0f);

		sp<DrawI> spDrawI=m_asViewer.render_draw(a_signal);

		String text;
		text.sPrintf("Vehicle Console\n");
		spDrawI->drawAlignedText(
				SpatialVector(10.0f,30.0f,1.0f),text,yellow);
	}
}


void VehicleConsole::initialize(void)
{
	m_spKeyHandler=Library::create<KeyHandler>("KeyHandler");
	if(m_spKeyHandler.isNull())
	{
		feLog("VehicleConsole::initialize could not create KeyHandler\n");
		return;
	}
	m_spKeyHandler->bind(sp<VehicleConsole>(this));

	m_spDrawHandler=Library::create<DrawHandler>("DrawHandler");
	if(m_spDrawHandler.isNull())
	{
		feLog("VehicleConsole::initialize could not create DrawHandler\n");
		return;
	}

	m_spQuickViewerI=registry()->create("QuickViewerI");
	if(m_spQuickViewerI.isNull())
	{
		feLog("VehicleConsole::initialize could not create QuickViewerI\n");
		return;
	}

	m_spQuickViewerI->getWindowI()->setSize(256,256);
	m_spQuickViewerI->open();

	sp<DrawI> spDrawI=m_spQuickViewerI->getDrawI();
	if(spDrawI.isNull())
	{
		feLog("VehicleConsole::initialize could not get draw interface\n");
		return;
	}

	m_spQuickViewerI->insertDrawHandler(m_spDrawHandler);
	m_spQuickViewerI->insertEventHandler(m_spKeyHandler);
}

void VehicleConsole::handleBind(sp<SignalerI> spSignalerI, sp<Layout> spLayout)
{
	m_hpSignalerI=spSignalerI;

	m_eventRelay.bind(spLayout->scope());
	m_eventRelay.createRecord();
}

void VehicleConsole::handle(fe::Record& a_signal)
{
#if VEHICLE_CONSOLE_VERBOSE
	feLog("VehicleConsole::handle layout \"%s\"\n",
			a_signal.layout()->name().c_str());
#endif

	m_event.bind(a_signal);

	if(!m_event.isPoll())
	{
		return;
	}

	if(m_spQuickViewerI.isValid())
	{
		m_spQuickViewerI->run(1);
	}
}

void VehicleConsole::relay(fe::Record& a_signal)
{
	sp<SignalerI> spSignalerI(m_hpSignalerI);
	if(spSignalerI.isNull())
	{
		feLog("VehicleConsole::KeyHandler::relay Signaler disappeared\n");
		return;
	}

	m_eventIn.bind(a_signal);

#if VEHICLE_CONSOLE_VERBOSE
	feLog("VehicleConsole::KeyHandler::relay original\n  %s\n",
			c_print(m_eventIn));
#endif

	if(!m_eventRelay.scope().isValid())
	{
#if VEHICLE_CONSOLE_VERBOSE
	feLog("VehicleConsole::KeyHandler::relay no event scope\n");
#endif
		return;
	}

	m_eventRelay.setSIS(m_eventIn.source(),m_eventIn.item(),
			m_eventIn.state());

#if VEHICLE_CONSOLE_VERBOSE
	feLog("VehicleConsole::KeyHandler::relay copy\n  %s\n",
			c_print(m_eventRelay));
#endif

	Record recordRelay=m_eventRelay.record();
	m_hpSignalerI->signal(recordRelay);
}

}
