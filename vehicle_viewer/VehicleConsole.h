/*	Copyright (C) 2018 Arrival Ltd
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit git.hiverepo.com */

/** @file */

#ifndef __vehicle_viewer_VehicleConsole_h__
#define __vehicle_viewer_VehicleConsole_h__

namespace hive
{

class FE_DL_EXPORT VehicleConsole:
	virtual public fe::ext::HandlerI,
	public fe::Initialize<VehicleConsole>
{
public:
				VehicleConsole(void)										{}
virtual			~VehicleConsole(void)										{}

		void	initialize(void);

virtual	void	handleBind(fe::sp<fe::ext::SignalerI> spSignalerI,
					fe::sp<fe::Layout> spLayout);
virtual	void	handle(fe::Record& a_signal);

private:

	class KeyHandler:
			virtual public fe::ext::HandlerI,
			public fe::CastableAs<KeyHandler>
	{
	public:
					KeyHandler(void)										{}
	virtual			~KeyHandler(void)										{}

	virtual	void	handle(fe::Record& a_signal);

			void	bind(fe::sp<VehicleConsole> a_spVehicleConsole)
					{	m_spVehicleConsole=a_spVehicleConsole; }

	private:
			fe::sp<VehicleConsole>			m_spVehicleConsole;
			fe::ext::WindowEvent			m_event;
	};

	class DrawHandler:
			virtual public fe::ext::HandlerI,
			public fe::CastableAs<DrawHandler>
	{
	public:
					DrawHandler(void)										{}
	virtual			~DrawHandler(void)										{}

	virtual	void	handle(fe::Record& a_signal);

	private:
			fe::ext::AsViewer			m_asViewer;
	};

		void	relay(fe::Record& a_signal);

		fe::sp<fe::ext::QuickViewerI>	m_spQuickViewerI;
		fe::hp<fe::ext::SignalerI>		m_hpSignalerI;
		fe::sp<KeyHandler>				m_spKeyHandler;
		fe::sp<fe::ext::HandlerI>		m_spDrawHandler;
		fe::ext::WindowEvent			m_event;
		fe::ext::WindowEvent			m_eventIn;
		fe::ext::WindowEvent			m_eventRelay;
};

} // namespace __vehicle_viewer_VehicleConsole_h__

#endif
