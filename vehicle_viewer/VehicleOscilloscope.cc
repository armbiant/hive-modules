/*	Copyright (C) 2018 Arrival Ltd
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit git.hiverepo.com */

/** @file */

#include "vehicle_viewer.pmh"

using namespace fe;
using namespace fe::ext;

#define VEHICLE_VIEWER_VERBOSE	FALSE

namespace hive
{

VehicleOscilloscope::VehicleOscilloscope(void)
{
}

void VehicleOscilloscope::initialize(void)
{
	m_spQuickViewerI=registry()->create("QuickViewerI");
	if(!m_spQuickViewerI.isValid())
	{
		feLog("VehicleOscilloscope::initialize could not create QuickViewerI");
		return;
	}

	m_spQuickViewerI->open();

	sp<DrawI> spDrawI=m_spQuickViewerI->getDrawI();
	if(spDrawI.isNull())
	{
		feLog("VehicleOscilloscope::initialize could not get draw interface");
		return;
	}

	m_spImguiHandlerRecord=registry()->create("*.ImguiHandlerRecord");
	if(m_spImguiHandlerRecord.isNull())
	{
		feLog("VehicleOscilloscope::initialize"
				" could not create ImguiHandlerRecord");
		return;
	}

	m_spImguiHandlerRecord->bind(spDrawI);

	m_spQuickViewerI->insertDrawHandler(m_spImguiHandlerRecord);
	m_spQuickViewerI->insertEventHandler(m_spImguiHandlerRecord);

	m_spRecordGroup=new RecordGroup();

	//* release in case handle() occurs on a different thread
	m_spQuickViewerI->getWindowI()->releaseCurrent();
}

void VehicleOscilloscope::handle(fe::Record& a_signal)
{
	if(m_spImguiHandlerRecord.isNull())
	{
		return;
	}

#if VEHICLE_VIEWER_VERBOSE
	feLog("VehicleOscilloscope::handle layout \"%s\"\n",
			a_signal.layout()->name().c_str());
#endif

	if(!m_asVehicle.bind(a_signal))
	{
		feLog("VehicleOscilloscope::handle not AsVehicle\n");
		return;
	}

//	const Real deltaTime=m_asVehicle.deltaTime(a_signal);

	m_spRecordGroup->add(a_signal);

	m_spImguiHandlerRecord->bind(m_spRecordGroup);

	m_spQuickViewerI->getWindowI()->makeCurrent();
	m_spQuickViewerI->run(1);

	m_spRecordGroup->remove(a_signal);
}

}
