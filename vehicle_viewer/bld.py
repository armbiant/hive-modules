import os
import sys
import string
forge = sys.modules["forge"]

def prerequisites():
	return [ "apiary", "imgui", "viewer" ]

def setup(module):
	srcList = [	"vehicle_viewer.pmh",
				"VehicleConsole",
				"VehicleOscilloscope",
				"vehicle_viewerDL" ]

	dll = module.DLL( "hiveVehicleViewerDL", srcList )

	deplibs = forge.corelibs + [
				"hiveApiaryDLLib",
				"fexImguiDLLib",
				"fexViewerDLLib",
				"fexWindowLib" ]

	if forge.fe_os == 'FE_WIN32' or forge.fe_os == 'FE_WIN64':
		deplibs += [	"fexSignalLib",
						"fexDataToolDLLib",
						"fexDrawDLLib",
						"fexGeometryDLLib",
						"fexOperateDLLib",
						"fexSurfaceDLLib",
						"fexThreadDLLib",
						"fexViewerDLLib" ]

	forge.deps( ["hiveVehicleViewerDLLib"], deplibs )

	forge.tests += [
		("inspect.exe",	"hiveVehicleViewerDL",	None,	None) ]

	module.Module('test')
