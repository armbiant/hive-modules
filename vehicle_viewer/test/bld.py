import sys
forge = sys.modules["forge"]

import os.path

def setup(module):
	deplibs = forge.corelibs[:]

	deplibs += [	"fexImguiDLLib",
					"fexViewerDLLib",
					"fexWindowLib",
					"fexNetworkHostDLLib",
					"hiveApiaryDLLib" ]

	if forge.fe_os == 'FE_WIN32' or forge.fe_os == 'FE_WIN64':
		deplibs += [	"fexSignalLib",
						"fexDataToolLib",
						"fexDrawDLLib",
						"fexGeometryDLLib",
						"fexOperateDLLib",
						"fexSurfaceDLLib",
						"fexThreadDLLib",
						"fexNetworkDLLib"]

	tests = [	'xHiveSpy' ]

	for t in tests:
		exe = module.Exe(t)
		if forge.fe_os == "FE_LINUX":
			exe.linkmap = { "stdthread": "-lpthread" }
		forge.deps([t + "Exe"], deplibs)
