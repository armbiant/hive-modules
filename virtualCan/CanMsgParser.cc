/*	Copyright (C) 2018 Arrival Ltd
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit git.hiverepo.com */

#include "virtualCan/virtualCan.pmh"


namespace hive
{

uint32_t CanMsgParser::getCanMsgVal(const struct can_frame * msg, uint32_t offset, uint64_t mask)
{
    uint64_t aux = 0;
    uint8_t sl = 0;
    for (unsigned int i = 0; i < 8; ++i) {
        aux += (((uint64_t)msg->data[i]) << sl);
        sl += 8;
    }
    return ((aux & mask) >> offset);
}

void CanMsgParser::setCanMsgVal(struct can_frame * msg, uint32_t offset, uint64_t mask, uint32_t val)
{ 
    // read
    uint64_t aux = 0;
    uint8_t sl = 0;
    for (unsigned int i = 0; i < 8; ++i) {
        aux += (((uint64_t)msg->data[i]) << sl);
        sl += 8;
    }
    // clear
    aux &= ~mask;

    // set
    uint64_t wr_word =  (uint64_t)val << offset;    
    wr_word = wr_word | aux;
    uint8_t sr = 0;
    for (unsigned int i = 0; i < 8; ++i) {
        msg->data[i] = ((wr_word >> sr) & 0x00FF);
        sr += 8;
    }  
}

}
