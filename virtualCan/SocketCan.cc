/*	Copyright (C) 2018 Arrival Ltd
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit git.hiverepo.com */

#include "virtualCan/virtualCan.pmh"

namespace hive
{

#if FE_OS!=FE_WIN32 && FE_OS!=FE_WIN64
SocketCan::SocketCan(void):
	m_sock(-1),
	m_ready(false)
{
}

SocketCan::~SocketCan(void)
{
	shutdown();
}

void SocketCan::shutdown(void)
{
	unbind();
}

void SocketCan::unbind(void)
{
	if(m_sock >= 0)
	{
		if(::close(m_sock) < 0)
		{
			feLog("SocketCan::unbind error closing socket %d\n",m_sock);
		}
	}
	m_sock = -1;
	m_ready = false;
}

void SocketCan::start(const std::string & CANDevice)
{
	unbind();

	m_deviceName = CANDevice;
	m_sock = socket(PF_CAN, SOCK_RAW, CAN_RAW);
	if(m_sock >= 0)
	{
		strcpy(m_ifr.ifr_name, CANDevice.c_str() );
		if(ioctl(m_sock, SIOCGIFINDEX, &m_ifr) < 0)
		{
			feLog("[SocketCan] failed ioctl\n");
			shutdown();
			return;
		}
		/*
		if(fcntl(m_sock, F_SETFL, O_NONBLOCK) < 0)
		{
			feLog("[SocketCan] failed fnctl\n");
			shutdown();
			return;
		}
		*/
		memset(&m_addr, 0, sizeof(m_addr));
		m_addr.can_family = AF_CAN;
		m_addr.can_ifindex = m_ifr.ifr_ifindex;

		if(bind(m_sock, (struct sockaddr *)&m_addr, sizeof(m_addr)) < 0)
		{
			feLog("[SocketCan] failed bind\n");
			return;
		}

		feLog("Socket CAN started\n");
		m_ready = true;
	}
}

int SocketCan::receive(struct can_frame *frame)
{
	if(!m_ready)
	{
		return 0;
	}

	int recvbytes = -1;
	if(m_sock < 0)
	{
		return recvbytes;
	}
	else
	{
		struct timeval timeout = {1, 0}; // 1 sec
		fd_set readSet;
		FD_ZERO(&readSet);
		FD_SET(m_sock, &readSet);
		if(select(m_sock + 1, &readSet, NULL, NULL, &timeout) >= 0)
		{
			if(FD_ISSET(m_sock, &readSet))
			{
				return (read(m_sock, frame, sizeof(struct can_frame)));
			}
		}
	}
	return recvbytes;
}

int SocketCan::send(struct can_frame *frame)
{
	if(!m_ready)
	{
		return 0;
	}

	int retVal = -1;
	if(m_sock < 0)
	{
		feLog("Socket CAN: bad socket\n");
		return(retVal);
	}
	else
	{
		retVal = write(m_sock, frame, sizeof(struct can_frame));
		return retVal;
	}
}

#else

SocketCan::SocketCan(void)
{
}

SocketCan::~SocketCan(void)
{
	shutdown();
}

void SocketCan::shutdown(void)
{
}

void SocketCan::start(const std::string &CANDevice)
{
	feLog("Socket CAN started\n");
}

int SocketCan::receive(struct can_frame *frame)
{
	return 0;
}

int SocketCan::send(struct can_frame *frame)
{
	return 0;
}

#endif

bool SocketCan::isSamePayload(can_frame &frame_1, can_frame &frame_2)
{
	for (int i = 0; i < 8; ++i)
	{
		if(frame_1.data[i] != frame_2.data[i])
		{
			return false;
		}
	}
	return true;
}

void SocketCan::copyPayload(can_frame &frameSrc, can_frame &frameDst)
{
	memcpy(&frameDst.data[0], &frameSrc.data[0], 8);
}

}
