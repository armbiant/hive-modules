/*	Copyright (C) 2018 Arrival Ltd
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit git.hiverepo.com */

#ifndef hive_virtual_can_I_h
#define hive_virtual_can_I_h

#include <virtualCan/can_data_types.h>


namespace hive
{
class FE_DL_EXPORT VirtualCanI : virtual public fe::Component,
                                 public fe::CastableAs<VirtualCanI>
{
public:
	virtual void start(const std::string &CANDevice)					=0;
	virtual int  receive(struct can_frame *frame)						=0;
	virtual int  send(struct can_frame * frame)							=0;
	virtual void registerSafetyHandler()								=0;
	virtual void registerTelemetryHandler() 							=0;
	virtual bool isSamePayload(can_frame &frame_1, can_frame &frame_2)	=0;
	virtual void copyPayload(can_frame &frameSrc, can_frame &frameDst)	=0;
};

}


#endif
