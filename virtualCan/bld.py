import os
import sys
import string
forge = sys.modules["forge"]

def setup(module):
	srcList = [	"virtualCan.pmh",
				"CanMsgParser",
				"SocketCan",
				"virtualCanDL" ]

	deplibs = forge.basiclibs + [
				"fePluginLib" ]

	dll = module.DLL( "hiveCanDL", srcList )

	dll.linkmap = {}

	forge.deps( ["hiveCanDLLib"], deplibs )

	forge.tests += [
		("inspect.exe",	"hiveCanDL",	None,	None) ]

	#module.Module('test')

def auto(module):
	# TODO check something
	if forge.fe_os != "FE_LINUX":
		return None

	test_file = """

#include <linux/can.h>
int main(void)
{
	return(0);
}
\n"""

	result = forge.cctest(test_file)

	forge.linkmap.pop('can_libs', None)

	return result
