/*	Copyright (C) 2018 Arrival Ltd
	Any use of this software requires a license.  If a valid license
	was not distributed with this file, visit git.hiverepo.com */

#ifndef __virtual_can_H__
#define __virtual_can_H__

#include <cinttypes>
#include "fe/plugin.h"
#include "virtualCan/VirtualCanI.h"
#include "CanMsgParser.h"

#endif
